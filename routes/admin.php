<?php
/*
|--------------------------------------------------------------------------
| Web Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
Route::group([
    'prefix' => 'auth',
    'as'     => 'auth.',
], function () {
    Route::get('login', 'LoginController@showLoginForm')->name('login');
    Route::post('login', 'LoginController@login')->name('postlogin');
    Route::post('logout', 'LoginController@logout')->name('logout');

    Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::post('password/reset', 'ForgotPasswordController@reset')->name('password.update');

    Route::get('register', 'RegisterController@showRegistrationForm')->name('register');
    Route::post('register', 'RegisterController@register')->name('postregister');

});

Route::group([
    'middleware' => 'auth.admin',
], function () {
    Route::get('/', function () {
        return redirect()->route('admin.home');
    });

    Route::get('/home', function () {
        return view('admin.home');
    })->name('home');

    Route::group(
        [
            'prefix' => 'image',
            'as'     => 'image.',
        ],
        function () {
            Route::post('uploadmultiple/{type?}', 'ImageController@uploadImageMultiple')->name('uploadmultiple');
            Route::post('uploadone/{type?}', 'ImageController@uploadImageOne')->name('uploadone');
            Route::post('removebysrc', 'ImageController@removeBySrc')->name('removebysrc');
        }
    );

    Route::group(
        [
            'prefix'     => 'blogcategory',
            'as'         => 'blogcategory.',
            'middleware' => ['permission:blog-category-control'],
        ],
        function () {
            Route::get('index', 'BlogCategoryController@index')->name('index');
            Route::get('show/{id}', 'BlogCategoryController@show')->name('show');
            Route::get('create', 'BlogCategoryController@create')->name('create');
            Route::post('store', 'BlogCategoryController@store')->name('store');
            Route::get('edit/{id}', 'BlogCategoryController@edit')->name('edit');
            Route::put('update/{id}', 'BlogCategoryController@update')->name('update');
            Route::delete('destroy/{id}', 'BlogCategoryController@destroy')->name('destroy');
            Route::put('restore/{id}', 'BlogCategoryController@restore')->name('restore');
        }
    );

    Route::group(
        [
            'prefix' => 'blogbanner',
            'as'     => 'blogbanner.',
        ],
        function () {
            Route::post('update_position','BlogBannerController@updatePosition')->name('update_position');
            Route::get('index', 'BlogBannerController@index')->name('index');
            Route::get('show/{id}', 'BlogBannerController@show')->name('show');
            Route::get('create', 'BlogBannerController@create')->name('create');
            Route::post('store', 'BlogBannerController@store')->name('store');
            Route::get('edit/{id}', 'BlogBannerController@edit')->name('edit');
            Route::put('update/{id}', 'BlogBannerController@update')->name('update');
            Route::delete('destroy/{id}', 'BlogBannerController@destroy')->name('destroy');
            Route::put('restore/{id}', 'BlogBannerController@restore')->name('restore');
        }
    );

    Route::group(
        [
            'prefix' => 'blog',
            'as'     => 'blog.',
        ],
        function () {
            Route::get('index', 'BlogController@index')->name('index')->middleware('permission:blog-show');
            Route::get('show/{id}', 'BlogController@show')->name('show')->middleware('permission:blog-show');
            Route::get('create', 'BlogController@create')->name('create')->middleware('permission:blog-create');
            Route::post('store', 'BlogController@store')->name('store')->middleware('permission:blog-create');
            Route::get('edit/{id}', 'BlogController@edit')->name('edit')->middleware('permission:blog-edit');
            Route::put('update/{id}', 'BlogController@update')->name('update')->middleware('permission:blog-edit');
            Route::delete('destroy/{id}', 'BlogController@destroy')->name('destroy')->middleware('permission:blog-remove');
            Route::put('restore/{id}', 'BlogController@restore')->name('restore')->middleware('permission:blog-remove');
        }
    );

    Route::group(
        [
            'prefix'     => 'productcategory',
            'as'         => 'productcategory.',
            'middleware' => ['permission:product-category-control'],
        ],
        function () {
            Route::get('index', 'ProductCategoryController@index')->name('index');
            Route::get('show/{id}', 'ProductCategoryController@show')->name('show');
            Route::get('create', 'ProductCategoryController@create')->name('create');
            Route::post('store', 'ProductCategoryController@store')->name('store');
            Route::get('edit/{id}', 'ProductCategoryController@edit')->name('edit');
            Route::put('update/{id}', 'ProductCategoryController@update')->name('update');
            Route::delete('destroy/{id}', 'ProductCategoryController@destroy')->name('destroy');
            Route::put('restore/{id}', 'ProductCategoryController@restore')->name('restore');
        }
    );

    Route::group(
        [
            'prefix' => 'product',
            'as'     => 'product.',
        ],
        function () {
            Route::get('index', 'ProductController@index')->name('index')->middleware('permission:product-show');
            Route::get('show/{id}', 'ProductController@show')->name('show')->middleware('permission:product-show');
            Route::get('create', 'ProductController@create')->name('create')->middleware('permission:product-create');
            Route::post('store', 'ProductController@store')->name('store')->middleware('permission:product-create');
            Route::get('edit/{id}', 'ProductController@edit')->name('edit')->middleware('permission:product-edit');
            Route::put('update/{id}', 'ProductController@update')->name('update')->middleware('permission:product-edit');
            Route::delete('destroy/{id}', 'ProductController@destroy')->name('destroy')->middleware('permission:product-remove');
            Route::put('restore/{id}', 'ProductController@restore')->name('restore')->middleware('permission:product-remove');
        }
    );

    Route::group(
        [
            'prefix' => 'slider',
            'as'     => 'slider.',
        ],
        function () {
            Route::post('update_position','SliderController@updatePosition')->name('update_position');
            Route::get('index', 'SliderController@index')->name('index');
            Route::get('show/{id}', 'SliderController@show')->name('show');
            Route::get('create', 'SliderController@create')->name('create');
            Route::post('store', 'SliderController@store')->name('store');
            Route::get('edit/{id}', 'SliderController@edit')->name('edit');
            Route::put('update/{id}', 'SliderController@update')->name('update');
            Route::delete('destroy/{id}', 'SliderController@destroy')->name('destroy');
            Route::put('restore/{id}', 'SliderController@restore')->name('restore');
        }
    );

    Route::group(
        [
            'prefix'     => 'user',
            'as'         => 'user.',
            'middleware' => ['permission:user-control'],
        ],
        function () {
            Route::get('index', 'UserController@index')->name('index');
            Route::get('show/{id}', 'UserController@show')->name('show');
            Route::get('create', 'UserController@create')->name('create');
            Route::post('store', 'UserController@store')->name('store');
            Route::get('edit/{id}', 'UserController@edit')->name('edit');
            Route::get('edit-permission/{id}', 'UserController@editPermission')->name('edit_permission');
            Route::put('update-permission/{id}', 'UserController@updatePermission')->name('update_permission');
            Route::put('update/{id}', 'UserController@update')->name('update');
            Route::delete('destroy/{id}', 'UserController@destroy')->name('destroy');
            Route::put('restore/{id}', 'UserController@restore')->name('restore');
            Route::put('approve/{id}', 'UserController@approve')->name('approve');
        }
    );


    Route::group(
        [
            'prefix'     => 'brandcategory',
            'as'         => 'brandcategory.',
        ],
        function () {
            Route::get('index', 'BrandCategoryController@index')->name('index');
            Route::get('show/{id}', 'BrandCategoryController@show')->name('show');
            Route::get('create', 'BrandCategoryController@create')->name('create');
            Route::post('store', 'BrandCategoryController@store')->name('store');
            Route::get('edit/{id}', 'BrandCategoryController@edit')->name('edit');
            Route::put('update/{id}', 'BrandCategoryController@update')->name('update');
            Route::delete('destroy/{id}', 'BrandCategoryController@destroy')->name('destroy');
            Route::put('restore/{id}', 'BrandCategoryController@restore')->name('restore');
        }
    );

    Route::group(
        [
            'prefix' => 'brand',
            'as'     => 'brand.',
        ],
        function () {
            Route::get('index', 'BrandController@index')->name('index');
            Route::get('show/{id}', 'BrandController@show')->name('show');
            Route::get('create', 'BrandController@create')->name('create');
            Route::post('store', 'BrandController@store')->name('store');
            Route::get('edit/{id}', 'BrandController@edit')->name('edit');
            Route::put('update/{id}', 'BrandController@update')->name('update');
            Route::delete('destroy/{id}', 'BrandController@destroy')->name('destroy');
            Route::put('restore/{id}', 'BrandController@restore')->name('restore');
        }
    );
});
