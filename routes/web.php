<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
Route::get('error/{code}', function ($code) {
    return view('errors.error', compact('code'));
})->name('error');

Route::group([
	'as' =>'user.',
    'namespace' => 'User',
], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('doanh-nghiep', 'HomeController@business')->name('business');
    Route::get('ve-chung-toi', 'HomeController@aboutus')->name('aboutus');
    Route::get('faq', 'HomeController@faq')->name('faq');
    Route::get('lien-he', 'HomeController@contact')->name('contact');
    Route::post('lien-he/{type}', 'HomeController@submitContact')->name('submitContact');
    Route::get('blog', 'HomeController@blog')->name('blog');
    Route::get('blog/article/{slug}_{id}', 'HomeController@blogDetail')->name('blog.detail');
    Route::get('blog/category/{slug}_{id}', 'HomeController@blogCategory')->name('blog.category');
});
