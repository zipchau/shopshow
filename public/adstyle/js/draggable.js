var $arrayPrioritize = {};
var fixHelperModified = function(e, tr) {
var $originals = tr.children();
var $helper = tr.clone();
$helper.children().each(function(index) {
    $(this).width($originals.eq(index).width())
});
return $helper;
},
updateIndex = function(e, ui) {
    $arrayPrioritize = {};
    $('td.index', ui.item.parent()).each(function (i) {
        $(this).html(i+1);
    });
    $('.position', ui.item.parent()).each(function (i) {
        $(this).text(i + 1);
    });
    $('input[type=hidden]', ui.item.parent()).each(function (i) {
        var id = $(this).attr("data-id");
        var value = i + 1;
        $(this).val(i + 1);
        $arrayPrioritize[id] = value
    });
};

$("#tableDrag tbody").sortable({
    helper: fixHelperModified,
    stop: updateIndex
}).disableSelection();

$("tbody").sortable({
    distance: 5,
    delay: 50,
    opacity: 0.6,
    cursor: 'move',
    update: function() {}
});

function updatePrioritize(url) {
    var prioritize  = JSON.stringify($arrayPrioritize);
    if(prioritize != '{}'){
        var form_data = new FormData();
            form_data.append('prioritize', prioritize);
            form_data.append('_token', $('meta[name="csrf-token"]').attr('content'));
        $.ajax({
          url: url,
          data: form_data,
          type: 'post',
          contentType: false,
          processData: false,
          success: function(res) {
            swal(
              'Updated!',
              'Changed positon!',
              'success'
            )
          },
          error:function(res){
              swal(
                  'Oops...',
                  'Something went wrong!',
                  'error',
              )
          }
        });
    }else{
        swal(
          'Oops...',
          'Something went wrong!',
          'error'
        )
    }
}