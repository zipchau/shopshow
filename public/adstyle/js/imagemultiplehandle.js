(function($, window, undefined) {
    //is onprogress supported by browser?
    var hasOnProgress = ("onprogress" in $.ajaxSettings.xhr());

    //If not supported, do nothing
    if (!hasOnProgress) {
        return;
    }
    
    //patch ajax settings to call a progress callback
    var oldXHR = $.ajaxSettings.xhr;
    $.ajaxSettings.xhr = function() {
        var xhr = oldXHR.apply(this, arguments);
        if(xhr instanceof window.XMLHttpRequest) {
            xhr.addEventListener('progress', this.progress, false);
        }
        
        if(xhr.upload) {
            xhr.upload.addEventListener('progress', this.progress, false);
        }
        
        return xhr;
    };
})(jQuery, window);

function UploadImage(e) {
    if (e.files[0]) {
        var form_data = new FormData();
        form_data.append('file', e.files[0]);
        form_data.append('_token', $('meta[name="csrf-token"]').attr('content'));
        $.ajax({
            url: "/admin/image/uploadone",
            data: form_data,
            type: 'POST',
            contentType: false,
            processData: false,
            success: function(res) {
                dataSuccess = JSON.parse(res);
                $(e).parent().parent().find('.image_show').attr('src', '/' + dataSuccess.filename);
                $(e).next('input').val(dataSuccess.filename)
                $(e).val('')
            }
        });
    }
};

function UploadImageMultiple(e) {
    var form_data = new FormData();
    var ins = e.files.length;
    for (var x = 0; x < ins; x++) {
        form_data.append("files[]", e.files[x]);
    }
    $('.progressBarUploadImage').attr('aria-valuenow', 0).css('width', 0+'%');
    $('.progressBarUploadImageShow').text(0+'%');
    form_data.append('_token', $('meta[name="csrf-token"]').attr('content'));
    $.ajax({
        url: "/admin/image/uploadmultiple",
        data: form_data,
        type: 'POST',
        contentType: false,
        processData: false,
        progress: function(e) {
            if(e.lengthComputable) {
                var pct = (e.loaded / e.total) * 100;
                $('.progressBarUploadImage').attr('aria-valuenow', parseInt(pct)).css('width', parseInt(pct)+'%');
                $('.progressBarUploadImageShow').text(parseInt(pct)+'%');
                if(parseInt(pct) == 100 ){
                    $('.progressBarUploadImageShow').text('complete');
                }
            }
        },
        success: function(res) {
            if(res.errors.length != 0){
                $('.errorUploadImageResult').html(printErrorsMultiple(res.errors))
            }
            if(res.success.length != 0){
                $('.listUploadImageResult').append(printSuccessMultiple(res.success))
            }
            $(e).val('')
            swal(
              'Uploaded!',
              'The file has been uploaded to the server!',
              'success'
            )
        }
    });
}

function printErrorsMultiple(param){
    var result = '<div class="alert alert-danger dismissible-alert" role="alert">'
    +'    <b>Errors:</b></i>'
    +'    <br>'
    +'    <ul class="list-unstyled">';
    param.forEach(function(element){
        result = result + '<li>' +element.file + '</li>';
    });
    result = result + '</ul></div>';
    return result;
}
var countImageBlog = $('.listUploadImageResult').find('.media').length +1;
function printSuccessMultiple(param) {
    var result = '';
    param.forEach(function(element){
    result = result + '<div class="media imageResultShow">'
    +'      <img src="/'+element.filename+'" onclick="showOneImage(this)" data-src="/'+element.filename+'" data-atl="" data-height="100%" data-width="100%" class="mr-3" style="width:70px">'
    +'      <div class="media-body ">'
    +'           <div class="form-group">'
    +'              <div class="row col-md-12"> <div class="col-md-9">'
    +'             <input type="text" class="form-control enable-mask inputSrc" value="'+location.protocol+ "//"+window.location.host+'/'+element.filename+'" disabled>'
    +'              </div><div class="col-md-3"><button type="button" class="btn btn-inverse-danger" onclick="removeSoft(this)" data-src="'+element.filename+'">Remove</button></div> '
    +'             <div class="col-md-12"><div class="checkbox">'
    +'                <label>'
    +'                   <input class="form-check-input" type="checkbox" name="file_uploads['+countImageBlog+'][status]">'
    +'                            Set index'
    +'                            <i class="input-frame"></i>'
    +'                    </input>'
    +'                </label></div></div>'
    +'               </div>'
    +'           </div>'
    +'          <input type="hidden" name="file_uploads['+countImageBlog+'][src]" value="'+element.filename+'"/>'
    +'      </div>'
    +'    </div>';
    countImageBlog = countImageBlog +1;
    });
    return result;
}


function removePedding(e) {
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result == true) {
        swal(
          'Deleted!',
          'Your file has been deleted.',
          'success'
        )
        $(e).parents('.imageResultShow').remove();
      }
    });
}

function removeSoft(e) {
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result == true) {
        var form_data = new FormData();
        form_data.append('src', $(e).attr("data-src"));
        form_data.append('_token', $('meta[name="csrf-token"]').attr('content'));
        $.ajax({
            url: "/admin/image/removebysrc",
            data: form_data,
            type: 'POST',
            contentType: false,
            processData: false,
            success: function(res) {
                swal(
                  'Deleted!',
                  'Your file has been deleted.',
                  'success'
                )
                $(e).parents('.imageResultShow').remove();
            },
            error:function(res){
                swal(
                    'Oops...',
                    'Something went wrong!',
                    'error',
                )
            }
        });
      }
    });
}


