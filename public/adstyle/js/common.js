numberComman()
var options = {
    filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
    filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
    filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
    filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
};
function deleteByIdModal(id, url, message = "You won't be able to revert this!") {
    $('#destroyForm').attr('action', url);
    $('#destroyAsk').text('Are you sure ? ');
    $('#destroyTitle').text("You won't be able to revert this!");
    $("#destroyIdForm").val(id);
    $('#deleteModalById').modal();
};

function approveByIdModal(id, url, message = "You won't be able to revert this!") {
    $('#approveForm').attr('action', url);
    $('#approveAsk').text('Are you sure ? ');
    $('#approveTitle').text("You won't be able to revert this!");
    $("#approveIdForm").val(id);
    $('#approveModalById').modal();
};

function restoreByIdModal(id, url, message = "You won't be able to revert this!") {
    $('#restoreForm').attr('action', url);
    $('#restoreAsk').text('Are you sure ? ');
    $('#restoreTitle').text(message);
    $("#restoreIdForm").val(id);
    $('#restoreModalById').modal();
}
//format price number
$(document).on("keyup", '.input_price', function(event) {
    if (event.which >= 37 && event.which <= 40) return;
    $(this).val(function(index, value) {
        return value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    });
});

function numberComman() {
    $('.input_price').val(function(index, value) {
        return value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    });
}

function showOneImage(e) {
    var src = $(e).data('src');
    var alt = $(e).data('alt');
    swal({
      imageUrl: src,
      imageHeight: 'auto',
      imageAlt: alt
    })
}

function logout() {
    swal({
        title: 'Logout',
        text: 'Do you want to log out?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes!'
    }).then((result) => {
        console.log(result)
        if (result == true) {
            return $.ajax({
                type: 'post',
                url: '/admin/auth/logout',
                data: {
                    '_token':$('meta[name="csrf-token"]').attr('content')
                }
            }).then(response => {
                window.location.href = "/admin/auth/login";
            }).catch(error => {
                swal.showValidationError(`An error ocurred: ${error.status}`)
            })
        }
    })
}