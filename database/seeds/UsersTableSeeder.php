<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Helper\HelperAdmin;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
        'name' => 'Admin',
        'slug' => 'admin',
        'email' => 'admin@gmail.com',
        'password' => 'admin',
        'phone' => '0962720730',
        'address' => 'Viet Nam',
        'admin' => 1,
        'title' => 'admin',
        ]);
        $user->syncPermissions(HelperAdmin::getListByRole('admin'));
    }
}
