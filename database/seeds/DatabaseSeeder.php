<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PermissionTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        
        \App\Models\BlogCategory::create(
            [
            'parent_id' => 0,
            'name' => 'All',
            'slug' => 'all',
            'status' => 1,
            ]);
    }
}
