<?php
return [
    'monetary_unit' => env('MONETARY_UNIT', 'VND'),
    'error_page' => [
        '403' =>'You can not access this page!',
        '404' =>'Page not found!',
        '500' =>'Error server!',
    ]
];
