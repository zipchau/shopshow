<?php
return [
    'nav' => [
        'GIỚI THIỆU',
        'DOANH NGHIỆP',
        'TIN TỨC - ƯU ĐÃI',
        'HỔ TRỢ VÀ LIÊN HỆ',
    ],
    'routeNav' =>[
        'GIỚI THIỆU' =>'aboutus',
        'DOANH NGHIỆP' =>'bussiness',
        'TIN TỨC - ƯU ĐÃI' => 'blog',
        'HỔ TRỢ VÀ LIÊN HỆ' => 'faq',
    ],
];
