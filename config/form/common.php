<?php
return [
    'paginate'  => 15,

    'tag_span' => [
        1 => 'primary',
        2 => 'success',
        3 => 'info',
        4 => 'warning',
        5 => 'danger',
    ],

];
