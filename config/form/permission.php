<?php
return [
    'permissions' => [
        //handle blog
        'blog-category-control',
        'blog-show',
        'blog-create',
        'blog-edit',
        'blog-remove',

        //handle product
        'product-category-control',
        'product-show',
        'product-create',
        'product-edit',
        'product-remove',

        //handle user
        'user-control',

        //only-admin
        'admin-control',
    ],

    'title'       => [
        'member'    => 'Member',
        'moderator' => 'Moderator',
        'admin'     => 'Admin',
        'lock'      => 'Lock',
    ],

    'roles'       => [
        'member'    => [
            'blog-show',
            'product-show',
        ],
        'moderator' => [
            'blog-category-control',
            'blog-show',
            'blog-create',
            'blog-edit',
            'blog-remove',
            'product-category-control',
            'product-show',
            'product-create',
            'product-edit',
            'product-remove',
        ],
        'admin'     => 'all-permission',
    ],
];
