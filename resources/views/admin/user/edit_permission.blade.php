@extends('admin.layout.master',[
    'title'=> 'Edit Permission',
    'breadcrumb'=>[
        [
            'name' => 'User',
            'src' => route('admin.user.index')
        ],
        [
            'name' => 'Edit Permission'
        ]
    ]
    ])
@section('content')
<div class="col-lg-12">
    <div class="grid">
        <p class="grid-header">
            Edit Permission User : {{$user->name}} ({{$user->email}})
        </p>
        <form action="{{route('admin.user.update_permission',['id'=>$user->id])}}" method="POST">
            @csrf
                @method('PUT')
            <div class="grid-body">
                <div class="item-wrapper">
                    <div class="row mb-3">
                        <div class="col-md-12 mx-auto">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <div class="form-group row showcase_row_area">
                                <div class="col-md-3 showcase_text_area">
                                    <label for="inputName">
                                        Permission :
                                    </label>
                                </div>
                                <div class="col-md-9 showcase_content_area">
                                    <div class="form-group">
                                        @foreach($permissions as $key => $permission)
                                        <div class="checkbox">
                                            <label>
                                                <input class="form-check-input" name="permission[]" value="{{$permission}}" {{in_array($permission,$userPermission) ? 'checked' : ''}} type="checkbox">
                                                    {{ucfirst(str_replace('-',' ',$permission))}}
                                                    <i class="input-frame">
                                                    </i>
                                                </input>
                                            </label>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-sm btn-primary">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@stop