@extends('admin.layout.master',[
    'title'=> isset($user->name) ? 'Update User '.$user->name : 'Create User',
    'breadcrumb'=>[
        [
            'name' => 'User',
            'src' => route('admin.user.index')
        ],
        [
            'name' => isset($user->name) ? 'Update User '.$user->name : 'Create User'
        ]
    ]
    ])
@section('content')
<div class="col-lg-12">
    <div class="grid">
        <p class="grid-header">
            {{isset($user->name) ? 'Update User '.$user->name : 'Create User'}}
        </p>
        <form action="{{isset($user->id) ? route('admin.user.update',['id'=>$user->id]) : route('admin.user.store')}}" method="POST">
            @csrf
            @isset($user->id)
                @method('PUT')
            @endisset
            <div class="grid-body">
                <div class="item-wrapper">
                    <div class="row mb-3">
                            <div class="col-md-12 mx-auto">
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                            </div>
                        <div class="col-md-12 mx-auto">
                            <div class="form-group row showcase_row_area">
                                <div class="col-md-3 showcase_text_area">
                                    <label>
                                        Email
                                        <span class="text-danger">*</span>
                                    </label>
                                </div>
                                <div class="col-md-9 showcase_content_area">
                                    <input class="form-control" name="email" type="email" value="{{old('email',$user->email ?? '')}}" placeholder="Email" />
                                </div>
                            </div>
                            <div class="form-group row showcase_row_area">
                                <div class="col-md-3 showcase_text_area">
                                    <label>
                                        Password
                                        <span class="text-danger">*</span>
                                    </label>
                                </div>
                                <div class="col-md-9 showcase_content_area">
                                    <input class="form-control" name="password" type="password" placeholder="Password" />
                                </div>
                            </div>
                            <div class="form-group row showcase_row_area">
                                <div class="col-md-3 showcase_text_area">
                                    <label>
                                        Password Confirm
                                        <span class="text-danger">*</span>
                                    </label>
                                </div>
                                <div class="col-md-9 showcase_content_area">
                                    <input class="form-control" name="password_confirmation" type="password" placeholder="Password Confirm" />
                                </div>
                            </div>
                            <div class="form-group row showcase_row_area">
                                <div class="col-md-3 showcase_text_area">
                                    <label>
                                        Name
                                        <span class="text-danger">*</span>
                                    </label>
                                </div>
                                <div class="col-md-9 showcase_content_area">
                                    <input class="form-control" name="name" type="text" value="{{old('name',$user->name ?? '')}}" placeholder="User Name" />
                                </div>
                            </div>
                            <div class="form-group row showcase_row_area">
                                <div class="col-md-3 showcase_text_area">
                                    <label>
                                        Phone
                                        <span class="text-danger">*</span>
                                    </label>
                                </div>
                                <div class="col-md-9 showcase_content_area">
                                    <input class="form-control" name="phone" type="text" value="{{old('phone',$user->phone ?? '')}}" placeholder="Phone" />
                                </div>
                            </div>
                            <div class="form-group row showcase_row_area">
                                <div class="col-md-3 showcase_text_area">
                                    <label>
                                        Address
                                    </label>
                                </div>
                                <div class="col-md-9 showcase_content_area">
                                    <input type="text" class="form-control" name="address" value="{{old('address',$user->address ?? '')}}">
                                </div>
                            </div>
                            <div class="form-group row showcase_row_area">
                                <div class="col-md-3 showcase_text_area">
                                    <label>
                                        Image
                                        <span class="text-danger">*</span>
                                    </label>
                                </div>
                                @php 
                                    $check_exist_images = old('images.src',$user->images->src ?? '') != '' ? true :false;
                                    // dd(old('images.src'));
                                @endphp
                                <div class="col-md-9 showcase_content_area">
                                    <div class="fileupload {{$check_exist_images ? "fileupload-exists" : "fileupload-new"}}" data-provides="fileupload">
                                        <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                            <img alt="image" class="img-fluid" src="{{url('adstyle/images/imagenotfound.png')}}" onclick="showOneImage(this)" data-src="{{url('adstyle/images/imagenotfound.png')}}">
                                            </img>
                                        </div>
                                        <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 0px;">
                                            @if($check_exist_images)
                                            <img alt="image" class="img-fluid" src="{{url(old('images.src',$user->images->src ?? 'adstyle/images/imagenotfound.png'))}}" onclick="showOneImage(this)" data-src="{{url(old('images.src',$user->images->src ?? 'adstyle/images/imagenotfound.png'))}}">
                                            @endif
                                        </div>
                                        <div>
                                            <button class="btn btn-primary btn-file" type="button" data-original="{{$user->images->src ?? ''}}" data-insert="{{route('admin.image.uploadone')}}" data-remove="{{route('admin.image.removebysrc')}}">
                                                <span class="fileupload-new">
                                                    <i class="mdi mdi-upload">
                                                    </i>
                                                    Select image
                                                </span>
                                                <span class="fileupload-exists">
                                                    <i class="mdi mdi-refresh">
                                                    </i>
                                                    Change
                                                </span>
                                                <input class="btn-light" type="file" accept="image/x-png,image/gif,image/jpeg">
                                                <input type="hidden" class="input-src" name="images[src]" value="{{old('images.src',$user->images->src ?? '')}}" >
                                            </button>
                                            <button type="button" class="btn btn-danger fileupload-exists" data-dismiss="fileupload">
                                                <i class="mdi mdi-delete">
                                                </i>
                                                Remove
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row showcase_row_area">
                                <div class="col-md-3 showcase_text_area">
                                    <label>
                                        Title
                                        <span class="text-danger">*</span>
                                    </label>
                                </div>
                                <div class="col-md-9 showcase_content_area">
                                    <select name="title" class="form-control">
                                        @foreach(config('form.permission.title') as $key => $value)
                                        <option value="{{$key}}" {{old('title', $user->title ?? '') == $key ? 'selected' : ''}}>{{$value}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-sm btn-primary">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@stop
@section('script')
<script src="/adstyle/js/bootstrap-fileupload.js"></script>
@endsection
