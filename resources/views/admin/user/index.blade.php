@extends('admin.layout.master',[
    'title'=>'User',
    'breadcrumb'=>[
        [
            'name' => 'User',
        ]
    ],
    'tablesaw'=>true,
])
@section('content')
<div class="row">
    <div class="col-lg-12">
        <a href="{{route('admin.user.create')}}" class="btn btn-primary has-icon">
            <i class="mdi mdi-plus">
            </i>
            Add new
        </a>
    </div>
    <div class="col-lg-12 mt-3">
        <div class="grid">
            <p class="grid-header">
                List User
            </p>
            <div class="grid-body">
                <div class="item-wrapper">
                    <div class="table-responsive">
                         <table class="table table-striped table-hover" data-tablesaw-sortable>
                            <thead>
                                <tr>
                                    <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="1">#</th>
                                    <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="2">Name</th>
                                    <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="3">Email</th>
                                    <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4">Title</th>
                                    <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="5">Approve</th>
                                    <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="6">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $key => $value)
                                <tr>
                                    <td>
                                        {{$key+1}}
                                    </td>
                                    <td>
                                        {{$value->name}}
                                    </td>
                                    <td>
                                        {{$value->email}}
                                    </td>
                                    <td>
                                        {{$value->title_text}}
                                    </td>
                                    <td>
                                        @if($value->approved())
                                            {{$value->userApprove->name}}
                                        @else
                                            <a href="#" onclick="approveByIdModal('{{$value->id}}','{{route('admin.user.approve',['id'=>$value->id])}}')">
                                                <i class="mdi mdi-backup-restore">Approve now</i>
                                            </a>
                                        @endif
                                    </td>
                                    <td>
                                        @if($value->trashed())
                                        <a href="javascript:restoreByIdModal('{{$value->id}}','{{route('admin.user.restore',['id'=>$value->id])}}')">
                                            <i class="mdi mdi-backup-restore">Restore</i>
                                        </a>
                                        @else
                                        <a href="{{route('admin.user.show',['id'=>$value->id])}}"><i class="mdi mdi-eye">Show</i></a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('modal')
@include('admin.layout.include.restoreModal')
@include('admin.user.modal.approveModal')
@stop
