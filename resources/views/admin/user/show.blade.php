@extends('admin.layout.master',['title'=>$user->name.' | User'])
@section('content')
<div class="col-lg-12">
    <div class="grid">
        <p class="grid-header"> Information User </p>
        <div class="row">
            <div class="col-lg-8 equel-grid">
                <div class="grid">
                    <div class="grid-body">
                        <div class="d-flex justify-content-between align-items-center">
                            <h4 class="card-title">
                                    Name : {{$user->name}}
                                    <br>
                            </h4>
                            <div class="fc-right">
                                <a href="{{route('admin.user.edit',['id'=>$user->id])}}" class="btn btn-info btn-sm">
                                    Edit
                                </a>
                                <a href="{{route('admin.user.edit_permission',['id'=>$user->id])}}" class="btn btn-warning btn-sm">
                                    Edit Permission
                                </a>
                                <button class="btn btn-danger btn-sm" onclick="deleteByIdModal('{{$user->id}}','{{route('admin.user.destroy',['id'=>$user->id])}}')">
                                    Delete
                                </button>
                            </div>
                        </div>
                        <div class="event-list-wrapper">
                            <div class="event-list">
                                <p class="event-text">
                                    Title : {{$user->title_text}}
                                </p>
                            </div>
                            <div class="event-list">
                                <p class="event-text">
                                    Email : {{$user->email}}
                                </p>
                            </div>
                            <div class="event-list">
                                <p class="event-text">
                                    Phone : {{$user->phone}}
                                </p>
                            </div>
                            <div class="event-list">
                                <p class="event-text">
                                    Address : {{$user->address}}
                                </p>
                            </div>
                            <div class="event-list">
                                <div class="item-wrapper demo-wrapper">
                                    Permission : {!! HelperAdmin::showPermissionTags($user->getAllPermissions()) !!}
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('modal')
@include('admin.layout.include.deleteModal')
@stop

