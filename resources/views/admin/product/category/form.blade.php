@extends('admin.layout.master',[
    'title'=> isset($category->name) ? 'Update Product Category '.$category->name : 'Create Product Category',
    'breadcrumb'=>[
        [
            'name' => 'Product Category',
            'src' => route('admin.productcategory.index')
        ],
        [
            'name' => isset($category->name) ? 'Update Product Category '.$category->name : 'Create Product Category'
        ]
    ]
    ])
@section('content')
<div class="col-lg-12">
    <div class="grid">
        <p class="grid-header">
            {{isset($category->name) ? 'Update Product Category '.$category->name : 'Create Product Category'}}
        </p>
        <form action="{{isset($category->id) ? route('admin.productcategory.update',['id'=>$category->id]) : route('admin.productcategory.store')}}" method="POST">
            @csrf
            @isset($category->id)
                @method('PUT')
            @endisset
            <div class="grid-body">
                <div class="item-wrapper">
                    <div class="row mb-3">
                        <div class="col-md-12 mx-auto">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        </div>
                        <div class="col-md-12 mx-auto">
                            <div class="form-group row showcase_row_area">
                                <div class="col-md-3 showcase_text_area">
                                    <label for="inputType13">
                                        Parent list
                                    </label>
                                </div>
                                <div class="col-md-9 showcase_content_area">
                                    <div class="form-group">
                                        <select class="form-control" id="js-select-example" name="parent_id">
                                            @foreach($parentList as $key => $value)
                                                <option value="{{$value['id']}}" {{old('parent_id', $category->parent_id ?? 0) == $value['id'] ? 'selected' : ''}}>
                                                    {{$value['name']}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 mx-auto">
                            <div class="form-group row showcase_row_area">
                                <div class="col-md-3 showcase_text_area">
                                    <label for="inputName">
                                        Name
                                    </label>
                                </div>
                                <div class="col-md-9 showcase_content_area">
                                    <input class="form-control" id="inputName" name="name" type="text" value="{{old('name',$category->name ?? '')}}" placeholder="Product Category Name" />
                                </div>
                            </div>
                            <div class="row showcase_row_area">
                                <div class="col-md-3 showcase_text_area">
                                    <label>
                                        Status
                                    </label>
                                </div>
                                <div class="col-md-9 showcase_content_area">
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input class="form-check-input" type="checkbox" name="status" value="2" {{old('status',$category->status ?? null) == '2' ? 'checked' : '' }} >
                                                    Hidden
                                                    <i class="input-frame"></i>
                                                </input>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-sm btn-primary">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@stop