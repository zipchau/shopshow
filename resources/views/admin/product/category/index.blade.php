@extends('admin.layout.master',[
    'title'=>'Product Category',
    'breadcrumb'=>[
        [
            'name' => 'Product Category',
            'src' => route('admin.productcategory.index')
        ]
    ],
    'tablesaw' => true,
])
@section('content')
<div class="row">
    <div class="col-lg-12">
        <a href="{{route('admin.productcategory.create')}}" class="btn btn-primary has-icon">
            <i class="mdi mdi-plus">
            </i>
            Add new
        </a>
    </div>
    <div class="col-lg-12 mt-3">
        <div class="grid">
            <p class="grid-header">
                List Product Category
            </p>
            <div class="grid-body">
                <div class="item-wrapper">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover" data-tablesaw-sortable>
                            <thead>
                                <tr>
                                    <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="1">#</th>
                                    <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="2">Name</th>
                                    <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="3">Parent</th>
                                    <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4">Status</th>
                                    <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="5">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($categories as $key => $value)
                                <tr>
                                    <td>
                                        {{$key+1}}
                                    </td>
                                    <td>
                                        {{$value->name}}
                                    </td>
                                    <td>
                                        {{$value->parent->name ?? ($value->parent_id != 0 ? "Error Parent" : "No Parent")}}
                                    </td>
                                    <td>
                                        {{$value->status_text}}
                                    </td>
                                    <td>
                                        @if($value->trashed())
                                        <a href="javascript:restoreByIdModal('{{$value->id}}','{{route('admin.productcategory.restore',['id'=>$value->id])}}')">
                                            <i class="mdi mdi-backup-restore">Restore</i> (Deleted at : {{HelperAdmin::showTime($value->deleted_at,'d/m/Y H:i:s')}})
                                        </a>
                                        @else
                                        <a href="{{route('admin.productcategory.show',['id'=>$value->id])}}"><i class="mdi mdi-eye">Show</i></a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('modal')
@include('admin.layout.include.restoreModal')
@stop
