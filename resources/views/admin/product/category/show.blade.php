@extends('admin.layout.master',['title'=>$category->name.' | Product Category'])
@section('content')
<div class="col-lg-12">
    <div class="grid">
        <p class="grid-header"> Information Product Category </p>
        <div class="row">
            <div class="col-lg-6 equel-grid">
                <div class="grid">
                    <div class="grid-body">
                        <div class="d-flex justify-content-between align-items-center">
                            <h4 class="card-title">
                                    Name : {{$category->name}}
                            </h4>
                            <div class="fc-right">
                                <a href="{{route('admin.productcategory.edit',['id'=>$category->id])}}" class="btn btn-info btn-sm">
                                    Edit
                                </a>
                                @if($category->checkDelete())
                                <button class="btn btn-danger btn-sm" onclick="deleteByIdModal('{{$category->id}}','{{route('admin.category.destroy',['id'=>$category->id])}}')">
                                    Delete
                                </button>
                                @endif
                            </div>
                        </div>
                        <div class="event-list-wrapper">
                            <div class="event-list">
                                <p class="event-text">
                                    Parent : {{$category->parent->name ?? 'No Parent'}}
                                </p>
                            </div>
                            <div class="event-list">
                                <p class="event-text">
                                    Status : {{$category->status == 2 ? 'Hidden' : 'Show'}}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 equel-grid">
                <div class="grid">
                    <div class="grid-body">
                        <div class="d-flex justify-content-between align-items-center">
                            <h4 class="card-title">
                                Product
                            </h4>
                        </div>
                        <div class="event-list-wrapper">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Status</th>
                                            <th>Price</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($category->products->take(5) as $keyProduct => $product)
                                        <tr>
                                           <td>{{$keyProduct + 1}}</td>
                                           <td>{{$product->name}}</td>
                                           <td>{{$product->status_text}}</td>
                                           <td>{!!$product->price_html!!}</td>
                                           <td>
                                            @if($product->trashed())
                                                Deleted
                                                @else
                                                <a href="{{route('admin.product.show',['id'=>$product->id])}}"><i class="mdi mdi-eye">Show</i></a>
                                                @endif
                                           </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('modal')
@include('admin.layout.include.deleteModal')
@stop

