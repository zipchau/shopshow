@extends('admin.layout.master',[
    'title'=>'Product',
    'breadcrumb'=>[
        [
            'name' => 'Product',
            'src' => route('admin.product.index')
        ]
    ],
    'tablesaw' => true,
])
@section('content')
<div class="row">
    <div class="col-lg-12">
        @can('product-create')
        <a href="{{route('admin.product.create')}}" class="btn btn-primary has-icon">
            <i class="mdi mdi-plus">
            </i>
            Add new
        </a>
        @endcan
    </div>
    <div class="col-lg-12 mt-3">
        <div class="grid">
            <p class="grid-header">
                List Product
            </p>
            <div class="grid-body">
                <div class="item-wrapper">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover" data-tablesaw-sortable>
                            <thead>
                                <tr>
                                    <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="1">#</th>
                                    <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="2">Name</th>
                                    <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="3">Category</th>
                                    <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4">Public at</th>
                                    <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="5">Price</th>
                                    <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="6">Status</th>
                                    <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="7">Update at</th>
                                    <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="8">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($products as $key => $value)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$value->name}}</td>
                                    <td>{{$value->category->name ?? "Error Category"}}</td>
                                    <td>{{HelperAdmin::showTime($value->public_at)}}</td>
                                    <td>{!!$value->price_html!!}</td>
                                    <td>{{$value->status_text}}</td>
                                    <td>{{$value->updated_at->diffForHumans()}}</td>
                                    <td>
                                        @if($value->trashed())
                                        @can('product-remove')
                                            <a href="javascript:restoreByIdModal('{{$value->id}}','{{route('admin.product.restore',['id'=>$value->id])}}')">
                                                <i class="mdi mdi-backup-restore">Restore</i> (Deleted at : {{HelperAdmin::showTime($value->deleted_at,'d/m/Y H:i:s')}})
                                            </a>
                                        @endcan
                                        @else
                                        <a href="{{route('admin.product.show',['id'=>$value->id])}}"><i class="mdi mdi-eye">Show</i></a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('modal')
@include('admin.layout.include.restoreModal')
@stop
