@extends('admin.layout.master',[
    'title'=> isset($product->title) ? 'Update Product '.$product->title : 'Create Product',
    'breadcrumb'=>[
        [
            'name' => 'Product',
            'src' => route('admin.product.index')
        ],
        [
            'name' => isset($product->title) ? 'Update Product '.$product->title : 'Create Product'
        ]
    ]
    ])
@section('content')
<form action="{{isset($product->id) ? route('admin.product.update',['id'=>$product->id]) : route('admin.product.store')}}" method="POST">
@csrf
@isset($product->id)
    @method('PUT')
@endisset
<div class="row col-lg-12">
    <div class="col-lg-8">
        <div class="grid">
            <p class="grid-header">
                {{isset($product->title) ? 'Update Product '.$product->title : 'Create Product'}}
            </p>
            <div class="grid-body">
                <div class="item-wrapper">
                    <div class="row mb-3">
                        <div class="col-md-12 mx-auto">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <div class="form-group row showcase_row_area">
                                <div class="col-md-3 showcase_text_area">
                                    <label for="inputType13">
                                        Category List
                                        <span class="text-danger">*</span>
                                    </label>
                                </div>
                                <div class="col-md-9 showcase_content_area">
                                    <div class="form-group">
                                        <select class="form-control" id="js-select-example" name="product_category_id">
                                            <option value="">Choose product category</option>
                                            @foreach($categories as $key => $value)
                                                <option value="{{$value['id']}}" {{old('product_category_id', $product->product_category_id ?? 0) == $value['id'] ? 'selected' : ''}}>
                                                    {{$value['name']}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 mx-auto">
                            <div class="form-group row showcase_row_area">
                                <div class="col-md-3 showcase_text_area">
                                    <label for="inputPubicAt">
                                        Public at
                                    </label>
                                </div>
                                <div class="col-md-9 showcase_content_area">
                                    <input class="form-control" id="inputPubicAt" name="public_at" type="date" value="{{HelperAdmin::timeNowOrOld(old('public_at',$product->public_at ?? null))}}" placeholder="Public at" />
                                </div>
                            </div>
                            <div class="form-group row showcase_row_area">
                                <div class="col-md-3 showcase_text_area">
                                    <label for="inputTitle">
                                        Name
                                        <span class="text-danger">*</span>
                                    </label>
                                </div>
                                <div class="col-md-9 showcase_content_area">
                                    <input class="form-control" id="inputName" name="name" type="text" value="{{old('name',$product->name ?? '')}}" placeholder="Product Name" />
                                </div>
                            </div>
                            <div class="form-group row showcase_row_area">
                                <div class="col-md-3 showcase_text_area">
                                    <label for="inputPriceRegular">
                                        Price regular
                                        <span class="text-danger">*</span>
                                    </label>
                                </div>
                                <div class="col-md-9 input-group">
                                    <input class="form-control input_price" name="price_regular" value="{{old('price_regular',$product->price_regular ?? 0)}}" type="text" placeholder="Price regular">
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                {{config('common.monetary_unit')}}
                                            </div>
                                        </div>
                                    </input>
                                </div>
                            </div>
                            <div class="form-group row showcase_row_area">
                                <div class="col-md-3 showcase_text_area">
                                    <label for="inputPriceSale">
                                        Price sale
                                        <span class="text-danger">*</span>
                                    </label>
                                </div>
                                <div class="col-md-9 input-group">
                                    <input class="form-control input_price" name="price_sale" value="{{old('price_sale',$product->price_sale ?? 0)}}" type="text" placeholder="Price sale">
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                {{config('common.monetary_unit')}}
                                            </div>
                                        </div>
                                    </input>
                                </div>
                            </div>
                            <div class="form-group row showcase_row_area">
                                <div class="col-md-3 showcase_text_area">
                                    <label for="inputContent">
                                        Content
                                        <span class="text-danger">*</span>
                                    </label>
                                </div>
                                <div class="col-md-9 showcase_content_area">
                                    <textarea name="content" id="textarea_ckediter" rows="10" cols="80">{!! old('content',$product->content ?? '') !!}</textarea>
                                </div>
                            </div>
                            <div class="form-group row showcase_row_area">
                                <div class="col-md-3 showcase_text_area">
                                    <label for="inputType12">
                                        Tags
                                    </label>
                                </div>
                                <div class="col-md-9 showcase_content_area mb-0">
                                    <input class="form-control" data-role="tagsinput" type="text" name="tags" value="{{old('tags',$product->tags ?? '')}}" placeholder="Tags" />
                                </div>
                            </div>
                            <div class="row showcase_row_area">
                                <div class="col-md-3 showcase_text_area">
                                    <label>
                                        Status
                                    </label>
                                </div>
                                <div class="col-md-9 showcase_content_area">
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input class="form-check-input" type="checkbox" name="status" value="2" {{old('status',$product->status ?? null) == '2' ? 'checked' : '' }} >
                                                    Hidden
                                                    <i class="input-frame"></i>
                                                </input>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-sm btn-primary">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @php
        $dataImage = old('file_uploads',isset($product->images) ? $product->images()->select(['src','status','id'])->get()->toArray() : [] );
    @endphp
    @include('admin.accessory.uploadMultiple',['dataImage'=>$dataImage])
</div>
</form>
@stop
@section('script')
<script src="/adstyle/js/imagemultiplehandle.js"></script>
<script>
CKEDITOR.replace('textarea_ckediter',options);
</script>
@stop
