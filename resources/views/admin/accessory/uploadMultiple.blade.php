<div class="col-lg-4">
    <div class="grid">
        <p class="grid-header">
            Upload Image
            <span class="text-danger">*</span>
        </p>
        <div class="grid-body">
            <div class="row showcase_row_area">
                <div class="custom-file">
                    <input type="file" class="custom-file-input" onchange="UploadImageMultiple(this)" multiple id="customFile">
                    <label class="custom-file-label" for="customFile">Choose file</label>
                </div>
            </div>
            <div class="row showcase_row_area">
                <div class="custom-file">
                    <div class="progress">
                        <div class="progress-bar bg-success progressBarUploadImage" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"><span class="progressBarUploadImageShow">0%</span></div>
                    </div>
                </div>
            </div>
            <div class="row showcase_row_area errorUploadImageResult">
            </div>
        </div>
    </div>
    <div class="grid">
        <p class="grid-header">
            List Image
        </p>
        <div class="grid-body ">
            <ul class="list-unstyled listUploadImageResult">
                @foreach($dataImage as $keyImage => $valueImage)
                <div class="media imageResultShow">
                    <img class="mr-3" src="{{url($valueImage['src'])}}" onclick="showOneImage(this)" data-src="{{url($valueImage['src'])}}" data-atl="" data-height="100%" data-width="100%" style="width:70px">
                        <div class="media-body">
                            <div class="form-group">
                                <div class="row col-md-12">
                                    <div class="col-md-9">
                                        <input type="text" class="form-control enable-mask inputSrc" value="{{url($valueImage['src'])}}" disabled>
                                    </div>
                                    <div class="col-md-3">
                                        <button type="button" class="btn btn-inverse-danger" onclick="removePedding(this)">Remove</button>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="checkbox">
                                            <label>
                                                <input class="form-check-input" type="checkbox" name="file_uploads[{{$keyImage +1}}][status]" value="2" {{ isset($valueImage['status']) && $valueImage['status'] == 2 ? 'checked' : ''}}>
                                                    Set index
                                                    <i class="input-frame"></i>
                                                </input>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @isset($valueImage['id'])
                            <input name="file_uploads[{{$keyImage +1}}][id]" type="hidden" value="{{$valueImage['id']}}"/>
                            @endisset
                            <input name="file_uploads[{{$keyImage +1}}][src]" type="hidden" value="{{$valueImage['src']}}"/>
                        </div>
                    </img>
                </div>
                @endforeach
            </ul>
        </div>
    </div>
</div>

