@extends('admin.layout.master',[
    'title'=> isset($blogBanner->name) ? 'Update Blog Banner '.$blogBanner->name : 'Create Blog Banner',
    'breadcrumb'=>[
        [
            'name' => 'Blog Banner',
            'src' => route('admin.blogbanner.index')
        ],
        [
            'name' => isset($blogBanner->name) ? 'Update Blog Banner '.$blogBanner->name : 'Create Blog Banner'
        ]
    ]
    ])
@section('content')
<div class="col-lg-12">
    <div class="grid">
        <p class="grid-header">
            {{isset($blogBanner->name) ? 'Update Blog Banner '.$blogBanner->name : 'Create Blog Banner'}}
        </p>
        <form action="{{isset($blogBanner->id) ? route('admin.blogbanner.update',['id'=>$blogBanner->id]) : route('admin.blogbanner.store')}}" method="POST">
            @csrf
            @isset($blogBanner->id)
                @method('PUT')
            @endisset
            <div class="grid-body">
                <div class="item-wrapper">
                    <div class="row mb-3">
                        <div class="col-md-12 mx-auto">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        </div>
                        <div class="col-md-12 mx-auto">
                            <div class="form-group row showcase_row_area">
                                <div class="col-md-3 showcase_text_area">
                                    <label>
                                        Image
                                        <span class="text-danger">*</span>
                                    </label>
                                </div>
                                @php 
                                    $check_exist_images = old('images.src',$blogBanner->images->src ?? '') != '' ? true :false;
                                @endphp
                                <div class="col-md-9 showcase_content_area">
                                    <div class="fileupload {{$check_exist_images ? "fileupload-exists" : "fileupload-new"}}" data-provides="fileupload">
                                        <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                            <img alt="image" class="img-fluid" src="{{url('adstyle/images/imagenotfound.png')}}" onclick="showOneImage(this)" data-src="{{url('adstyle/images/imagenotfound.png')}}">
                                            </img>
                                        </div>
                                        <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 0px;">
                                            @if($check_exist_images)
                                            <img alt="image" class="img-fluid" src="{{url(old('images.src',$blogBanner->images->src ?? 'adstyle/images/imagenotfound.png'))}}" onclick="showOneImage(this)" data-src="{{url(old('images.src',$blogBanner->images->src ?? 'adstyle/images/imagenotfound.png'))}}">
                                            @endif
                                        </div>
                                        <div>
                                            <button class="btn btn-primary btn-file" type="button" data-original="{{$blogBanner->images->src ?? ''}}" data-insert="{{route('admin.image.uploadone')}}" data-remove="{{route('admin.image.removebysrc')}}">
                                                <span class="fileupload-new">
                                                    <i class="mdi mdi-upload">
                                                    </i>
                                                    Select image
                                                </span>
                                                <span class="fileupload-exists">
                                                    <i class="mdi mdi-refresh">
                                                    </i>
                                                    Change
                                                </span>
                                                <input class="btn-light" type="file" accept="image/x-png,image/gif,image/jpeg">
                                                <input type="hidden" class="input-src" name="images[src]" value="{{old('images.src',$blogBanner->images->src ?? '')}}" >
                                            </button>
                                            <button type="button" class="btn btn-danger fileupload-exists" data-dismiss="fileupload">
                                                <i class="mdi mdi-delete">
                                                </i>
                                                Remove
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row showcase_row_area">
                                <div class="col-md-3 showcase_text_area">
                                    <label for="inputPubicAt">
                                        Public at
                                    </label>
                                </div>
                                <div class="col-md-9 showcase_content_area">
                                    <input class="form-control" id="inputPubicAt" name="public_at" type="date" value="{{HelperAdmin::timeNowOrOld(old('public_at',$blogBanner->public_at ?? null))}}" placeholder="Public at" />
                                </div>
                            </div>
                            <button type="submit" class="btn btn-sm btn-primary">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@stop
@section('script')
<script src="/adstyle/js/bootstrap-fileupload.js"></script>
@stop