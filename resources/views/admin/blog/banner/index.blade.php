@extends('admin.layout.master',[
    'title'=>'Blog Banner',
    'breadcrumb'=>[
        [
            'name' => 'Blog Banner',
        ]
    ]
])
@section('content')
<style>
td:hover{
cursor:move;
}
.bd-placeholder-img {
  font-size: 1.125rem;
  text-anchor: middle;
}
</style>
<div class="row">
    <div class="col-lg-12">
        <a href="{{route('admin.blogbanner.create')}}" class="btn btn-primary has-icon">
            <i class="mdi mdi-plus">
            </i>
            Add new
        </a>
        <button class="btn btn-warning has-icon" onclick="updatePrioritize('{{route('admin.blogbanner.update_position')}}')">Update Position</button>
    </div>
    <div class="col-lg-12 mt-3">
        <div class="grid">
            <p class="grid-header">
                List Blog Banner
            </p>
            <div class="grid-body">
                <div class="item-wrapper">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover" data-tablesaw-sortable id="tableDrag">
                            <thead>
                                <tr>
                                    <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="1">#</th>
                                    <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="2">Image</th>
                                    <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="3">Public_at</th>
                                    <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="6">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($sliders as $key => $value)
                                <tr>
                                    <td>
                                        <span class="position">{{$value->prioritize}}</span>
                                        <input type="hidden" name="prioritize" data-id="{{$value->id}}" id="index" value="{{$value->prioritize}}">
                                    </td>
                                    <td>
                                        <img class="bd-placeholder-img img-thumbnail" width="200" height="200" src="{{url($value->images->src ?? 'adstyle/images/imagenotfound.png')}}" alt="">
                                    </td>
                                    <td>
                                        {{$value->public_at}}
                                    </td>
                                    <td>
                                        @if($value->trashed())
                                        <a href="javascript:restoreByIdModal('{{$value->id}}','{{route('admin.blogbanner.restore',['id'=>$value->id])}}')">
                                            <i class="mdi mdi-backup-restore">Restore</i> (Deleted at : {{HelperAdmin::showTime($value->deleted_at,'d/m/Y H:i:s')}})
                                        </a>
                                        @else
                                        <a href="{{route('admin.blogbanner.edit',['id'=>$value->id])}}"><i class="mdi mdi-eye">Edit</i></a> |
                                        <a href="#" id="destroyItem" onclick="deleteByIdModal('{{$value->id}}','{{route('admin.blogbanner.destroy',['id'=>$value->id])}}')">
                                             <i class="mdi mdi-delete">Delete</i>
                                        </a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('script')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" integrity="sha256-T0Vest3yCU7pafRw9r+settMBX6JkKN06dqBnpQ8d30=" crossorigin="anonymous"></script>

<script src="/adstyle/js/draggable.js"></script>
@stop
@section('modal')
    @include('admin.layout.include.restoreModal')
    @include('admin.layout.include.deleteModal')
@stop