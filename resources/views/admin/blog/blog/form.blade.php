@extends('admin.layout.master',[
    'title'=> isset($blog->title) ? 'Update Blog '.$blog->title : 'Create Blog',
    'breadcrumb'=>[
        [
            'name' => 'Blog',
            'src' => route('admin.blog.index')
        ],
        [
            'name' => isset($blog->title) ? 'Update Blog '.$blog->title : 'Create Blog'
        ]
    ]
    ])
@section('content')
<form action="{{isset($blog->id) ? route('admin.blog.update',['id'=>$blog->id]) : route('admin.blog.store')}}" method="POST">
@csrf
@isset($blog->id)
    @method('PUT')
@endisset
<div class="row col-lg-12">
    <div class="col-lg-8">
        <div class="grid">
            <p class="grid-header">
                {{isset($blog->title) ? 'Update Blog '.$blog->title : 'Create Blog'}}
            </p>
            <div class="grid-body">
                <div class="item-wrapper">
                    <div class="row mb-3">
                        <div class="col-md-12 mx-auto">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <div class="form-group row showcase_row_area">
                                <div class="col-md-3 showcase_text_area">
                                    <label for="inputType13">
                                        Category List
                                        <span class="text-danger">*</span>
                                    </label>
                                </div>
                                <div class="col-md-9 showcase_content_area">
                                    <div class="form-group">
                                        <select class="form-control" id="js-select-example" name="blog_category_id">
                                            <option value="">Choose blog category</option>
                                            @foreach($categories as $key => $value)
                                                <option value="{{$value['id']}}" {{old('blog_category_id', $blog->blog_category_id ?? 0) == $value['id'] ? 'selected' : ''}}>
                                                    {{$value['name']}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 mx-auto">
                            <div class="form-group row showcase_row_area">
                                <div class="col-md-3 showcase_text_area">
                                    <label for="inputPubicAt">
                                        Public at
                                    </label>
                                </div>
                                <div class="col-md-9 showcase_content_area">
                                    <input class="form-control" id="inputPubicAt" name="public_at" type="date" value="{{HelperAdmin::timeNowOrOld(old('public_at',$blog->public_at ?? null))}}" placeholder="Public at" />
                                </div>
                            </div>
                            <div class="form-group row showcase_row_area">
                                <div class="col-md-3 showcase_text_area">
                                    <label for="inputTitle">
                                        Title
                                        <span class="text-danger">*</span>
                                    </label>
                                </div>
                                <div class="col-md-9 showcase_content_area">
                                    <input class="form-control" id="inputTitle" name="title" type="text" value="{{old('title',$blog->title ?? '')}}" placeholder="Blog Title" />
                                </div>
                            </div>
                            <div class="form-group row showcase_row_area">
                                <div class="col-md-3 showcase_text_area">
                                    <label for="inputContent">
                                        Content
                                        <span class="text-danger">*</span>
                                    </label>
                                </div>
                                <div class="col-md-9 showcase_content_area">
                                    <textarea name="content" id="textarea_ckediter" rows="10" cols="80">{!! old('content',$blog->content ?? '') !!}</textarea>
                                </div>
                            </div>
                            <div class="form-group row showcase_row_area">
                                <div class="col-md-3 showcase_text_area">
                                    <label for="inputType12">
                                        Tags
                                    </label>
                                </div>
                                <div class="col-md-9 showcase_content_area mb-0">
                                    <input class="form-control" data-role="tagsinput" type="text" name="tags" value="{{old('tags',$blog->tags ?? '')}}" placeholder="Tags" />
                                </div>
                            </div>
                            <div class="row showcase_row_area">
                                <div class="col-md-3 showcase_text_area">
                                    <label>
                                        Popular
                                    </label>
                                </div>
                                <div class="col-md-9 showcase_content_area">
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input class="form-check-input" type="checkbox" name="popular" value="2" {{old('popular',$blog->popular ?? null) == '2' ? 'checked' : '' }} >
                                                    Popular
                                                    <i class="input-frame"></i>
                                                </input>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row showcase_row_area">
                                <div class="col-md-3 showcase_text_area">
                                    <label>
                                        Status
                                    </label>
                                </div>
                                <div class="col-md-9 showcase_content_area">
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input class="form-check-input" type="checkbox" name="status" value="2" {{old('status',$blog->status ?? null) == '2' ? 'checked' : '' }} >
                                                    Hidden
                                                    <i class="input-frame"></i>
                                                </input>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-sm btn-primary">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @php
        $dataImage = old('file_uploads',isset($blog->images) ? $blog->images()->select(['src','status','id'])->get()->toArray() : [] );
    @endphp
    @include('admin.accessory.uploadMultiple',['dataImage'=>$dataImage])
</div>
</form>
@stop
@section('script')
<script src="/adstyle/js/imagemultiplehandle.js"></script>
<script>
CKEDITOR.replace('textarea_ckediter',options);
</script>
@stop
