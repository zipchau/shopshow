@extends('admin.layout.layoutAuth')

@section('content')
<div class="authentication-theme auth-style_3">
    <div class="row inner-wrapper">
        <div class="col-md-5 mx-auto form-section">
            <div class="logo-section">
                <a class="logo" href="/adstyle/index.html">
                    <img alt="logo" src="/adstyle/images/logo.svg"/>
                </a>
            </div>
            <form method="POST" action="{{ route('admin.auth.login') }}">
                @csrf
                <div class="form-group input-rounded">
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" placeholder="Email" value="{{ old('email') }}" required autofocus>
                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group input-rounded">
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" required>
                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-inline">
                    <div class="checkbox">
                        <label>
                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                {{ __('Remember Me') }}
                                <i class="input-frame">
                                </i>
                            </input>
                        </label>
                    </div>
                </div>
                <button class="btn btn-primary btn-block" type="submit">
                    {{ __('Login') }}
                </button>
            </form>
            <div class="signup-link">
                <p>
                    @if (Route::has('admin.auth.password.request'))
                        <a href="{{ route('admin.auth.password.request') }}">
                            {{ __('Forgot Your Password?') }}
                        </a>
                    @endif
                </p>
                @if (Route::has('admin.auth.register'))
                <a href="#">
                    Sign Up
                </a>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
