<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
            <meta content="width=device-width,initial-scale=1,shrink-to-fit=no" name="viewport">
                <title>
                    Login
                </title>
                <!-- plugins:css -->
                <link href="../../vendors/iconfonts/mdi/css/materialdesignicons.css" rel="stylesheet">
                    <link href="../../vendors/css/vendor.addons.css" rel="stylesheet">
                        <!-- endinject -->
                        <!-- vendor css for this page -->
                        <!-- End vendor css for this page -->
                        <!-- inject:css -->
                        <link href="../../css/style.css" rel="stylesheet">
                            <!-- endinject -->
                            <link href="../../images/favicon.ico" rel="shortcut icon"/>
                        </link>
                    </link>
                </link>
            </meta>
        </meta>
    </head>
    <body>
        <div class="authentication-theme auth-style_3">
            <div class="row inner-wrapper">
                <div class="col-md-5 mx-auto form-section">
                    <div class="logo-section">
                        <a class="logo" href="../../index.html">
                            <img alt="logo" src="../../images/logo.svg"/>
                        </a>
                    </div>
                    <form action="#">
                        <div class="form-group input-rounded">
                            <input class="form-control" placeholder="Username" type="text"/>
                        </div>
                        <div class="form-group input-rounded">
                            <input class="form-control" placeholder="Password" type="password"/>
                        </div>
                        <div class="form-inline">
                            <div class="checkbox">
                                <label>
                                    <input class="form-check-input" type="checkbox">
                                        Remember me
                                        <i class="input-frame">
                                        </i>
                                    </input>
                                </label>
                            </div>
                        </div>
                        <button class="btn btn-primary btn-block" type="submit">
                            Login
                        </button>
                    </form>
                    <div class="signup-link">
                        <p>
                            Don't have an account yet?
                        </p>
                        <a href="#">
                            Sign Up
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!--page body ends -->
        <!-- SCRIPT LOADING START FORM HERE /////////////-->
        <!-- plugins:js -->
        <script src="../../vendors/js/core.js">
        </script>
        <script src="../../vendors/js/vendor.addons.js">
        </script>
        <!-- endinject -->
        <!-- Vendor Js For This Page Ends-->
        <!-- Vendor Js For This Page Ends-->
        <script src="../../js/script.js">
        </script>
    </body>
</html>