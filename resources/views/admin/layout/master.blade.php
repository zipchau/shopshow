<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta content="width=device-width,initial-scale=1,shrink-to-fit=no" name="viewport">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="url-origin" content="{{url(request()->getPathInfo())}}">
        <title>Admin | {{$title ?? "Home"}}</title>
        <link href="/adstyle/vendors/iconfonts/mdi/css/materialdesignicons.css" rel="stylesheet">
        <link href="/adstyle/vendors/css/vendor.addons.css" rel="stylesheet">
        <link href="/adstyle/vendors/iconfonts/flag-icon-css/css/flag-icon.min.css" rel="stylesheet">
        <link href="/adstyle/vendors/sweet-alert/sweetalert2.min.css" rel="stylesheet">
        <link href="/adstyle/css/style.css" rel="stylesheet">
        <link href="/adstyle/css/custom.css" rel="stylesheet">
        <link href="/adstyle/images/favicon.ico" rel="shortcut icon"/>
        <script src="/adstyle/vendors/ckeditor/ckeditor.js"></script>
        @if(isset($tablesaw) && $tablesaw == true)
        <link href="/adstyle/vendors/tablesaw/css/tablesaw.css" rel="stylesheet" type="text/css" />
        @endif
        @yield('css')
    </head>
    <body class="header-fixed">
        <noscript>
            <div style="position: fixed; top: 0px; left: 0px; z-index: 3000;height: 100%; width: 100%; background-color: #FFFFFF">
                <p style="margin-left: 10px">JavaScript is not enabled.</p>
            </div>
        </noscript>
        @include('admin.layout.include.nav')
        <div class="page-body">
            @include('admin.layout.include.sidebar')
            <div class="page-content-wrapper">
                <div class="page-content-wrapper-inner">
                    <div class="viewport-header">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb has-arrow">
                                <li class="breadcrumb-item {{count($breadcrumb ?? []) == 0 ? 'active' :'' }}">
                                    <a href="{{route('admin.home')}}">Home</a>
                                </li>
                                @if(count($breadcrumb ?? []) != 0)
                                @foreach($breadcrumb ?? [] as $keyBreadcrumb => $valueBreadcrumb)
                                <li aria-current="page" class="breadcrumb-item {{$keyBreadcrumb == count($breadcrumb) - 1 ? 'active' : ''}}">
                                    @if($keyBreadcrumb == count($breadcrumb) - 1)
                                        {{$valueBreadcrumb['name']}}
                                    @else
                                        <a href="{{$valueBreadcrumb['src'] ?? ''}}" >{{$valueBreadcrumb['name']}}</a>
                                    @endif
                                </li>
                                @endforeach
                                @endif
                            </ol>
                        </nav>
                    </div>
                    <div class="content-viewport">
                        @yield('content')
                    </div>
                </div>
                @include('admin.layout.include.footer')
            </div>
        </div>
        @yield('modal')
        <script src="/adstyle/vendors/js/core.js"></script>
        <script src="/adstyle/vendors/js/vendor.addons.js"></script>
        <script src="/adstyle/vendors/sweet-alert/sweetalert2.min.js"></script>
        <script src="/adstyle/js/script.js"></script>
        <script src="/adstyle/js/common.js"></script>
        @if(isset($tablesaw) && $tablesaw == true)
        <!-- Tablesaw js -->
        <script src="/adstyle/vendors/tablesaw/js/tablesaw.js"></script>
        <script src="/adstyle/vendors/tablesaw/js/tablesaw-init.js"></script>
        @endif
        @yield('script')
    </body>
</html>
