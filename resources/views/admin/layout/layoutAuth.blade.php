<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta content="width=device-width,initial-scale=1,shrink-to-fit=no" name="viewport">
        <title>Admin | {{$title ?? 'Auth'}}</title>
        <!-- plugins:css -->
        <link href="/adstyle/vendors/iconfonts/mdi/css/materialdesignicons.css" rel="stylesheet">
        <link href="/adstyle/vendors/css/vendor.addons.css" rel="stylesheet">
        <!-- endinject -->
        <!-- vendor css for this page -->
        <!-- End vendor css for this page -->
        <!-- inject:css -->
        <link href="/adstyle/css/style.css" rel="stylesheet">
        <!-- endinject -->
        <link href="/adstyle/images/favicon.ico" rel="shortcut icon"/>
        @yield('css')
    </head>
    <body>
        @yield('content')
        
        <script src="/adstyle/vendors/js/core.js"></script>
        <script src="/adstyle/vendors/js/vendor.addons.js"></script>
        <script src="/adstyle/js/script.js"></script>
    </body>
</html>