<div class="modal fade" id="deleteModalById" role="dialog" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h4 class="modal-title text-white">
                    Destroy
                </h4>
                <button aria-label="Close" class="close text-white" data-dismiss="modal" type="button">
                    <span aria-hidden="true">
                        ×
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div class="d-flex flex-column justify-content-center align-items-center">
                    <i class="mdi mdi-alert-octagram mdi-6x text-danger">
                    </i>
                    <h4 class="text-black font-weight-medium mb-4">
                        <span class="destroyAsk">Are you sure ?</span>
                    </h4>
                    <p class="text-center">
                        <span class="destroyContent">You won't be able to revert this!</span>
                    </p>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-link text-black component-flat" data-dismiss="modal" type="button">
                    Dismiss
                </button>
                <form action="" id="destroyForm" method="post" accept-charset="utf-8">
                    @csrf
                    <input name="_method" type="hidden" value="Delete">
                    <input type="hidden" name="id" id="destroyIdForm">
                    <button class="btn btn-danger btn-sm" type="submit">
                        Yes, Continue
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>