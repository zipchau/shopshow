<div class="sidebar">
    <ul class="navigation-menu">
        <li class="nav-category-divider">
            MAIN
        </li>
        <li>
            <a href="{{route('admin.home')}}">
                <span class="link-title">
                    Dashboard
                </span>
                <i class="mdi mdi-gauge link-icon">
                </i>
            </a>
        </li>
        <li class="nav-category-divider">
            APPLICATION
        </li>
        @can('blog-show')
        <li>
            <a aria-expanded="false" data-toggle="collapse" href="#blog">
                <span class="link-title">
                    Blog
                </span>
                <i class="mdi mdi-note-text link-icon">
                </i>
            </a>
            <ul class="collapse navigation-submenu" id="blog">
                <li>
                    <a href="{{route('admin.blogbanner.index')}}">
                        Banner
                    </a>
                </li>
                @can('blog-category-control')
                <li>
                    <a href="{{route('admin.blogcategory.index')}}">
                        Blog Categories
                    </a>
                </li>
                @endcan
                <li>
                    <a href="{{route('admin.blog.index')}}">
                        Post
                    </a>
                </li>
                @can('blog-create')
                <li>
                    <a href="{{route('admin.blog.create')}}">
                        Create Post
                    </a>
                </li>
                @endcan
            </ul>
        </li>
        @endcan
        @can('product-show-hidden')
        <li>
            <a aria-expanded="false" data-toggle="collapse" href="#product">
                <span class="link-title">
                    Product
                </span>
                <i class="mdi mdi-gift link-icon">
                </i>
            </a>
            <ul class="collapse navigation-submenu" id="product">
                @can('product-category-control')
                <li>
                    <a href="{{route('admin.productcategory.index')}}">
                        Product Categories
                    </a>
                </li>
                @endcan
                <li>
                    <a href="{{route('admin.product.index')}}">
                        Product
                    </a>
                </li>
                @can('product-create')
                <li>
                    <a href="{{route('admin.product.create')}}">
                        Create Product
                    </a>
                </li>
                @endcan
            </ul>
        </li>
        @endcan
        <li class="nav-category-divider">
            Setting
        </li>
        <li>
            <li>
            <a aria-expanded="false" data-toggle="collapse" href="#brand">
                <span class="link-title">
                    Brand
                </span>
                <i class="mdi mdi-gift link-icon">
                </i>
            </a>
            <ul class="collapse navigation-submenu" id="brand">
                <li>
                    <a href="{{route('admin.brandcategory.index')}}">
                        Brand Categories
                    </a>
                </li>
                <li>
                    <a href="{{route('admin.brand.index')}}">
                        Brand
                    </a>
                </li>
                <li>
                    <a href="{{route('admin.brand.create')}}">
                        Create Brand
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-category-divider">
            USER
        </li>
        @can('user-control')
        <li>
            <a aria-expanded="false" data-toggle="collapse" href="#user">
                <span class="link-title">
                    User
                </span>
                <i class="mdi mdi-account-multiple link-icon">
                </i>
            </a>
            <ul class="collapse navigation-submenu" id="user">
                <li>
                    <a href="{{route('admin.user.index')}}">
                        User
                    </a>
                </li>
                <li>
                    <a href="{{route('admin.user.create')}}">
                        Create User
                    </a>
                </li>
            </ul>
        </li>
        @endcan
    </ul>
    {{-- account --}}
    <div class="sidebar_footer">
        <div class="user-account">
            <a class="user-profile-item" href="#">
                <i class="mdi mdi-account">
                </i>
                Profile
            </a>
            <a class="btn btn-primary btn-logout" href="javascript:logout()">
                Logout
            </a>
        </div>
        <div class="btn-group admin-access-level">
            <div class="avatar">
                <img alt="" class="profile-img" src="{{url(HelperAdmin::currentAdmin('image'))}}"/>
            </div>
            <div class="user-type-wrapper">
                <p class="user_name">
                    {{HelperAdmin::currentAdmin('name')}}
                </p>
                <div class="d-flex align-items-center">
                    <div class="status-indicator small rounded-indicator bg-success">
                    </div>
                    <small class="user_access_level">
                        {{HelperAdmin::currentAdmin('title_text')}}
                    </small>
                </div>
            </div>
            <i class="arrow mdi mdi-chevron-right">
            </i>
        </div>
    </div>
</div>
