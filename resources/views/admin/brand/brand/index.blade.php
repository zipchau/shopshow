@extends('admin.layout.master',[
    'title'=>'Brand',
    'breadcrumb'=>[
        [
            'name' => 'Brand',
            'src' => route('admin.brand.index')
        ]
    ],
    'tablesaw' => true
])
@section('content')
<div class="row">
    <div class="col-lg-12">
        <a href="{{route('admin.brand.create')}}" class="btn btn-primary has-icon">
            <i class="mdi mdi-plus">
            </i>
            Add new
        </a>
    </div>
    <div class="col-lg-12 mt-3">
        <div class="grid">
            <p class="grid-header">
                List Brand
            </p>
            <div class="grid-body">
                <div class="item-wrapper">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover" data-tablesaw-sortable>
                            <thead>
                                <tr>
                                    <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="1">#</th>
                                    <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="2">Name</th>
                                    <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="3">Category</th>
                                    <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4">Url</th>
                                    <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="6">Update at</th>
                                    <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="7">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($brands as $key => $value)
                                <tr>
                                    <td>
                                        {{$key+1}}
                                    </td>
                                    <td>
                                        {{$value->name}}
                                    </td>
                                    <td>
                                        {{$value->category->name ?? "Error Category"}}
                                    </td>
                                    <td>
                                        {{$value->url}}
                                    </td>
                                    <td>
                                        {{$value->updated_at->diffForHumans()}}
                                    </td>
                                    <td>
                                        @if($value->trashed())
                                            <a href="javascript:restoreByIdModal('{{$value->id}}','{{route('admin.brand.restore',['id'=>$value->id])}}')">
                                                <i class="mdi mdi-backup-restore">Restore</i> (Deleted at : {{HelperAdmin::showTime($value->deleted_at,'d/m/Y H:i:s')}})
                                            </a>
                                        @else
                                        <a href="{{route('admin.brand.edit',['id'=>$value->id])}}"><i class="mdi mdi-eye">Edit</i></a> |
                                        <a href="javascript:deleteByIdModal('{{$value->id}}','{{route('admin.brand.destroy',['id'=>$value->id])}}')">Delete</a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        {{$brands->links()}}
    </div>
</div>
@stop
@section('modal')
@include('admin.layout.include.restoreModal')
@include('admin.layout.include.deleteModal')
@stop
