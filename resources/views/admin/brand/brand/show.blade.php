@extends('admin.layout.master',['title'=>$blog->title.' | Blog'])
@section('content')
<div class="col-lg-12">
    <div class="grid">
        <p class="grid-header"> Information Blog </p>
        <div class="row">
            <div class="col-lg-9 equel-grid">
                <div class="grid">
                    <div class="grid-body">
                        <div class="d-flex justify-content-between align-items-center">
                            <h4 class="card-title">
                                Title : {{$blog->title}}
                            </h4>
                            <div class="fc-right">
                                @can('blog-edit')
                                    <a href="{{route('admin.blog.edit',['id'=>$blog->id])}}" class="btn btn-info btn-sm">
                                        Edit
                                    </a>
                                @endcan
                                @can('blog-remove')
                                    <button class="btn btn-danger btn-sm" id="destroyItem" onclick="deleteByIdModal('{{$blog->id}}','{{route('admin.blog.destroy',['id'=>$blog->id])}}')">
                                        Delete
                                    </button>
                                @endcan
                            </div>
                        </div>
                        <div class="event-list-wrapper">
                            <div class="event-list">
                                <div class="item-wrapper demo-wrapper">{!!HelperAdmin::showTags($blog->tags)!!}</div>
                            </div>
                            <div class="event-list">
                                Public at : {{HelperAdmin::showTime($blog->public_at)}}
                            </div>
                            <div class="event-list">
                                <p class="event-text">
                                    Blog Category : {{$blog->category->name ?? 'Error Data'}}
                                </p>
                            </div>
                            <div class="event-list">
                                <p class="event-text">
                                    Popular : {{$blog->popular_text}}
                                </p>
                            </div>
                            <div class="event-list">
                                <p class="event-text">
                                    Status : {{$blog->status_text}}
                                </p>
                            </div>
                            <div>
                                <p>Content : </p>
                                <br>
                                <p>{!! nl2br($blog->content) !!}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 ">
                <div class="grid">
                    <div class="grid-body">
                        <section class="hk-sec-wrapper hk-gallery-wrap">
                            <div class="tab-content">
                                <div class="tab-pane fade show active" role="tabpanel">
                                    <h6 class="mt-30 mb-20 ml-20">Gallery</h6>
                                    <div class="row hk-gallery">
                                        @foreach($blog->images as $keyImage => $valueImage)
                                            <div class="{{$valueImage->status == 2 ? 'col-lg-12' : 'col-lg-6' }} col-md-4" data-src="{{url($valueImage['src'])}}">
                                                <a class="" href="#">
                                                    <div class="gallery-img" style="background-image:url('{{url($valueImage['src'])}}');">
                                                    </div>
                                                </a>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('css')
<link href="/adstyle/vendors/lightgallery/css/lightgallery.min.css" rel="stylesheet" type="text/css">
@stop
@section('script')
<script src="/adstyle/vendors/lightgallery/js/lightgallery-all.min.js"></script>
<script>
    $('.hk-gallery').lightGallery({  showThumbByDefault: false,hash: false});
</script>
@stop

@section('modal')
    @can('blog-remove')
        @include('admin.layout.include.deleteModal')
    @endcan
@stop

