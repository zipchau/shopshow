@extends('admin.layout.master',['title'=>$category->name.' | Blog Category'])
@section('content')
<div class="col-lg-12">
    <div class="grid">
        <p class="grid-header"> Information Blog Category </p>
        <div class="row">
            <div class="col-lg-6 equel-grid">
                <div class="grid">
                    <div class="grid-body">
                        <div class="d-flex justify-content-between align-items-center">
                            <h4 class="card-title">
                                    Name : {{$category->name}}
                            </h4>
                            <div class="fc-right">
                                <a href="{{route('admin.blogcategory.edit',['id'=>$category->id])}}" class="btn btn-info btn-sm">
                                    Edit
                                </a>
                                @if($category->checkDelete())
                                <button class="btn btn-danger btn-sm" onclick="deleteByIdModal('{{$category->id}}','{{route('admin.blogcategory.destroy',['id'=>$category->id])}}')">
                                    Delete
                                </button>
                                @endif
                            </div>
                        </div>
                        <div class="event-list-wrapper">
                            <div class="event-list">
                                <p class="event-text">
                                    Parent : {{$category->parent->name ?? 'No Parent'}}
                                </p>
                            </div>
                            <div class="event-list">
                                <p class="event-text">
                                    Status : {{$category->status == 2 ? 'Hidden' : 'Show'}}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 equel-grid">
                <div class="grid">
                    <div class="grid-body">
                        <div class="d-flex justify-content-between align-items-center">
                            <h4 class="card-title">
                                Blog
                            </h4>
                        </div>
                        <div class="event-list-wrapper">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($category->blogs->take(5) as $keyBlog => $blog)
                                        <tr>
                                           <td>{{$keyBlog + 1}}</td>
                                           <td>{{$blog->title}}</td>
                                           <td>{{$blog->status_text}}</td>
                                           <td>Show</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('modal')
@include('admin.layout.include.deleteModal')
@stop

