@extends('utop.layouts.master',[
    'title'=>'Doanh Nghiệp',
])
@section('content')
<!-- Banner Section Three -->
<section class="banner-section-three style-two">
	<div class="auto-container">
		<div class="row clearfix">
        	
            <!--Content Column-->
            <div class="content-column col-lg-6 col-md-12 col-sm-12">
            	<div class="inner-column wow slideInLeft">
                	<h2>Utop <br> dành cho doanh nghiệp</h2>
                    <div class="text">Utop là giải pháp hiệu quả cho chương trình Loyalty, mang lại cho khách hàng trải nghiệm tiện lợi trong việc tích điểm và đổi quà.</div>
                    <br>
                    <a href="#choose-business" class="theme-btn btn-style-two">Xem thêm</a>
                </div>
            </div>
            <!--Image Column-->
            <div class="image-column col-lg-6 col-md-12 col-sm-12">
            	<div class="image paroller" data-paroller-factor="0.15" data-paroller-factor-lg="0.15" data-paroller-factor-md="0.10" data-paroller-factor-sm="0.10" data-paroller-type="foreground" data-paroller-direction="horizontal">
                	<img src="/utop/images/resource/mokeup-4.png" alt="" />
                </div>
            </div>
            
        </div>
    </div>
</section>
<!--End Banner Section Three -->

<section class="business-page-section" id="choose-business">
	<div class="auto-container">
		<div class="sec-title text-center">
			<h2>GÓI DOANH NGHIỆP</h2>
		</div>
		
		<div class="row clearfix">
					
			<!-- Price Block -->
			<div class="price-block col-lg-6 col-md-6 col-sm-12">
				<div class="inner-box wow fadeInLeft animated" data-wow-delay="0ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 0ms; animation-name: fadeInLeft;">
					<div class="title-box">
						<h2>Doanh nghiệp lớn</h2>
					</div>
					<div class="content-box">
						<div class="price"><sub>$</sub> 25<span>/month</span></div>
						<div class="title">$75 USD billed annually</div>
						<!-- Price List -->
						<ul class="price-list">
							<li class="check"><span class="check fa fa-check"></span>Push Notifications</li>
							<li class="cross"><span class="remove fa fa-remove"></span>Offline Synchronization</li>
							<li class="check"><span class="check fa fa-check"></span>Data Transfer</li>
							<li class="cross"><span class="remove fa fa-remove"></span>Speech &amp; Text Analytics</li>
							<li class="check"><span class="check fa fa-check"></span>24/7 Support</li>
						</ul>
						<!-- Button Box -->
						<div class="btn-box">
							<a href="large-business.html" class="theme-btn btn-style-four">Xem chi tiết</a>
						</div>
					</div>
				</div>
			</div>
			
			<!-- Price Block -->
			<div class="price-block col-lg-6 col-md-6 col-sm-12">
				<div class="inner-box wow fadeInLeft animated" data-wow-delay="300ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 300ms; animation-name: fadeInLeft;">
					<div class="title-box">
						<h2>Doanh nghiệp nhỏ</h2>
					</div>
					<div class="content-box">
						<div class="price"><sub>$</sub> 50<span>/month</span></div>
						<div class="title">$102 USD billed annually</div>
						<!-- Price List -->
						<ul class="price-list">
							<li class="check"><span class="check fa fa-check"></span>Push Notifications</li>
							<li class="cross"><span class="remove fa fa-remove"></span>Offline Synchronization</li>
							<li class="check"><span class="check fa fa-check"></span>Data Transfer</li>
							<li class="cross"><span class="remove fa fa-remove"></span>Speech &amp; Text Analytics</li>
							<li class="check"><span class="check fa fa-check"></span>24/7 Support</li>
						</ul>
						<!-- Button Box -->
						<div class="btn-box">
							<a href="small-business.html" class="theme-btn btn-style-four">Xem chi tiết</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@stop