@extends('utop.layouts.master',[
    'title'=>'Liên hệ',
])
@section('content')
<!-- Banner Section Three -->
<section class="banner-section-three">
	<div class="auto-container">
		<div class="row clearfix">
        	
            <!--Content Column-->
            <div class="content-column col-lg-6 col-md-12 col-sm-12">
            	<div class="inner-column wow fadeInLeft">
                	<h2>Liên hệ chúng tôi</h2>
                    <div class="text">Hãy liên hệ hay nếu bạn có thắc mắc hoặc cần hỗ trợ về bất cứ vấn đề nào liên quan tới Utop.</div>
                </div>
            </div>
            <!--Image Column-->
            <div class="image-column col-lg-6 col-md-12 col-sm-12">
            	<div class="image  wow slideInRight" data-wow-delay="0ms" data-wow-duration="2500ms">
                	<img src="/utop/images/resource/mokeup-10.png" alt="" />
                </div>
            </div>
            
        </div>
    </div>
</section>
<!--End Banner Section Three -->

<!-- Contact Page Section -->
<section class="contact-page-section">
	<div class="auto-container">
		<div class="row clearfix">
			
			<!-- Map Column -->
			<div class="map-column col-lg-6 col-md-12 col-sm-12">
				<div class="inner-column">
					<!--Map Outer-->
					<div class="map-outer">
						<!--Map Canvas-->
						<div class="map-canvas"
							data-zoom="12"
							data-lat="-37.817085"
							data-lng="144.955631"
							data-type="roadmap"
							data-hue="#ffc400"
							data-title="Envato"
							data-icon-path="/utop/images/icons/map-marker.png"
							data-content="Melbourne VIC 3000, Australia<br><a href='mailto:info@youremail.com'>info@youremail.com</a>">
						</div>
					</div>
				</div>
			</div>
			
			<!-- Form Column -->
			<div class="form-column col-lg-6 col-md-12 col-sm-12">
				<div class="inner-column">
					
					<!-- Contact Form -->
                    <div class="contact-form">
                            
						<!-- Title Box -->
                        <div class="title-box">
							<h2>Gửi tin nhắn cho chúng tôi</h2>
							<div class="title">Vui lòng điền vào phần bên dưới. Chúng tôi sẽ phản hồi bạn sớm.</div>
						</div>
                        
                        <!--Contact Form-->
                        <form method="post" action="sendemail.php" id="contact-form">
                            <div class="row clearfix">
								<div class="form-group col-lg-6 col-md-6 col-sm-12">
									<input type="text" name="username" placeholder="Họ và tên*">
								</div>
								
								<div class="form-group col-lg-6 col-md-6 col-sm-12">
									<input type="email" name="email" placeholder="Email*">
								</div>
								
								<div class="form-group col-lg-12 col-md-12 col-sm-12">
									<input type="text" name="subject" placeholder="Chủ đề*">
								</div>
								
								<div class="form-group col-lg-12 col-md-12 col-sm-12">
									<textarea class="darma" name="message" placeholder="Nội dung*"></textarea>
								</div>
								
								<div class="form-group col-lg-6 col-md-6 col-sm-12">
									<button class="theme-btn btn-style-two" type="submit" name="submit-form">Gửi ngay <span class="icon flaticon-next-3"></span></button>
								</div>
                            </div>
                        </form>
                            
                    </div>
                    <!--End Contact Form -->
					
				</div>
			</div>
			
		</div>
	</div>
</section>
<!-- End Contact Page Section -->
@stop

@section('script')
<script src="http://maps.google.com/maps/api/js?key=AIzaSyBKS14AnP3HCIVlUpPKtGp7CbYuMtcXE2o"></script>
<script src="/utop/js/map-script.js"></script>
@stop