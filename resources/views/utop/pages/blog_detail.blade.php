@extends('utop.layouts.master',[
    'title'=>$blog->title.' | Blog',
])
@section('content')
<!--Sidebar Page Container-->
<div class="sidebar-page-container">
	<div class="auto-container">
    	<div class="row clearfix">
			
			<!--Content Side-->
            <div class="content-side col-lg-8 col-md-12 col-sm-12">
				<div class="blog-single">
					<div class="inner-box">
						<div class="image">
							<div class="post-time">{{HelperAdmin::showTime($blog->public_at)}}</div>
							<img src="{{HelperAdmin::showImage($blog->image_first)}}" alt="" />
						</div>
						<div class="lower-content">
							<h2>{{$blog->title}}</h2>
							<div class="text">
								{!! nl2br($blog->content) !!}
							</div>
							
							<!--post-share-options-->
							<div class="post-share-options">
								<div class="post-share-inner clearfix">
									<div class="pull-left">
										<!-- Social Nav -->
										<div class="social-icons">
											<a href="https://www.facebook.com/sharer/sharer.php?u={{url()->current()}}" target="_blank"><span class="fa fa-facebook"></span></a>
										</div>
									</div>
								</div>
							</div>
							
						</div>
					</div>	
					
					<!--Comments Area-->
                    <!-- <div class="comments-area">
                        <div class="group-title"><h2>04 comments</h2></div>
                        
                        <div class="comment-box">
                            <div class="comment">
                                <div class="author-thumb"><img src="/utop/images/resource/author-5.jpg" alt=""></div>
                                <div class="comment-info clearfix"><strong>John Doue - <span>Admin</span></strong><div class="comment-time">September 05 - 2018  AT 8.00 PM</div></div>
                                <div class="text">Credibly innovate granular internal Interactively procrastinate high-payoff content without backward-compatible data. Quickly cultivate mal processes and tactical architectures. Completely iterate covalent strategic theme areas via accurate e-markets.</div>
                                <a class="theme-btn reply-btn" href="#">Reply</a>
                            </div>
                        </div>
                        
                        <div class="comment-box reply-comment">
                            <div class="comment">
                                <div class="author-thumb"><img src="/utop/images/resource/author-6.jpg" alt=""></div>
                                <div class="comment-info clearfix"><strong>Miller- <span>Designer</span></strong><div class="comment-time">September 06 - 2018  AT 10.00 AM</div></div>
                                <div class="text">Credibly innovate granular internal Interactively procrastinate high-payoff content without backward-compatible data. Quickly cultivate mal cesses and tactical architectures.</div>
                                <a class="theme-btn reply-btn" href="#">Reply</a>
                            </div>
                        </div>
                        
                        <div class="comment-box reply-comment">
                            <div class="comment">
                                <div class="author-thumb"><img src="/utop/images/resource/author-7.jpg" alt=""></div>
                                <div class="comment-info clearfix"><strong>Nattasha - <span>Model</span></strong><div class="comment-time">September 06 - 2018  AT 12.00 PM</div></div>
                                <div class="text">Credibly innovate granular internal Interactively procrastinate high-payoff content without backward-compatible data. Quickly cultivate mal cesses Completely iterate theme areas via accurate e-markets.</div>
                                <a class="theme-btn reply-btn" href="#">Reply</a>
                            </div>
                        </div>
                        
                        <div class="comment-box">
                            <div class="comment">
                                <div class="author-thumb"><img src="/utop/images/resource/author-5.jpg" alt=""></div>
                                <div class="comment-info clearfix"><strong>John Doue - <span>Admin</span></strong><div class="comment-time">September 11 - 2018  AT 11.00 PM</div></div>
                                <div class="text">Credibly innovate granular internal Interactively procrastinate high-payoff content without backward-compatible data. </div>
                                <a class="theme-btn reply-btn" href="#">Reply</a>
                            </div>
                        </div>
					
                    </div> -->
					
					<!-- Comment Form -->
                    <!-- <div class="comment-form">
                            
                        <div class="group-title">
							<h2>Leave a Comment</h2>
							<div class="title">Your email address will not be published. Required fields are marked *</div>
						</div>
                        
                        <form method="post" action="contact.html">
                            <div class="row clearfix">
								<div class="form-group col-lg-6 col-md-6 col-sm-12">
									<input type="text" name="username" placeholder="Name*" required>
								</div>
								
								<div class="form-group col-lg-6 col-md-6 col-sm-12">
									<input type="email" name="email" placeholder="Email*" required>
								</div>
								
								<div class="form-group col-lg-12 col-md-12 col-sm-12">
									<textarea class="darma" name="message" placeholder="Enter Your Comment*"></textarea>
								</div>
								
								<div class="form-group col-lg-6 col-md-6 col-sm-12">
									<button class="theme-btn btn-style-two" type="submit" name="submit-form">Send Now <span class="icon flaticon-next-3"></span></button>
								</div>
                            </div>
                        </form>
                            
                    </div> -->
                    <!--End Comment Form -->
					
				</div>
			</div>
			
			<!--Sidebar Side-->
			@include('utop.layouts.accessories.right_menu_blog')
		</div>
	</div>
</div>
@stop