@extends('utop.layouts.master',[
    'title'=>'Blog',
])
@section('content')
<!-- Banner Blog -->
@include('utop.layouts.accessories.banner_blog')
<!--End Banner Blog -->

<!--Sidebar Page Container-->
<div class="sidebar-page-container" style="padding-top: 10px">
	<div class="auto-container">
    	<div class="row clearfix">
			
			<!--Content Side-->
            <div class="content-side col-lg-8 col-md-12 col-sm-12">
				<div class="blog-masonry">
					
					<div class="masonry-items-container row clearfix">
						
						@foreach($blogs as $blog)
						<!-- News Block -->
						<div class="news-block masonry-item col-lg-6 col-md-6 col-sm-12">
							<div class="inner-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
								<div class="image">
									<a href="{{route('user.blog.detail',['slug'=>$blog->slug,'id'=>$blog->id])}}"><img src="{{HelperAdmin::showImage($blog->image_first)}}" alt="" /></a>
								</div>
								<div class="lower-content">
									<div class="time">{{HelperAdmin::showTime($blog->public_at)}}</div>
									<h3><a href="{{route('user.blog.detail',['slug'=>$blog->slug,'id'=>$blog->id])}}">{{$blog->title}}</a></h3>
									<!-- <div class="text">Efficiently unleash cross-media tion without cross-media value. Quickly maximize timely deliverables schemas.</div> -->
								</div>
							</div>
						</div>
						@endforeach
						
					</div>
					
					<div class="pagination-outer text-center">
                        <!--Styled Pagination-->
                		{{$blogs->links('utop.layouts.accessories.paginate')}}
                        <!--End Styled Pagination-->
                    </div>
					
				</div>
			</div>
			
			<!--Sidebar Side-->
			@include('utop.layouts.accessories.right_menu_blog')
			
		</div>
	</div>
</div>
@stop

@section('script')
<script src="/utop/js/isotope.js"></script>
<script src="/utop/js/jssor.slider.min.js"></script>
<script src="/utop/js/banner.js"></script>
@stop