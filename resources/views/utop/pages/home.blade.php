@extends('utop.layouts.master',[
    'title'=>'Trang Chủ',
])
@section('content')
<!--Main Banner-->
<section class="main-banner main-banner-color-utop">
	<div class="bg-round-layer"></div>
    
	<div class="auto-container">
		<div class="row clearfix">
        	
            <!--Content Column-->
            <div class="content-column col-lg-6 col-md-12 col-sm-12">
            	<div class="inner-column wow slideInLeft">
                	<h2>Chào mừng FPT Shop <br> trở thành đối tác cùng Utop</h2>
                    <div class="text">Đổi ngay voucher FPT Shop để sở hữu bất kỳ sản phẩm điện tử bao gồm điện thoại di động, máy tính bảng, laptop, phụ kiện và dịch vụ công nghệ tại đây.
                    	
                 </div>
                    <a href="#" class="theme-btn btn-style-two">Khám phá ngay</a>
                </div>
            </div>
            <!--Image Column-->
            <div class="image-column col-lg-6 col-md-12 col-sm-12">
            	<div class="inner-column">
					<div class="image paroller" data-paroller-factor="-0.15" data-paroller-factor-lg="-0.15" data-paroller-factor-md="-0.15" data-paroller-factor-sm="-0.15" data-paroller-type="foreground" data-paroller-direction="horizontal">
						<img src="/utop/images/resource/mokeup-1.png" alt="" />
					</div>
                </div>
            </div>
            
        </div>
    </div>
</section>
<!--End Main Banner-->

<!-- Partner Section -->
<section class="paddingtop-100 sponsors-section style-two ">
	<div class="auto-container">
		<!-- Title Box -->
		<div class="title-box">
			<h2>Đối tác</h2>
			<!-- <div class="title">Đối tác Utop </div> -->
		</div>
		
		<div class="carousel-outer">
            <!--Partner Slider-->
            <ul class="sponsors-carousel owl-carousel owl-theme">
                <li><div class="image-box"><a href="#"><img src="/utop/images/clients/1.png" alt=""></a></div></li>
                <li><div class="image-box"><a href="#"><img src="/utop/images/clients/2.png" alt=""></a></div></li>
                <li><div class="image-box"><a href="#"><img src="/utop/images/clients/3.png" alt=""></a></div></li>
                <li><div class="image-box"><a href="#"><img src="/utop/images/clients/4.png" alt=""></a></div></li>
                <li><div class="image-box"><a href="#"><img src="/utop/images/clients/5.png" alt=""></a></div></li>
                <li><div class="image-box"><a href="#"><img src="/utop/images/clients/1.png" alt=""></a></div></li>
                <li><div class="image-box"><a href="#"><img src="/utop/images/clients/2.png" alt=""></a></div></li>
                <li><div class="image-box"><a href="#"><img src="/utop/images/clients/3.png" alt=""></a></div></li>
            </ul>
            <ul class="sponsors-carousel owl-carousel owl-theme sponsors-carousel-100">
                <li><div class="image-box"><a href="#"><img src="/utop/images/clients/1.png" alt=""></a></div></li>
                <li><div class="image-box"><a href="#"><img src="/utop/images/clients/2.png" alt=""></a></div></li>
                <li><div class="image-box"><a href="#"><img src="/utop/images/clients/3.png" alt=""></a></div></li>
                <li><div class="image-box"><a href="#"><img src="/utop/images/clients/4.png" alt=""></a></div></li>
                <li><div class="image-box"><a href="#"><img src="/utop/images/clients/5.png" alt=""></a></div></li>
                <li><div class="image-box"><a href="#"><img src="/utop/images/clients/1.png" alt=""></a></div></li>
                <li><div class="image-box"><a href="#"><img src="/utop/images/clients/2.png" alt=""></a></div></li>
                <li><div class="image-box"><a href="#"><img src="/utop/images/clients/3.png" alt=""></a></div></li>
            </ul>
            <ul class="sponsors-carousel owl-carousel owl-theme sponsors-carousel-100">
                <li><div class="image-box"><a href="#"><img src="/utop/images/clients/1.png" alt=""></a></div></li>
                <li><div class="image-box"><a href="#"><img src="/utop/images/clients/2.png" alt=""></a></div></li>
                <li><div class="image-box"><a href="#"><img src="/utop/images/clients/3.png" alt=""></a></div></li>
                <li><div class="image-box"><a href="#"><img src="/utop/images/clients/4.png" alt=""></a></div></li>
                <li><div class="image-box"><a href="#"><img src="/utop/images/clients/5.png" alt=""></a></div></li>
                <li><div class="image-box"><a href="#"><img src="/utop/images/clients/1.png" alt=""></a></div></li>
                <li><div class="image-box"><a href="#"><img src="/utop/images/clients/2.png" alt=""></a></div></li>
                <li><div class="image-box"><a href="#"><img src="/utop/images/clients/3.png" alt=""></a></div></li>
            </ul>
        </div>
		
	</div>
</section>
<!-- End Partner Section -->


<!-- App Section -->
<section class="app-section paddingtop-200">
	<div class="auto-container">
		<div class="row clearfix">
			
			<!-- Image Column -->
			<div class="image-column col-lg-6 col-md-6 col-md-12">
				<div class="inner-column">
					<!-- Message Box -->
					<div class="message-box  paroller" data-paroller-factor="0.15" data-paroller-factor-lg="0.15" data-paroller-factor-md="0.15" data-paroller-factor-sm="0.15" data-paroller-type="foreground" data-paroller-direction="horizontal">
						<div class="box-inner">
							<!-- <div class="icon-one flaticon-paper-plane"></div> -->
							<div class="icon-two flaticon-message"></div>
							<h3> Thay thế thẻ thành viên</h3>
							<div class="text">Chỉ 1 lần duy nhất đăng ký tài khoản với Utop, bạn đã sở hữu được 1 ứng dụng cho phép bạn tích điểm mọi lúc, mọi nơi.</div>
						</div>
					</div>
					
					<!-- Chat Icon -->
					<div class="chat-icon  paroller" data-paroller-factor="-0.15" data-paroller-factor-lg="-0.15" data-paroller-factor-md="-0.15" data-paroller-factor-sm="-0.15" data-paroller-type="foreground" data-paroller-direction="vertical"><img src="/utop/images/icons/chat-icon.png" alt="" /></div>
					
					<div class="image">
						<img src="/utop/images/resource/app-utop1.png" alt="" />
					</div>
				</div>
			</div>
			
			<!-- Content Column -->
			<div class="content-column col-lg-6 col-md-6 col-md-12">
				<div class="inner-column">
					<h2>Tích điểm - Đổi quà <br>cùng Utop</h2>
					<div class="text">
						<p>
							Utop là ứng dụng tích và đổi quà điện tử, cho phép bạn tích lũy điểm hoặc trực tiếp nạp điểm để đổi quà và tiêu điểm tại hàng trăm cửa hàng khác nhau trong hệ thống đối tác Utop tại Việt Nam.
						</p>
						<p>
					 		Đây là mạng lưới được xây dựng dựa trên nền tảng Blockchain – Akachain, đảm bảo đem tới sự bảo mật tuyệt đối cũng như tối ưu hóa trong tốc độ giao dịch của đối tác và khách hàng.
						</p>
					</div>
					<a href="#tai-app" class="theme-btn btn-style-utop">TẢI NGAY<span class="icon flaticon-next-3"></span></a>
				</div>
			</div>
			
		</div>
	</div>
</section>
<!-- End App Section -->

<!-- Services Section -->
<section class="services-section">
	<div class="auto-container">
		<!-- Sec Title -->
		<!-- <div class="sec-title centered">
			<h2>The only app you will need</h2>
		</div> -->
		
		<div class="row clearfix">
			
			<!-- Services Block -->
			<div class="services-block col-lg-3 col-md-6 col-sm-12">
				<div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
					<div class="icon-box">
						<span class="icon flaticon-like-1"></span>
					</div>
					<h3><a href="feature.html">TÍCH ĐIỂM MỌI NƠI</a></h3>
					<div class="text">Cho phép bạn tích điểm với tất cả các đối tác trong hệ thống chỉ với app Utop.</div>
				</div>
			</div>
			
			<!-- Services Block -->
			<div class="services-block col-lg-3 col-md-6 col-sm-12">
				<div class="inner-box wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1500ms">
					<div class="icon-box">
						<span class="icon flaticon-heart"></span>
					</div>
					<h3><a href="feature.html">QUÀ TẶNG THIẾT THỰC</a></h3>
					<div class="text">Quà tặng bao gồm từ thẻ nạp điện thoại đến voucher từ nhiều thương hiệu: CGV, Phúc Long,…</div>
				</div>
			</div>
			
			<!-- Services Block -->
			<div class="services-block col-lg-3 col-md-6 col-sm-12">
				<div class="inner-box wow fadeInLeft" data-wow-delay="600ms" data-wow-duration="1500ms">
					<div class="icon-box">
						<span class="icon flaticon-speed-meter"></span>
					</div>
					<h3><a href="feature.html">TRẢI NGHIỆM 3 NHANH</a></h3>
					<div class="text">Tính năng thỏa mãn 3 NHANH: Tích điểm nhanh, nhận ưu đãi nhanh và đổi quà nhanh.</div>
				</div>
			</div>
			
			<!-- Services Block -->
			<div class="services-block col-lg-3 col-md-6 col-sm-12">
				<div class="inner-box wow fadeInLeft" data-wow-delay="900ms" data-wow-duration="1500ms">
					<div class="icon-box">
						<span class="icon flaticon-locked"></span>
					</div>
					<h3><a href="feature.html">GIAO DỊCH AN TOÀN</a></h3>
					<div class="text">Đảm bảo sự bảo mật tuyệt đối trong giao dịch nhờ vào công nghệ Blockchain – Akachain.</div>
				</div>
			</div>
			
		</div>
		
	</div>
</section>
<!-- End Services Section -->

<!-- New Section -->
<section class="news-section" style="padding:0px">
	<div class="auto-container">
		<!-- Sec Title -->
		<div class="sec-title centered">
			<h2>Tin tức - Quà tặng</h2>
		</div>
		<div class="row clearfix">
			@foreach($blogsPopular as $blog)
			<!-- News Block -->
			<div class="news-block col-lg-4 col-md-6 col-sm-12">
				<div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
					<div class="image">
						<a href="{{ route('user.blog.detail',['slug'=>$blog->slug,'id'=>$blog->id])}}"><img src="{{HelperAdmin::showImage($blog->image_first)}}" alt="{{$blog->slug}}" /></a>
					</div>
					<div class="lower-content">
						<div class="time">{{HelperAdmin::showTime($blogsPopular->public_at)}}</div>
						<h3><a href="{{route('user.blog.detail',['slug'=>$blog->slug,'id'=>$blog->id])}}">{{$blog->title}}</a></h3>
					</div>
				</div>
			</div>
			@endforeach
		</div>
		<div class="sec-title centered">
            <a href="#" class="theme-btn btn-style-two">Xem thêm <span class="icon flaticon-next-3"></span></a>
		</div>
	</div>
</section>
<!-- End New Section -->

<!-- Download Section -->
<section class="download-section" style="background-image:url(/utop/images/background/1.jpg)" id="tai-app">
	<div class="circle-icons paroller" data-paroller-factor="-0.05" data-paroller-factor-lg="-0.05" data-paroller-factor-md="-0.05" data-paroller-factor-sm="-0.08" data-paroller-type="foreground" data-paroller-direction="vertical">
		<span class="icon-one paroller" data-paroller-factor="0.10" data-paroller-factor-lg="0.10" data-paroller-factor-md="0.10" data-paroller-factor-sm="0.08" data-paroller-type="foreground" data-paroller-direction="vertical"></span>
		<span class="icon-two paroller" data-paroller-factor="-0.08" data-paroller-factor-lg="-0.08" data-paroller-factor-md="-0.08" data-paroller-factor-sm="-0.08" data-paroller-type="foreground" data-paroller-direction="horizontal"></span>
		<span class="icon-three paroller" data-paroller-factor="0.15" data-paroller-factor-lg="0.15" data-paroller-factor-md="0.15" data-paroller-factor-sm="0.08" data-paroller-type="foreground" data-paroller-direction="horizontal"></span>
		<span class="icon-four paroller" data-paroller-factor="-0.05" data-paroller-factor-lg="-0.05" data-paroller-factor-md="-0.05" data-paroller-factor-sm="-0.08" data-paroller-type="foreground" data-paroller-direction="horizontal"></span>
		<span class="icon-twelve paroller" data-paroller-factor="-0.10" data-paroller-factor-lg="-0.08" data-paroller-factor-md="-0.08" data-paroller-factor-sm="-0.08" data-paroller-type="foreground" data-paroller-direction="vertical"></span>
		<span class="icon-thirteen paroller" data-paroller-factor="0.05" data-paroller-factor-lg="0.08" data-paroller-factor-md="0.08" data-paroller-factor-sm="0.08" data-paroller-type="foreground" data-paroller-direction="horizontal"></span>
		<span class="icon-fourten paroller" data-paroller-factor="-0.08" data-paroller-factor-lg="-0.08" data-paroller-factor-md="-0.08" data-paroller-factor-sm="-0.08" data-paroller-type="foreground" data-paroller-direction="horizontal"></span>
		<span class="icon-fifteen paroller" data-paroller-factor="0.10" data-paroller-factor-lg="0.08" data-paroller-factor-md="0.08" data-paroller-factor-sm="0.08" data-paroller-type="foreground" data-paroller-direction="vertical"></span>
	</div>
	<div class="auto-container">
		<!-- Title Box -->
		<div class="title-box wow fadeInDown">
			<h2>TẢI ỨNG DỤNG</h2>
			<!-- <div class="text">Efficiently unleash cross-media information without cross-media value. Quickly maximizePodcasting operational change management inside of workflows to establish a framework. Taking seamless key performance indicators offline to maximise the long tail. </div> -->
		</div>
		<!-- Buttons Box -->
		<div class="buttons-box">
			<a href="https://itunes.apple.com/vn/app/utop/id1442447113?mt=8" class="theme-btn wow slideInLeft" data-wow-delay="0ms" data-wow-duration="1500ms" target=”_blank”><img src="/utop/images/icons/playstore-btn.png" alt="" /></a>
			<a href="https://play.google.com/store/apps/details?id=com.utop" class="theme-btn wow slideInRight" data-wow-delay="0ms" data-wow-duration="1500ms" target=”_blank”><img src="/utop/images/icons/google-play.png" alt="" /></a>
		</div>
	</div>
</section>
<!-- End Download Section -->
@stop