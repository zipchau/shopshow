@extends('utop.layouts.master',[
    'title'=>'Về chúng tôi',
])
@section('content')
<!-- Banner Section Two -->
<section class="banner-section-two">
	<div class="auto-container">
		
        <!-- Content Column -->
		<div class="content-column">
			<div class="inner-column wow fadeInDown">
				<h2>Giải pháp quà tặng và quản lý khách hàng <br> dành cho Doanh nghiệp</h2>
				<a href="#" class="theme-btn btn-style-two">Khám phá ngay <span class="icon flaticon-next-3"></span></a>
			</div>
		</div>
		<!-- Image Box -->
		<div class="image-box">
			<div class="image wow fadeInUp">
				<img src="/utop/images/resource/mokeup-2.png" alt="" />
			</div>
		</div>
        
    </div>
</section>
<!-- End Banner Section Two -->

<!-- Default Section -->
<section class="default-section">

	<!-- Counter Section -->
	<section class="counter-section">
		<div class="auto-container">
			<div class="inner-container">
				<div class="row clearfix">
					
					<!-- Counter Column -->
					<div class="counter-column col-lg-7 col-md-12 col-sm-12">
						<div class="inner-column">
							
							<div class="fact-counter">
								<div class="row clearfix">
								
									<!--Column-->
									<div class="column counter-column col-lg-4 col-md-4 col-sm-12">
										<div class="inner wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
											<div class="count-outer count-box clearfix">
												<span class="count-text" data-speed="0" data-stop="50">0</span>
												<h4 class="counter-title">Đối tác</h4>
											</div>
										</div>
									</div>
							
									<!--Column-->
									<div class="column counter-column col-lg-4 col-md-4 col-sm-12">
										<div class="inner wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1500ms">
											<div class="count-outer count-box clearfix">
												<span class="count-text" data-speed="5" data-stop="10">0</span>
												<h4 class="counter-title">Lĩnh vực</h4>
											</div>
										</div>
									</div>
							
									<!--Column-->
									<div class="column counter-column col-lg-4 col-md-4 col-sm-12">
										<div class="inner wow fadeInLeft" data-wow-delay="600ms" data-wow-duration="1500ms">
											<div class="count-outer count-box clearfix">
												<span class="count-text" data-speed="2" data-stop="5">0</span>
												<h4 class="counter-title">lần doanh thu</h4>
											</div>
										</div>
									</div>
									
								</div>
							</div>
							
						</div>
					</div>
					
					<!-- Counter Column -->
					<div class="content-column col-lg-5 col-md-12 col-sm-12">
						<div class="inner-column wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
							<div class="text">Giải pháp chi phí hiệu quả cho chương trình khách hàng thân thiết, mang lại doanh thu cho doanh nghiệp:</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</section>
	<!-- End Counter Section -->
	
	<!-- App Section Two -->
	<section class="app-section-two">
		<div class="auto-container">
			<h2>Utop chính là giải pháp chuyên nghiệp - đáp án cho trăn trở của Doanh nghiệp:</h2>
			<div class="row clearfix">
			
				<!-- Accordian Column -->
				<div class="accordian-column col-lg-5 col-md-12 col-sm-12">
					<div class="inner-column">
						
						<!--Accordian Box-->
						<ul class="accordion-box">
							
							<!--Block-->
							<li class="accordion block">
								<div class="acc-btn"><div class="icon-outer"><span class="icon icon-plus flaticon-heart"></span></div>Khách hàng không quay lại</div>
								<div class="acc-content">
									<div class="content">
										<div class="text">Bạn muốn biết lí do vì sao khách hàng không tiếp tục mua sản phẩm của bạn? Bạn phải làm gì?</div>
									</div>
								</div>
							</li>
	
							<!--Block-->
							<li class="accordion block active-block">
								<div class="acc-btn active"><div class="icon-outer"><span class="icon icon-plus flaticon-locked"></span></div>Cạnh tranh - Giảm giá</div>
								<div class="acc-content current">
									<div class="content">
										<div class="text">Khách hàng thường có xu hướng chọn sản phẩm rẻ. Nên bạn phải cạnh tranh với nhiều nơi khác và phải giảm giá nhiều lần để tăng khách hàng. </div>
									</div>
								</div>
							</li>
							
							<!--Block-->
							<li class="accordion block">
								<div class="acc-btn"><div class="icon-outer"><span class="icon icon-plus flaticon-speed-meter"></span></div>Bị bỏ qua tên thương hiệu</div>
								<div class="acc-content">
									<div class="content">
										<div class="text">Khách hàng thường có xu hướng xem sản phẩm, không chú ý đến tên cthương hiệu của bạn. Đặc biệt là trên các kênh thương mại điện tử (Tiki, Shopee,...)</div>
									</div>
								</div>
							</li>
							
							<!--Block-->
							<li class="accordion block">
								<div class="acc-btn"><div class="icon-outer"><span class="icon icon-plus flaticon-like-1"></span></div>Kinh doanh không hiệu quả</div>
								<div class="acc-content">
									<div class="content">
										<div class="text">Bạn đã làm rất nhiều cách kể cả giảm giá tuy nhiên vẫn không tăng được khách hàng và doanh số.</div>
									</div>
								</div>
							</li>
							
						</ul>
						
					</div>
				</div>
				
				<!-- Image Column -->
				<div class="image-column col-lg-7 col-md-12 col-sm-12">
					<div class="inner-column wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
						<span class="icon-one"></span>
						<span class="icon-two"></span>
						<span class="icon-three"></span>
						<span class="icon-four"></span>
						<span class="icon-five"></span>
						<div class="lock-icon flaticon-locked paroller" data-paroller-factor="-0.25" data-paroller-factor-lg="-0.25" data-paroller-factor-md="-0.25" data-paroller-factor-sm="-0.15" data-paroller-type="foreground" data-paroller-direction="vertical"></div>
						<div class="image paroller" data-paroller-factor="-0.15" data-paroller-factor-lg="-0.15" data-paroller-factor-md="-0.15" data-paroller-factor-sm="-0.15" data-paroller-type="foreground" data-paroller-direction="horizontal">
							<img src="/utop/images/resource/mokeup-3.png" alt="" />
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</section>
	<!-- End App Section Two -->
	
</section>
<!-- End Default Section -->

<!-- Services Section -->
<section class="services-section-two">
	<div class="auto-container">
		<!-- Sec Title -->
		<div class="sec-title centered">
			<h3>Utop đưa ra giải pháp giúp doanh nghiệp kết nối với khách hàng, mang đến dữ liệu chọn lọc và cần thiết. Từ đó, doanh nghiệp xác định được và xây dựng những​ chiến dịch ​Marketing và chương trình giảm giá đúng đối tượng, và mang lại doanh thu.
			</h3>
		</div>
		
		<!-- Services Block Two -->
		<div class="services-block-two style-two">
			<div class="inner-box">
				<div class="row clearfix">
				
					<!-- Content Column -->
					<div class="content-column col-lg-6 col-md-12 col-sm-12">
						<div class="inner-column wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
							<div class="icon-box">
								<div class="service-number">01</div>
								<span class="icon flaticon-light-bulb"></span>
							</div>
							<h3>Triển khai đơn giản, dễ dàng</h3>
							<div class="text">- Tạo mã gift card với số lượng và giá trị bạn mong muốn. 
							<br>- In sticker gift card và dán lên sản phẩm để khách hàng dễ dàng nạp.
							<br>- Khách hàng nạp Utop bằng cách quét mã trên sticker gift card. </div>
						</div>
					</div>
					
					<!-- Image Column -->
					<div class="image-column col-lg-6 col-md-12 col-sm-12">
						<div class="inner-column wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
							<div class="image">
								<img src="/utop/images/resource/services-5.png" alt="" />
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
		
		<!-- Services Block Three -->
		<div class="services-block-three style-two">
			<div class="inner-box">
				<div class="row clearfix">
				
					<!-- Image Column -->
					<div class="image-column col-lg-6 col-md-12 col-sm-12">
						<div class="inner-column wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
							<div class="image">
								<img src="/utop/images/resource/services-6.png" alt="" />
							</div>
						</div>
					</div>
					
					<!-- Content Column -->
					<div class="content-column col-lg-6 col-md-12 col-sm-12">
						<div class="inner-column wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
							<div class="icon-box">
								<div class="service-number">02</div>
								<span class="icon flaticon-big-anchor"></span>
							</div>
							<h3>Theo dõi dữ liệu -  Quản lý database hiệu quả</h3>
							<div class="text">- Theo dõi dữ liệu tự động cập nhật trên trang admin của bạn hằng ngày.
							<br>- Số liệu được hệ thống hóa để bạn dễ dàng phân loại và nhận điện khách hàng.
							<br>- Từ dữ liệu có sẵn và tài liệu chiến lược Marketing, bạn chỉ cần áp dụng và chờ đợi kết quả.</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
		
		<!-- Services Block Two -->
		<div class="services-block-two style-two">
			<div class="inner-box">
				<div class="row clearfix">
				
					<!-- Content Column -->
					<div class="content-column col-lg-6 col-md-12 col-sm-12">
						<div class="inner-column wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
							<div class="icon-box">
								<div class="service-number">03</div>
								<span class="icon flaticon-multiple-users-silhouette"></span>
							</div>
							<h3>Chi phí được sử dụng hợp lý</h3>
							<div class="text">- Phần mềm  và công cụ truyền thông chuyên nghiệp miễn phí (Pop-up, noti, email) hỗ trợ truyền thông và hỗ trợ tăng tỷ lệ quay lại của khách hàng.
							<br>- Hệ thống quà tặng dành cho khách hàng có sẵn.
							<br>- Lên chiến lược Sales và Marketing đúng đối tượng .
							<br>Ví dụ: Khách hàng mới và khách hàng thường xuyên sẽ có chương trình sales khác nhau.</div>
						</div>
					</div>
					
					<!-- Image Column -->
					<div class="image-column col-lg-6 col-md-12 col-sm-12">
						<div class="inner-column wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
							<div class="image">
								<img src="/utop/images/resource/services-7.png" alt="" />
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
		
		<!-- Services Block Three -->
		<div class="services-block-three style-two">
			<div class="inner-box">
				<div class="row clearfix">
				
					<!-- Image Column -->
					<div class="image-column col-lg-6 col-md-12 col-sm-12">
						<div class="inner-column wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
							<div class="image">
								<img src="/utop/images/resource/services-8.png" alt="" />
							</div>
						</div>
					</div>
					
					<!-- Content Column -->
					<div class="content-column col-lg-6 col-md-12 col-sm-12">
						<div class="inner-column wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
							<div class="icon-box">
								<div class="service-number">04</div>
								<span class="icon flaticon-chat-1"></span>
							</div>
							<h3>Được training và hỗ trợ thường xuyên</h3>
							<div class="text">- Utop sẵn sàng hỗ trợ trong quá trình triển khai về mặt vận hành cũng như về kiến thức liên quan.
							<br>
							- Song song với quá trình chạy chương trình "Khách hàng thân thiết", Utop cung cấp hoàn toàn miễn phí các tài liệu, khóa học,... để tối ưu hóa công cụ Utop dành cho bạn.
							</div>
							
						</div>
					</div>
					
				</div>
			</div>
		</div>
		
	</div>
</section>
<!-- End Services Section Two -->

<!-- Platform Section -->
<section class="platform-section paddingtop-50">
	<div class="auto-container">
		<!-- Title Box -->
		<div class="title-box">
			<h2>Trang quản lý hiệu quả cho bất kỳ nền tảng nào</h2>
			<div class="text">Doanh nghiệp dễ dàng quản lý dữ liệu trên bất cứ thiết bị nào (điện thoại, máy tính, ipad,...) và dễ dàng tương tác, yêu cầu hồ trợ từ Utop ngay tại group hỗ trợ doanh nghiệp trên Facebook - mạng xã hội phổ biến nhất hiện nay.</div>
		</div>
		
		<!-- Inner Container -->
		<div class="inner-container">
			<div class="row clearfix">
				
				<!-- Image Column -->
				<div class="image-column col-lg-4 col-md-4 col-sm-12">
					<div class="image paroller" data-paroller-factor="-0.15" data-paroller-factor-lg="-0.15" data-paroller-factor-md="-0.15" data-paroller-factor-sm="-0.15" data-paroller-type="foreground" data-paroller-direction="horizontal">
						<img src="/utop/images/resource/platform.png" alt="" />
					</div>
				</div>
				
				<!-- Image Column -->
				<div class="image-column col-lg-4 col-md-4 col-sm-12">
					<div class="image paroller" data-paroller-factor="0.10" data-paroller-factor-lg="0.10" data-paroller-factor-md="0.10" data-paroller-factor-sm="0.10" data-paroller-type="foreground" data-paroller-direction="vertical">
						<img src="/utop/images/resource/platform-1.png" alt="" />
					</div>
				</div>
				
				<!-- Image Column -->
				<div class="image-column col-lg-4 col-md-4 col-sm-12">
					<div class="image paroller" data-paroller-factor="0.15" data-paroller-factor-lg="0.15" data-paroller-factor-md="0.15" data-paroller-factor-sm="0.15" data-paroller-type="foreground" data-paroller-direction="horizontal">
						<img src="/utop/images/resource/platform-2.png" alt="" />
					</div>
				</div>
				
			</div>
			
			<!-- Button Box -->
			<div class="button-box text-center">
				<a href="#" class="theme-btn btn-style-two">Liên hệ tư vấn <span class="icon flaticon-next-3"></span></a>
			</div>
			
		</div>
		
	</div>
</section>
<!-- End Platform Section -->

<!-- New Section -->
<section class="news-section">
	<div class="auto-container">
		<!-- Sec Title -->
		<div class="sec-title centered">
			<h2>Tin tức nổi bật</h2>
		</div>
		<div class="row clearfix">
			
			<!-- News Block -->
			<div class="news-block col-lg-4 col-md-6 col-sm-12">
				<div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
					<div class="image">
						<a href="blog-detail.html"><img src="/utop/images/resource/news-1.jpg" alt="" /></a>
					</div>
					<div class="lower-content">
						<div class="time">On 15 August 2018 , 10 min Read</div>
						<h3><a href="blog-detail.html">How to become a best sale marketer in a year!</a></h3>
						<div class="clearfix">
							<div class="pull-left">
								<!-- Author Box -->
								<div class="author-box">
									<div class="author-inner">
										<div class="image">
											<img src="/utop/images/resource/author-1.jpg" alt="" />
										</div>
										<div class="admin">Admin ;</div>
										<div class="author-name">Helena Kamil</div>
									</div>
								</div>
							</div>
							<div class="pull-right">
								<a href="#" class="likes flaticon-heart"><span class="total">12</span></a>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<!-- News Block -->
			<div class="news-block col-lg-4 col-md-6 col-sm-12">
				<div class="inner-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
					<div class="image">
						<a href="blog-detail.html"><img src="/utop/images/resource/news-2.jpg" alt="" /></a>
					</div>
					<div class="lower-content">
						<div class="time">On 15 August 2018 , 10 min Read</div>
						<h3><a href="blog-detail.html">Best sale <br> marketer in a year!</a></h3>
						<div class="clearfix">
							<div class="pull-left">
								<!-- Author Box -->
								<div class="author-box">
									<div class="author-inner">
										<div class="image">
											<img src="/utop/images/resource/author-2.jpg" alt="" />
										</div>
										<div class="admin">Admin ;</div>
										<div class="author-name">Nancy carolin</div>
									</div>
								</div>
							</div>
							<div class="pull-right">
								<a href="#" class="likes flaticon-heart"><span class="total">12</span></a>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<!-- News Block -->
			<div class="news-block col-lg-4 col-md-6 col-sm-12">
				<div class="inner-box wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
					<div class="image">
						<a href="blog-detail.html"><img src="/utop/images/resource/news-3.jpg" alt="" /></a>
					</div>
					<div class="lower-content">
						<div class="time">On 15 August 2018 , 10 min Read</div>
						<h3><a href="blog-detail.html">Best sale become a best sale marketer in a year!</a></h3>
						<div class="clearfix">
							<div class="pull-left">
								<!-- Author Box -->
								<div class="author-box">
									<div class="author-inner">
										<div class="image">
											<img src="/utop/images/resource/author-3.jpg" alt="" />
										</div>
										<div class="admin">Admin ;</div>
										<div class="author-name">John Martin</div>
									</div>
								</div>
							</div>
							<div class="pull-right">
								<a href="#" class="likes flaticon-heart"><span class="total">12</span></a>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</div>
</section>
<!-- End New Section -->
@stop