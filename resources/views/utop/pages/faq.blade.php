@extends('utop.layouts.master',[
    'title'=>'Faq',
])
@section('content')
<!-- Banner Section Three -->
<section class="banner-section-three">
	<div class="auto-container">
		<div class="row clearfix">
        	
            <!--Content Column-->
            <div class="content-column col-lg-6 col-md-12 col-sm-12">
            	<div class="inner-column wow fadeInLeft">
                	<h2>Câu hỏi thường gặp</h2>
                    <div class="text">Thắc mắc thường gặp khi bạn dùng Utop. Nếu có câu hỏi khác hoặc thắc mắc trong khi sử dụng Utop, vui lòng liên hệ chúng tôi tại đây</div>
                </div>
            </div>
            <!--Image Column-->
            <div class="image-column col-lg-6 col-md-12 col-sm-12">
            	<div class="image  wow slideInRight" data-wow-delay="0ms" data-wow-duration="2500ms">
                	<img src="/utop/images/resource/mokeup-5.png" alt="" />
                </div>
            </div>
            
        </div>
    </div>
</section>
<!--End Banner Section Three -->

<!-- Faq Page Section -->
<section class="faq-page-section">
	<div class="outer-container">
		
		<!--Faq Info Tabs-->
		<div class="faq-info-tabs">
			<!--Faq Tabs-->
			<div class="faq-tabs tabs-box">
			
				<!--Tab Btns-->
				<ul class="tab-btns tab-buttons clearfix">
					<li data-tab="#faq-started" class="tab-btn active-btn">Tài khoản thẻ</li>
					<li data-tab="#faq-work" class="tab-btn">Tích và sử dụng điểm</li>
				
				</ul>
				
				<!--Tabs Container-->
				<div class="tabs-content">
					<!--Tab / Active Tab-->
					<div class="tab active-tab" id="faq-started">
						<div class="content">
							<div class="row faq-row clearfix">
								
								<!-- Faq Block -->
								<div class="faq-block">
									<div class="inner-box">
										<h3>Điểm Utop là gì?</h3>
										<div class="text">
											<p>Điểm Utop là điểm được xây dựng trên nền tảng Blockchain – Akachain, và được sử dụng tại tất cả các cửa hàng trong mạng lưới Utop.</p>
											
										</div>
									</div>
								</div>
								
								<!-- Faq Block -->
								<div class="faq-block">
									<div class="inner-box">
										<h3>Làm sao để tôi tích lũy điểm Utop?</h3>
										<div class="text">
											<p>Bạn có thể tích điểm Utop theo 2 cách:</p>
											<p>- Tích điểm Utop khi sử dụng dịch vụ/sản phẩm tại các “Đối tác tích điểm” của Utop.</p>
											<p>- Đổi điểm thành viên của đối tác liên kết sang điểm Utop.</p>
										</div>
									</div>
								</div>
								
								<!-- Faq Block -->
								<div class="faq-block">
									<div class="inner-box">
										<h3>Thông tin đối tác tích điểm ở đâu?</h3>
										<div class="text">
											<p>Để biết thông tin tích điểm, bạn vào app Utop và xem tại mục “Đối tác tích điểm”. Ngoài ra, bạn có thể theo dõi thường xuyên tại fanpage của Utop: https://www.facebook.com/utop.vn/
											</p>
										</div>
									</div>
								</div>
								
								<!-- Faq Block -->
								<div class="faq-block">
									<div class="inner-box">
										<h3>Ngoài tích điểm thì tôi có thể có điểm Utop bằng cách khác?</h3>
										<div class="text">
											<p>Ngoài việc tích điểm Utop tại các đối tác tích điểm của Utop, bạn có thể nạp trực tiếp tại app từ Gift Card.</p>
											
										</div>
									</div>
								</div>
								
							
								
								<!-- Faq Block -->
								<div class="faq-block">
									<div class="inner-box">
										<h3>Làm sao để tôi có Gift Card?</h3>
										<div class="text">
											<p>Hiện tại, Gift Card đã bắt đầu được sử dụng tại các doanh nghiệp. Và chắc chắn sẽ đến tay bạn sớm thôi. Để biết thêm thông tin, bạn có thể theo dõi tại app hoặc tại fanpage của Utop (https://www.facebook.com/utop.vn/).</p>
										
										</div>
									</div>
								</div>
								
							</div>
						</div>
					</div>
					
					<!--Tab-->
                    <div class="tab" id="faq-work">
						<div class="content">
						
							<div class="row faq-row clearfix">
								<!-- Faq Block -->
								<div class="faq-block">
									<div class="inner-box">
										<h3>Điểm Utop để sử dụng để làm gì?</h3>
										<div class="text">
											<p>Có 2 cách để bạn sử dụng điểm Utop:</p>
											<p>- Đổi quà: bạn có thể đổi các quà tặng của nhiều thương hiệu hiện đang có tại app Utop.</p>
											<p>- Đổi điểm trực tiếp tại quầy thanh toán: bạn có thể sử dụng điểm Utop để thanh toán 1 phần hoặc toàn bộ hóa đơn tại cửa hàng hiện đang nằm trong đối tác “thanh toán tại quầy” trong app.</p>
										</div>
									</div>
								</div>
								
								<!-- Faq Block -->
								<div class="faq-block">
									<div class="inner-box">
										<h3>Làm thế nào để đổi quà?</h3>
										<div class="text">
											<p>Để đổi quà, bạn truy cập vào app Utop. Sau đó, chọn lựa quà tặng mà bạn muốn lấy. Và bấm vào nút đổi để nhận món quà đó.</p>
										</div>
									</div>
								</div>
								
								<!-- Faq Block -->
								<div class="faq-block">
									<div class="inner-box">
										<h3>Làm thế nào để đổi điểm trực tiếp tại quầy thanh toán?</h3>
										<div class="text">
											<p>Thay vì thanh toán bằng tiền mặt, bạn có thể sử dụng điểm Utop bạn đang có để thanh toán 1 phần hoặc toàn bộ hóa đơn. Phần thiếu bạn trả bằng tiền mặt hoặc thẻ đều được.</p>
											<p>Bạn vui lòng vào app và xem phần ” Thanh toán tại quầy” để xem những đối tác nào hiện cho phép bạn đổi Utop trực tiếp nhé.</p>
										</div>
									</div>
								</div>
								
								<!-- Faq Block -->
								<div class="faq-block">
									<div class="inner-box">
										<h3>Tôi có thể liên hệ ai nếu xảy ra vấn đề?</h3>
										<div class="text">
											<p>Nếu bạn có bất cứ thắc mắc nào về ứng dụng hoặc gặp vấn đề nào liên quan khi sử dụng Utop, bạn vui lòng liên hệ qua 3 cách sau:</p>
											<p>- Hotline:+84 767 596 728</p>
											<p>- Email: support@utop.vn</p>
											<p>- Fanpage: https://www.facebook.com/utop.vn/</p>
										</div>
									</div>
								</div>
								
								<!-- Faq Block -->
								<div class="faq-block">
									<div class="inner-box">
										<h3>Làm sao để tôi liên hệ hợp tác với Utop?</h3>
										<div class="text">
											<p>Nếu bạn có bất cứ thắc mắc nào về ứng dụng hoặc gặp vấn đề nào liên quan khi sử dụng Utop, bạn vui lòng liên hệ qua 3 cách sau:</p>
											<p>- Hotline:+84 767 596 728</p>
											<p>- Email: support@utop.vn</p>
											<p>- Fanpage: https://www.facebook.com/utop.vn/</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</section>
<!-- End Faq Page Section -->
@stop