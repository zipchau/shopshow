<div class="site-mobile-menu">
  <div class="site-mobile-menu-header">
    <div class="site-mobile-menu-close mt-3">
      <span class="icon-close2 js-menu-toggle"></span>
    </div>
  </div>
  <div class="site-mobile-menu-body"></div>
</div>

<header class="main-nav site-navbar py-4 bg-white" role="banner">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-10 col-xl-2">
        <h1 class="mb-0 site-logo"><a href="{{route('user.home')}}" class="text-black h2 mb-0"><img src="/utop/images/logo-utop2.png" alt="" title=""></a></h1>
      </div>
      <div class="col-12 col-md-10 d-none d-xl-block">
        <nav class="site-navigation position-relative text-right" role="navigation">
          <ul class="site-menu js-clone-nav mr-auto d-none d-lg-block">
            <!-- class="active" -->
            <li><a href="{{route('user.home')}}">Trang chủ</a></li>
            <li><a href="{{route('user.aboutus')}}">Về chúng tôi</a></li>
            <li><a href="{{route('user.business')}}">Doanh nghiệp</a></li>
            <li><a href="{{route('user.blog')}}">Blog</a></li>
            <li class="has-children"><a href="#">Hỗ trợ - liên hệ</a>
                <ul class="dropdown">
                    <li><a href="{{route('user.faq')}}">FAQ</a></li>
                    <li><a href="{{route('user.contact')}}">Liên hệ</a></li>
                </ul>
            </li>
          </ul>
        </nav>
      </div>
      <div class="d-inline-block d-xl-none ml-md-0 mr-auto py-3" style="position: relative; top: 3px;">
        <a href="#" class="site-menu-toggle js-menu-toggle text-black active">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </a>
      </div>
    </div>
  </div>
</header>