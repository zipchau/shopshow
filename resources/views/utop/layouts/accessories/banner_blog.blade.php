<section class="banner-section-blog">
	<div class="auto-container">
		<!-- Jssor Slider Begin -->
        <div id="slider1_container" class="slider1_container">
            <!-- Loading Screen -->
            <div data-u="loading" class="jssorl-009-spin loadingJssorl">
                <img class="imgLoadignJssorl" src="/utop/svg/loading/static-svg/spin.svg" />
            </div>

            <!-- Slides Container -->
            <div data-u="slides" class="slidesJssorl">
                @foreach($blogBanner as $banner)
                <div>
                    <img data-u="image" src="{{url($banner->image)}}" />
                </div>
                @endforeach
            </div>
            
            <!--#region Bullet Navigator Skin Begin -->
            <div data-u="navigator" class="jssorb031 navigatorJssorb" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
                <div data-u="prototype" class="i prototype">
                    <svg viewBox="0 0 16000 16000">
                        <circle class="b" cx="8000" cy="8000" r="5800"></circle>
                    </svg>
                </div>
            </div>
            <!--#endregion Bullet Navigator Skin End -->
        
            <!--#region Arrow Navigator Skin Begin -->
            <div data-u="arrowleft" class="jssora051 arrowleft" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
                <svg viewBox="0 0 16000 16000" style="">
                    <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
                </svg>
            </div>
            <div data-u="arrowright" class="jssora051 arrowright" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
                <svg viewBox="0 0 16000 16000">
                    <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
                </svg>
            </div>
            <!--#endregion Arrow Navigator Skin End -->
        </div>
        <!-- Jssor Slider End -->
	</div>
</section>