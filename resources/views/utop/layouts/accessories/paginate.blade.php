@if ($paginator->hasPages())
    <ul class="styled-pagination">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li aria-disabled="true" aria-label="@lang('pagination.previous')">
                <span aria-hidden="true">&lsaquo;</span>
            </li>
        @else
            <li>
                <a href="{{ $paginator->previousPageUrl() }}" rel="prev" aria-label="@lang('pagination.previous')">&lsaquo;</a>
            </li>
        @endif
        @if($paginator->currentPage() > 3)
            <li><a href="{{ $paginator->url(1)}}">1</a></li>
        @endif
        @if($paginator->currentPage() > 4)
            <li>
                <span aria-hidden="true">...</span>
            </li>
        @endif
        {{-- Pagination Elements --}}
        @foreach(range(1, $paginator->lastPage()) as $i)
            @if($i >= $paginator->currentPage() - 2 && $i <= $paginator->currentPage() + 2)
                @if ($i == $paginator->currentPage())
                <li class="active" aria-disabled="true"><span>{{ $i }}</span></li>
                @else
                <li><a href="{{ $paginator->url($i) }}">{{ $i }}</a></li>

                @endif
            @endif
        @endforeach

        @if($paginator->currentPage() < $paginator->lastPage() - 3)
            <li>
                <span aria-hidden="true">...</span>
            </li>
        @endif
        @if($paginator->currentPage() < $paginator->lastPage() - 2)
            <li><a href="{{ $paginator->url($paginator->lastPage())}}">{{ $paginator->lastPage() }}</a></li>
        @endif

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li>
                <a href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')">&rsaquo;</a>
            </li>
        @else
            <li class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.next')">
                <span aria-hidden="true">&rsaquo;</span>
            </li>
        @endif
    </ul>
@endif