<div class="sidebar-side col-lg-4 col-md-12 col-sm-12">
	<aside class="sidebar default-sidebar">
		
		<!--search box-->
		<div class="sidebar-widget search-box">
			<form method="post" action="blog.html">
				<div class="form-group">
					<input type="search" name="search-field" value="" placeholder="Search" required="">
					<button type="submit"><span class="icon flaticon-magnifying-glass"></span></button>
				</div>
			</form>
		</div>
		
		<!-- Category Widget -->
        <div class="sidebar-widget category-widget">
        	<div class="sidebar-title">
            	<h2>Danh Mục</h2>
            </div>
            <div class="widget-content">
            	<ul>
            		@foreach($categories as $category)
                	<li><a href="{{route('user.blog.category',['slug'=>$category->slug,'id'=>$category->id])}}">{{$category->name}}</a></li>
                	@endforeach
                </ul>
            </div>
        </div>
		
		<!-- Post Widget -->
        <div class="sidebar-widget popular-posts">
        	<div class="sidebar-title">
            	<h2>Bài viết nổi bật</h2>
            </div>
            <div class="widget-content">
                @foreach($blogsPopular as $blogPopular)
            	<article class="post">
                    <div class="text"><a href="{{route('user.blog.detail',['slug'=>$blogPopular->slug,'id'=>$blogPopular->id])}}">{{$blogPopular->title}}</a></div>
                    <div class="post-info">{{HelperAdmin::showTime($blogPopular->public_at)}}</div>
                </article>
                @endforeach
            </div>
        </div>
		
		<!-- Tags Widget -->
        <!-- <div class="sidebar-widget tags">
        	<div class="sidebar-title">
            	<h2>Tags</h2>
            </div>
            <div class="widget-content clearfix">
            	<a href="#">Business</a>
                <a href="#">plan</a>
            </div>
        </div> -->
		
		<!-- Tweet Widget -->
        <div class="sidebar-widget tweet-widget">
        	<div class="sidebar-title">
            	<h2>Facebook</h2>
            </div>
			<div class="widget-content">
				<div class="fb-page" data-href="https://www.facebook.com/utop.vn/" data-tabs="timeline" data-width="" data-height="" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/utop.vn/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/utop.vn/">Utop</a></blockquote></div>
			</div>
		</div>
		
	</aside>
</div>