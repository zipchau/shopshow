<!-- <footer class="main-footer">
	<div class="footer-bottom">
		<div class="auto-container">
			<div class="logo">
				<a href="index.html"><img src="/utop/images/logoutop.png" alt="" /></a>
			</div>
			<ul class="list-inline">
			  <li class="list-inline-item">Về chúng tôi</li>
			  <li class="list-inline-item">Điều khoản sử dụng</li>
			  <li class="list-inline-item">Chính sách bảo mật</li>
			</ul>
			<hr>
			<ul class="list-inline">
			  <li class="list-inline-item">(+84)767 596 728</li>
			  <li class="list-inline-item">support@utop.vn</li>
			</ul>
			  , 
			<hr>
			<div class="copyright">Copyright &copy; 2019 Utop . All Rights Reserved.</div>
			<div class="social-nav">
				<a href="#"><span class="fa fa-facebook"></span></a>
				<a href="#"><span class="fa fa-instagram"></span></a>
			</div>
		</div>
	</div>
	
</footer> -->

<footer class="footer-distributed">

	<div class="footer-left">

		<h3>Utop<span>logo</span></h3>

		<p class="footer-links">
			<ul class="list-inline">
			  <li class="list-inline-item">Về chúng tôi</li>
			  <li class="list-inline-item">Điều khoản sử dụng</li>
			  <li class="list-inline-item">Chính sách bảo mật</li>
			</ul>
		</p>
	</div>

	<div class="footer-center">

		<div>
			<i class="fa fa-map-marker"></i>
			<p><span>Tòa nhà F-Town, Khu T2, Đường D1, Khu Công Nghệ Cao, Phường Tân Phú, Quận 9</span> Thành phố Hồ Chí Minh, Vietnam</p>
		</div>

		<div>
			<a  href="tel:+84767596728">
				<i class="fa fa-phone"></i>
				<p>(+84) 767 596 728</p>
			</a>
		</div>

		<div>
			<a href="mailto:support@utop.vn">
				<i class="fa fa-envelope"></i>
				<p>support@utop.vn</p>
			</a>
		</div>

	</div>

	<div class="footer-right">
		<p class="footer-company-about">
			<div class="fb-page" data-href="https://www.facebook.com/utop.vn/" data-tabs="" data-width="" data-height="" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/utop.vn/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/utop.vn/">Utop</a></blockquote></div>
		</p>
	</div>
</footer>