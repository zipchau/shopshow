<!DOCTYPE html>
<html lang="vn">
	<head>
		<meta charset="utf-8">
		<title>{{$title}} | Utop </title>
		<!-- Stylesheets -->
		<link href="/utop/css/bootstrap.css" rel="stylesheet">
		<link href="/utop/css/style.css" rel="stylesheet">
		<link href="/utop/css/responsive.css" rel="stylesheet"> 

		<!--Favicon-->
		<link rel="shortcut icon" href="/utop/images/favicon.png" type="image/x-icon">
		<link rel="icon" href="/utop/images/favicon.png" type="image/x-icon">
		<!-- Responsive -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
		@yield('head')
	</head>

	<body>
		<div id="fb-root"></div>
		<script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.3&appId=571626656607451&autoLogAppEvents=1"></script>
		<div class="page-wrapper">
		    <!-- Main Header-->
		    @include('utop.layouts.accessories.header')
		    <!--End Main Header -->

		    <!--Form Back Drop-->
		    <!-- <div class="form-back-drop"></div> -->
		    <!--End Consulting Form-->

		    <!--Content Page-->
		    @yield('content')
		    <!--End ContentPage-->

		    
			<!-- Main Footer -->
		    @include('utop.layouts.accessories.footer')
			<!-- End Main Footer -->
			
		</div>
		<!--End pagewrapper-->

		<!--Scroll to top-->
		<div class="scroll-to-top scroll-to-target" data-target="html">
			<span class="icon fa fa-angle-double-up"></span>
		</div>

		<script src="/utop/js/jquery.js"></script>
		<script src="/utop/js/popper.min.js"></script>
		<script src="/utop/js/bootstrap.min.js"></script>
		<script src="/utop/js/jquery.fancybox.js"></script>
		<script src="/utop/js/jquery-ui.js"></script>
		<script src="/utop/js/owl.js"></script>
		<script src="/utop/js/appear.js"></script>
		<script src="/utop/js/wow.js"></script>
		<script src="/utop/js/validate.js"></script>
		<script src="/utop/js/element-in-view.js"></script>
		<script src="/utop/js/paroller.js"></script>
		<script src="/utop/js/particles.js"></script>
		@yield('script')
		<script src="/utop/js/script.js"></script>
	</body>
</html>