<!DOCTYPE html>
<html lang="en">
  <head>
    <title>{{$title ?? "Mosaic"}}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet">

    <link rel="stylesheet" href="/mosaic/css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="/mosaic/css/animate.css">
    
    <link rel="stylesheet" href="/mosaic/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/mosaic/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="/mosaic/css/magnific-popup.css">

    <link rel="stylesheet" href="/mosaic/css/aos.css">

    <link rel="stylesheet" href="/mosaic/css/ionicons.min.css">

    <link rel="stylesheet" href="/mosaic/css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="/mosaic/css/jquery.timepicker.css">

    
    <link rel="stylesheet" href="/mosaic/css/flaticon.css">
    <link rel="stylesheet" href="/mosaic/css/icomoon.css">
    <link rel="stylesheet" href="/mosaic/css/style.css">
  </head>
  <body>
    @include('user.mosaic.layout.accessories.nav')
    <section class="hero-wrap hero-wrap-2" style="background-image: url('/mosaic/images/banner_utop.png');" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
            <h1 class="mb-2 bread">Tin Tức - Ưu Đãi</h1>
            <p class="breadcrumbs"><span class="mr-2"><a href="/">Trang Chủ <i class="ion-ios-arrow-forward"></i></a></span> <span>Tin Tức - Ưu Đãi <i class="ion-ios-arrow-forward"></i></span></p>
          </div>
        </div>
      </div>
    </section>
    
    <section class="ftco-section">
      <div class="container">
        <div class="row">
          <div class="col-lg-8">
            <div class="row">
              @foreach($blogs as $keyBlog => $valueBlog)
              <div class="col-md-6 ftco-animate">
                <div class="blog-entry">
                  <a href="blog-single.html" class="block-20" style="background-image: url('{{$valueBlog->image_first}}');">
                  </a>
                  <div class="text d-flex py-4">
                    <div class="meta mb-3">
                      <div><a href="#">{{$valueBlog->public_at_show}}</a></div>
                    </div>
                    <div class="desc pl-3">
                      <h3 class="heading"><a href="#">{{$valueBlog->title}}</a></h3>
                    </div>
                  </div>
                </div>
              </div>
              @endforeach
            </div>
            <div class="row no-gutters my-5">
              <div class="col text-center">
                <div class="block-27">
                  {{ $blogs->appends($_GET)->links("user.mosaic.layout.paginate") }}
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 sidebar ftco-animate">
            <div class="sidebar-box ftco-animate">
              <h3>Danh mục</h3>
              <ul class="categories">
                @foreach($categories as $keyCategory => $category)
                <li><a href="#">{{$category->name}}</a></li>
                @endforeach
              </ul>
            </div>

            <div class="sidebar-box ftco-animate">
              <h3>Popular Articles</h3>
              @foreach($popularBlogs as $keyPopularBlog => $popularBlog)
              <div class="block-21 mb-4 d-flex">
                <a class="blog-img mr-4" style="background-image: url({{$popularBlog->image_first}});"></a>
                <div class="text">
                  <h3 class="heading"><a href="#">{{$valueBlog->title}}</a></h3>
                  <div class="meta">
                    <div><a href="#"><span class="icon-calendar"></span> {{$popularBlog->public_at_show}}</a></div>
                  </div>
                </div>
              </div>
              @endforeach
            </div>

            {{-- <div class="sidebar-box ftco-animate">
              <h3>Tag Cloud</h3>
              <ul class="tagcloud m-0 p-0">
                <a href="#" class="tag-cloud-link">House</a>
                <a href="#" class="tag-cloud-link">Office</a>
                <a href="#" class="tag-cloud-link">Land</a>
                <a href="#" class="tag-cloud-link">Building</a>
                <a href="#" class="tag-cloud-link">Table</a>
                <a href="#" class="tag-cloud-link">Intrior</a>
                <a href="#" class="tag-cloud-link">Exterior</a>
              </ul>
            </div> --}}


            <div class="sidebar-box ftco-animate">
              <h3>Paragraph</h3>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus itaque, autem necessitatibus voluptate quod mollitia delectus aut, sunt placeat nam vero culpa sapiente consectetur similique, inventore eos fugit cupiditate numquam!</p>
            </div>
          </div><!-- END COL -->
        </div>
      </div>
    </section>
    @include('user.mosaic.layout.accessories.footer')

  <script src="/mosaic/js/jquery.min.js"></script>
  <script src="/mosaic/js/jquery-migrate-3.0.1.min.js"></script>
  <script src="/mosaic/js/popper.min.js"></script>
  <script src="/mosaic/js/bootstrap.min.js"></script>
  <script src="/mosaic/js/jquery.easing.1.3.js"></script>
  <script src="/mosaic/js/jquery.waypoints.min.js"></script>
  <script src="/mosaic/js/jquery.stellar.min.js"></script>
  <script src="/mosaic/js/owl.carousel.min.js"></script>
  <script src="/mosaic/js/jquery.magnific-popup.min.js"></script>
  <script src="/mosaic/js/aos.js"></script>
  <script src="/mosaic/js/jquery.animateNumber.min.js"></script>
  <script src="/mosaic/js/bootstrap-datepicker.js"></script>
  <script src="/mosaic/js/scrollax.min.js"></script>
  <script src="/mosaic/js/main.js"></script>
    
  </body>
</html>