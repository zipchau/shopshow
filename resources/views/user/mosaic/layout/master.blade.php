<!DOCTYPE html>
<html lang="vn">
  <head>
    <title>{{$title ?? "UTOP"}}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet">

    <link rel="stylesheet" href="/mosaic/css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="/mosaic/css/animate.css">
    
    <link rel="stylesheet" href="/mosaic/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/mosaic/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="/mosaic/css/magnific-popup.css">

    <link rel="stylesheet" href="/mosaic/css/aos.css">

    <link rel="stylesheet" href="/mosaic/css/ionicons.min.css">

    <link rel="stylesheet" href="/mosaic/css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="/mosaic/css/jquery.timepicker.css">

    
    <link rel="stylesheet" href="/mosaic/css/flaticon.css">
    <link rel="stylesheet" href="/mosaic/css/icomoon.css">
    <link rel="stylesheet" href="/mosaic/css/style.css">
  </head>
  <body>
    
    @include('user.mosaic.layout.accessories.nav')
    @yield('content')
    @include('user.mosaic.layout.accessories.footer')
    
  

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  <script src="/mosaic/js/jquery.min.js"></script>
  <script src="/mosaic/js/jquery-migrate-3.0.1.min.js"></script>
  <script src="/mosaic/js/popper.min.js"></script>
  <script src="/mosaic/js/bootstrap.min.js"></script>
  <script src="/mosaic/js/jquery.easing.1.3.js"></script>
  <script src="/mosaic/js/jquery.waypoints.min.js"></script>
  <script src="/mosaic/js/jquery.stellar.min.js"></script>
  <script src="/mosaic/js/owl.carousel.min.js"></script>
  <script src="/mosaic/js/jquery.magnific-popup.min.js"></script>
  <script src="/mosaic/js/aos.js"></script>
  <script src="/mosaic/js/jquery.animateNumber.min.js"></script>
  <script src="/mosaic/js/bootstrap-datepicker.js"></script>
  <script src="/mosaic/js/scrollax.min.js"></script>
  <script src="/mosaic/js/main.js"></script>
    
  </body>
</html>