<section class="home-slider js-fullheight owl-carousel bg-white">
  @foreach($sliderImage as $keySlider => $valueSlider)
  <div class="slider-item js-fullheight">
    <div class="overlay"></div>
    <div class="container">
      <div class="row d-md-flex no-gutters slider-text js-fullheight align-items-center justify-content-end" data-scrollax-parent="true">
        <div class="one-third order-md-last img js-fullheight" style="background-image:url({{url($valueSlider->image)}});">
        </div>
          <div class="one-forth d-flex js-fullheight align-items-center ftco-animate" data-scrollax=" properties: { translateY: '70%' }">
            <div class="text">
                <h1 class="mb-4">{{$valueSlider->title}}</h1>
                <p>{{$valueSlider->content}}</p>
                <p><a href="{{$valueSlider->btn_url}}" class="btn btn-primary px-4 py-3 mt-3">{{$valueSlider->btn_name}}</a></p>
            </div>
          </div>
        </div>
    </div>
  </div>
  @endforeach
</section>