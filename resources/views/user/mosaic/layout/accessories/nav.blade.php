<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
  <div class="container">
    <a class="navbar-brand" href="/"><img class="thememount-logo-img standardlogo" src="http://utop.esy.es/wp-content/uploads/2019/02/logoutop.png" alt="Utop" width="140" height="62"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="oi oi-menu"></span> Menu
    </button>

    <div class="collapse navbar-collapse" id="ftco-nav">
      <ul class="navbar-nav ml-auto">
          @php
            $menu = $menu ?? null;
          @endphp
          <li class="nav-item {{$menu == 'aboutus' ? 'active' : ''}}"><a href="#aboutUs" class="nav-link">GIỚI THIỆU</a></li>
          <li class="nav-item {{$menu == 'bussiness' ? 'active' : ''}}"><a href="#" class="nav-link">DOANH NGHIỆP</a></li>
          <li class="nav-item {{$menu == 'blog' ? 'active' : ''}}"><a href="{{route('blog')}}" class="nav-link">TIN TỨC - ƯU ĐÃI</a></li>
          <li class="nav-item {{$menu == 'faq' ? 'active' : ''}}"><a href="#" class="nav-link">HỔ TRỢ VÀ LIÊN HỆ</a></li>
      </ul>
    </div>
  </div>
</nav>