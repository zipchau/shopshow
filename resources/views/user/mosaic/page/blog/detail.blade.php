@extends('user.mosaic.layout.master',[
    'title'=>'| Tin Tức - Ưu Đãi | Utop',
    'menu' => 'blog',
])
@section('content')
<section class="hero-wrap hero-wrap-2" style="background-image: url('images/bg_1.jpg');" data-stellar-background-ratio="0.5">
  <div class="overlay"></div>
  <div class="container">
    <div class="row no-gutters slider-text align-items-center justify-content-center">
      <div class="col-md-9 ftco-animate text-center">
        <h1 class="mb-2 bread">{{$blog->title}}</h1>
        <p class="breadcrumbs"><span class="mr-2"><a href="{{route('home')}}">Home <i class="ion-ios-arrow-forward"></i></a></span> <span class="mr-2"><a href="{{route('blog')}}">Blog <i class="ion-ios-arrow-forward"></i></a></span> <span>{{$blog->title}} <i class="ion-ios-arrow-forward"></i></span></p>
      </div>
    </div>
  </div>
</section>
    
<section class="ftco-section">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 ftco-animate">
          {!!nl2br($blog->content)!!}
          <div class="tag-widget post-tag-container mb-5 mt-5">
            <div class="tagcloud">
              @foreach(explode(",", $blog->tags) as $keyTag => $valueTag)
              <a href="#" class="tag-cloud-link">{{$valueTag}}</a>
              @endforeach
            </div>
          </div>
          <section class="ftco-section ftco-no-pb">
              <div class="container-wrap">
                <div class="row no-gutters">
                  @foreach($blog->images as $keyImage =>$valueImage)
                  <div class="col-md-6 col-lg-3 ftco-animate">
                    <div class="project">
                      <img src="{{url($valueImage->src)}}" class="img-fluid" alt="Colorlib Template">
                      <div class="text">
                        <span>{{$blog->title}}</span>
                        <h3>Hình số {{$keyImage +1}}</h3>
                      </div>
                      <a href="{{url($valueImage->src)}}" class="icon image-popup d-flex justify-content-center align-items-center">
                        <span class="icon-expand"></span>
                      </a>
                    </div>
                  </div>
                  @endforeach
                </div>
              </div>
            </section>
        </div> <!-- .col-md-8 -->
        @include('user.mosaic.page.blog.menu_right')
      </div><!-- END COL -->
    </div>
</section>
@stop