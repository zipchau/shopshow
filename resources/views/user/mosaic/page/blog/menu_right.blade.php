<div class="col-lg-4 sidebar ftco-animate">
<div class="sidebar-box ftco-animate">
  <h3>Danh mục</h3>
  <ul class="categories">
    @foreach($categories as $keyCategory => $category)
    <li><a href="#">{{$category->name}}</a></li>
    @endforeach
  </ul>
</div>

<div class="sidebar-box ftco-animate">
  <h3>Popular Articles</h3>
  @foreach($popularBlogs as $keyPopularBlog => $popularBlog)
  <div class="block-21 mb-4 d-flex">
    <a class="blog-img mr-4" style="background-image: url({{$popularBlog->image_first}});"></a>
    <div class="text">
      <h3 class="heading"><a href="#">{{$popularBlog->title}}</a></h3>
      <div class="meta">
        <div><a href="#"><span class="icon-calendar"></span> {{$popularBlog->public_at_show}}</a></div>
      </div>
    </div>
  </div>
  @endforeach
</div>

{{-- <div class="sidebar-box ftco-animate">
  <h3>Tag Cloud</h3>
  <ul class="tagcloud m-0 p-0">
    <a href="#" class="tag-cloud-link">House</a>
    <a href="#" class="tag-cloud-link">Office</a>
    <a href="#" class="tag-cloud-link">Land</a>
    <a href="#" class="tag-cloud-link">Building</a>
    <a href="#" class="tag-cloud-link">Table</a>
    <a href="#" class="tag-cloud-link">Intrior</a>
    <a href="#" class="tag-cloud-link">Exterior</a>
  </ul>
</div> --}}


<div class="sidebar-box ftco-animate">
  <h3>Utop</h3>
  <p>Utop là ứng dụng tích và đổi quà điện tử, cho phép bạn tích lũy điểm hoặc trực tiếp nạp điểm để đổi quà và tiêu điểm tại hàng trăm cửa hàng khác nhau trong hệ thống đối tác Utop tại Việt Nam.</p>
  <p>Đây là mạng lưới được xây dựng dựa trên nền tảng Blockchain – Akachain, đảm bảo đem tới sự bảo mật tuyệt đối cũng như tối ưu hóa trong tốc độ giao dịch của đối tác và khách hàng.</p>
</div>
