@extends('user.mosaic.layout.master',[
    'title'=>'Tin Tức - Ưu Đãi',
    'menu' => 'blog',
])
@section('content')
<section class="hero-wrap hero-wrap-2" style="background-image: url('/mosaic/images/banner_utop.png');" data-stellar-background-ratio="0.5">
  <div class="overlay"></div>
  <div class="container">
    <div class="row no-gutters slider-text align-items-center justify-content-center">
      <div class="col-md-9 ftco-animate text-center">
        <h1 class="mb-2 bread">Tin Tức - Ưu Đãi</h1>
        <p class="breadcrumbs"><span class="mr-2"><a href="/">Trang Chủ <i class="ion-ios-arrow-forward"></i></a></span> <span>Tin Tức - Ưu Đãi <i class="ion-ios-arrow-forward"></i></span></p>
      </div>
    </div>
  </div>
</section>

<section class="ftco-section">
  <div class="container">
    <div class="row">
      <div class="col-lg-8">
        <div class="row">
          @foreach($blogs as $keyBlog => $valueBlog)
          <div class="col-md-6 ftco-animate">
            <div class="blog-entry">
              <a href="{{route('blog.detail',['slug'=>$valueBlog->slug,'id'=>$valueBlog->id])}}" class="block-20" style="background-image: url('{{$valueBlog->image_first}}');">
              </a>
              <div class="text d-flex py-4">
                <div class="meta mb-3">
                  <div><a href="#">{{$valueBlog->public_at_show}}</a></div>
                </div>
                <div class="desc pl-3">
                  <h3 class="heading"><a href="{{route('blog.detail',['slug'=>$valueBlog->slug,'id'=>$valueBlog->id])}}">{{$valueBlog->title}}</a></h3>
                </div>
              </div>
            </div>
          </div>
          @endforeach
        </div>
        <div class="row no-gutters my-5">
          <div class="col text-center">
            <div class="block-27">
              {{ $blogs->appends($_GET)->links("user.mosaic.layout.paginate") }}
            </div>
          </div>
        </div>
      </div>
    
      @include('user.mosaic.page.blog.menu_right')
      </div><!-- END COL -->
    </div>
  </div>
</section>
@stop