@extends('user.mosaic.layout.master',[
    'title'=>'Trang Chủ',
])
@section('content')
@include('user.mosaic.layout.accessories.slider')
<section class="ftco-section ftc-no-pb" id="aboutUs">
    <div class="container">
        <div class="row no-gutters">
            <div class="col-md-5 " style="background-image: url(http://utop.esy.es/wp-content/uploads/2019/02/iphone-x-mockup-1543822110-361x573.png);">
            </div>
            <div class="col-md-7 wrap-about pb-md-5 ftco-animate">
      <div class="heading-section mb-5 pl-md-5">
        <div class="pl-md-5 ml-md-5">
            <span class="subheading subheading-with-line"><small class="pr-2 bg-white">Giới thiệu</small></span>
            <h2 class="mb-4">Utop</h2>
        </div>
      </div>
      <div class="pl-md-5 ml-md-5 mb-5">
                    <p>Utop là ứng dụng tích và đổi quà điện tử, cho phép bạn tích lũy điểm hoặc trực tiếp nạp điểm để đổi quà và tiêu điểm tại hàng trăm cửa hàng khác nhau trong hệ thống đối tác Utop tại Việt Nam.</p>
                    <br>
                    <p>Đây là mạng lưới được xây dựng dựa trên nền tảng Blockchain – Akachain, đảm bảo đem tới sự bảo mật tuyệt đối cũng như tối ưu hóa trong tốc độ giao dịch của đối tác và khách hàng.</p>
                    <br>
                    <hr>
                    <br>
                    <div class="row">
                      <div class="col-md-6">
                        <a id="tai-app-ios" class="tai-app-ios" href="https://itunes.apple.com/vn/app/utop/id1442447113?mt=8">Tải App iOS</a>
                      </div>
                      <div class="col-md-6">
                        <a id="tai-app-android" class="tai-app-android" href="https://play.google.com/store/apps/details?id=com.utop">Tải App Android</a>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="ftco-services">
  <div class="container">
    <div class="row justify-content-start mt-5 py-5">
      <div class="col-md-4 heading-section ftco-animate">
        <h2 class="mb-4">Utop!</h2>
        <p>Utop đã có mặt trên vài quốc gia.</p>
      </div>
      <div class="col-md-8 ftco-animate">
        <div class="row d-flex">
            <div class="col-md-6 d-flex align-items-stretch">
                <div class="media block-6 services services-2 d-block bg-light p-4 mb-4">
                  <div class="icon d-flex justify-content-center align-items-center">
                        <span class="flaticon-layers"></span>
                  </div>
                  <div class="media-body p-2 mt-3">
                    <h3 class="heading">TÍCH ĐIỂM MỌI NƠI</h3>
                    <p>Cho phép bạn tích điểm nhanh chóng với các đối tác trong hệ thống của Utop chỉ với app Utop.</p>
                  </div>
                </div>
            </div>
            <div class="col-md-6 d-flex align-items-stretch">
                <div class="media block-6 services services-2 d-block bg-light p-4 mb-4">
                  <div class="icon d-flex justify-content-center align-items-center">
                        <span class="flaticon-layers"></span>
                  </div>
                  <div class="media-body p-2 mt-3">
                    <h3 class="heading">QUÀ TẶNG THIẾT THỰC</h3>
                    <p>Không cầu kỳ, chỉ cần món quà đơn giản và thiết thực: từ thẻ nạp điện thoại đến voucher từ nhiều thương hiệu: CGV, Phúc Long, Gogi House,…</p>
                  </div>
                </div>
            </div>
            <div class="col-md-6 d-flex align-items-stretch">
                <div class="media block-6 services services-2 d-block bg-light p-4 mb-4">
                  <div class="icon d-flex justify-content-center align-items-center">
                        <span class="flaticon-layers"></span>
                  </div>
                  <div class="media-body p-2 mt-3">
                    <h3 class="heading">ĐƠN GIẢN, NHANH CHÓNG</h3>
                    <p>3 NHANH dành cho các tính năng của Utop: Tích điểm nhanh, nhận ưu đãi nhanh và đổi quà nhanh.</p>
                  </div>
                </div>
            </div>
            <div class="col-md-6 d-flex align-items-stretch">
                <div class="media block-6 services services-2 d-block bg-light p-4 mb-4">
                  <div class="icon d-flex justify-content-center align-items-center">
                        <span class="flaticon-layers"></span>
                  </div>
                  <div class="media-body p-2 mt-3">
                    <h3 class="heading">GIAO DỊCH AN TOÀN</h3>
                    <p>Đảm bảo sự bảo mật tuyệt đối trong giao dịch nhờ vào công nghệ Blockchain – Akachain</p>
                  </div>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</section>

    <section class="ftco-section testimony-section bg-light">
      <div class="container">
        <div class="row justify-content-center mb-5 pb-3">
          <div class="col-md-7 heading-section ftco-animate">
            <span class="subheading subheading-with-line"><small class="pr-2 bg-light">Hướng dẫn</small></span>
            <h2 class="mb-4">Các tính năng chính</h2>
            <!-- <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in</p> -->
          </div>
        </div>
        <div class="row ui-tabbed-showcase">
          
            <!-- Device Slider Column -->
            <div class="col-md-6 col-sm-5 animate" data-show="fade-in-right">
              <!-- Device Slider -->
              <div class="ui-device-slider">
                <!-- Device Image -->
                <div class="device">
                  <img src="/mosaic/images/device-slider/device.png" data-uhd alt="Applify - App Landing HTML Template" />
                </div>
                <!-- Slider Images -->
                <div id="device-slider-2" class="screens owl-carousel">
                  <div class="item">
                    <img src="/mosaic/images/device-slider/app-screen-1.jpg" data-uhd alt="Applify - App Landing HTML Template" />  
                  </div>
                  <div class="item">
                    <img src="/mosaic/images/device-slider/app-screen-8.jpg" data-uhd alt="Applify - App Landing HTML Template" />  
                  </div>
                  <div class="item">
                    <img src="/mosaic/images/device-slider/app-screen-2.jpg" data-uhd alt="Applify - App Landing HTML Template" />  
                  </div>
                  <div class="item">
                    <img src="/mosaic/images/device-slider/app-screen-3.jpg" data-uhd alt="Applify - App Landing HTML Template" />  
                  </div>
                </div>
              </div><!-- .ui-device-slider -->
            </div><!-- Device Slider Column -->
            
            <!-- Tabs Column -->
            <div class="col-md-6 col-sm-7" data-vertical_center="true">
              <!-- UI Tabs -->
              <div class="ui-tabs ui-green">
                <!-- Nav Tabs -->
              <ul class="nav nav-tabs" role="tablist">
                <!-- Nav Tab 1 -->
                <li role="presentation" class="nav-item">
                  <a class="nav-link active" href="#home" role="tab" data-toggle="tab" data-toggle_screen="1" data-toggle_slider="device-slider-2">
                  Home
                  </a>
                </li>
                <!-- Nav Tab 2 -->
                <li role="presentation" class="nav-item">
                  <a class="nav-link" href="#profile" role="tab" data-toggle="tab" data-toggle_screen="2" data-toggle_slider="device-slider-2">
                     Profile
                  </a>
                </li>
                <!-- Nav Tab 3 -->
                <li role="presentation" class="nav-item">
                  <a class="nav-link" href="#messages" role="tab" data-toggle="tab" data-toggle_screen="3" data-toggle_slider="device-slider-2">
                    Messages
                  </a>
                </li>
                <!-- Nav Tab 4 -->
                <li role="presentation" class="nav-item">
                  <a class="nav-link" href="#settings" role="tab" data-toggle="tab" data-toggle_screen="4" data-toggle_slider="device-slider-2">
                    Settings
                  </a>
                </li>
              </ul>

              <!-- Tab Panels -->
              <div class="tab-content">
                <!-- Tab 1 -->
                <div role="tabpanel" class="tab-pane fade show active" id="home">
                  <p class="sub-heading">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                  <ul class="ui-checklist">
                    <li>
                      <h6 class="heading">Consectetur adipisicing</h6>
                    </li>
                    <li>
                      <h6 class="heading">Eiusmod tempor incididunt</h6>
                    </li>
                    <li>
                      <h6 class="heading">Ut enim ad minim</h6>
                    </li>
                    <li>
                      <h6 class="heading">Lorem ipsum dolor</h6>
                    </li>
                  </ul>
                </div>
                <!-- Tab 2 -->
                <div role="tabpanel" class="tab-pane fade" id="profile">
                  <p class="sub-heading">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non.</p>
                  <ul class="ui-checklist">
                    <li>
                      <h6 class="heading">Eiusmod tempor incididunt</h6>
                    </li>
                    <li>
                      <h6 class="heading">Consectetur adipisicing</h6>
                    </li>
                    <li>
                      <h6 class="heading">Lorem ipsum dolor</h6>
                    </li>
                    <li>
                      <h6 class="heading">Ut enim ad minim</h6>
                    </li>
                  </ul>
                </div>
                <!-- Tab 3 -->
                <div role="tabpanel" class="tab-pane fade" id="messages">
                  <p class="sub-heading">Donec nec dolor erat, condimentum sagittis sem. Praesent porttitor porttitor risus, dapibus rutrum ipsum gravida et. Integer lectus nisi.</p>
                  <ul class="ui-checklist">
                    <li>
                      <h6 class="heading">Ut enim ad minim</h6>
                    </li>
                    <li>
                      <h6 class="heading">Lorem ipsum dolor</h6>
                    </li>
                    <li>
                      <h6 class="heading">Eiusmod tempor incididunt</h6>
                    </li>
                    <li>
                      <h6 class="heading">Consectetur adipisicing</h6>
                    </li>
                  </ul>
                </div>
                <!-- Tab 4 -->
                <div role="tabpanel" class="tab-pane fade" id="settings">
                  <p class="sub-heading">Sed consectetur dignissim dignissim. Donec pretium est sit amet ipsum fringilla feugiat. Aliquam erat volutpat. </p>
                  <ul class="ui-checklist">
                    <li>
                      <h6 class="heading">Eiusmod tempor incididunt</h6>
                    </li>
                    <li>
                      <h6 class="heading">Ut enim ad minim</h6>
                    </li>
                    <li>
                      <h6 class="heading">Consectetur adipisicing</h6>
                    </li>
                    <li>
                      <h6 class="heading">Lorem ipsum dolor</h6>
                    </li>
                  </ul>
                </div>
              </div><!-- .tab-content -->
          </div><!-- .ui-tabs -->
      </div>
    </section>
            
        {{-- <section class="ftco-section">
            <div class="container">
                <div class="row justify-content-center mb-5 pb-3">
          <div class="col-md-7 heading-section ftco-animate">
            <span class="subheading subheading-with-line"><small class="pr-2 bg-white">Expert Team</small></span>
            <h2 class="mb-4">Our Architect Team</h2>
            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in</p>
          </div>
        </div>  
                <div class="row">
                    <div class="col-md-6 col-lg-3 ftco-animate">
                        <div class="staff">
                            <div class="img" style="background-image: url(/mosaic/images/team-1.jpg);"></div>
                            <div class="text px-4 pt-4">
                                <h3>John Wilson</h3>
                                <span class="position mb-2">Co-Founder / CEO</span>
                                <div class="faded">
                                    <p>I am an ambitious workaholic, but apart from that, pretty simple person.</p>
                                    <ul class="ftco-social d-flex">
                        <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
                        <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
                        <li class="ftco-animate"><a href="#"><span class="icon-google-plus"></span></a></li>
                        <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
                      </ul>
                  </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3 ftco-animate">
                        <div class="staff">
                            <div class="img" style="background-image: url(/mosaic/images/team-2.jpg);"></div>
                            <div class="text px-4 pt-4">
                                <h3>David Smith</h3>
                                <span class="position mb-2">Achitect</span>
                                <div class="faded">
                                    <p>I am an ambitious workaholic, but apart from that, pretty simple person.</p>
                                    <ul class="ftco-social d-flex">
                        <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
                        <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
                        <li class="ftco-animate"><a href="#"><span class="icon-google-plus"></span></a></li>
                        <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
                      </ul>
                  </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3 ftco-animate">
                        <div class="staff">
                            <div class="img" style="background-image: url(/mosaic/images/team-3.jpg);"></div>
                            <div class="text px-4 pt-4">
                                <h3>David Smith</h3>
                                <span class="position mb-2">Achitect</span>
                                <div class="faded">
                                    <p>I am an ambitious workaholic, but apart from that, pretty simple person.</p>
                                    <ul class="ftco-social d-flex">
                        <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
                        <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
                        <li class="ftco-animate"><a href="#"><span class="icon-google-plus"></span></a></li>
                        <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
                      </ul>
                  </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3 ftco-animate">
                        <div class="staff">
                            <div class="img" style="background-image: url(/mosaic/images/team-4.jpg);"></div>
                            <div class="text px-4 pt-4">
                                <h3>David Smith</h3>
                                <span class="position mb-2">Achitect</span>
                                <div class="faded">
                                    <p>I am an ambitious workaholic, but apart from that, pretty simple person.</p>
                                    <ul class="ftco-social d-flex">
                        <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
                        <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
                        <li class="ftco-animate"><a href="#"><span class="icon-google-plus"></span></a></li>
                        <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
                      </ul>
                  </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section> --}}

  <section class="ftco-section bg-light">
    <div class="container">
      <div class="row justify-content-start mb-5 pb-2">
      <div class="col-md-4 heading-section ftco-animate">
        <span class="subheading subheading-with-line"><small class="pr-2 bg-white">TIN TỨC</small></span>
        <h2 class="mb-4">ƯU ĐÃI</h2>
      </div>
      <div class="col-md-8 pl-md-5 heading-section ftco-animate">
        <div class="pl-md-4 border-line">
            <p>Thông tin về các voucher mới nhất.</p>
        </div>
      </div>
    </div>  
    <div class="row">
      @foreach($popularBlogs as $keyPopularBlog => $popularBlog)
      <div class="col-md-4 ftco-animate">
        <div class="blog-entry">
          <a href="blog-single.html" class="block-20" style="background-image: url('{{$popularBlog->image_first}}');">
          </a>
          <div class="text d-flex py-4">
            <div class="meta mb-3">
              <div><a href="#">{{$popularBlog->public_at_show}}</a></div>
            </div>
            <div class="desc pl-3">
                <h3 class="heading"><a href="#">{{$popularBlog->title}}</a></h3>
              </div>
          </div>
        </div>
      </div>
      @endforeach
    </div>
  </section>

        <section class="ftco-section ftco-client">
            <div class="container">
                <div class="row justify-content-start mb-5 pb-2">
          <div class="col-md-4 heading-section ftco-animate">
            <span class="subheading subheading-with-line"><small class="pr-2 bg-white">Thương hiệu</small></span>
            <h2 class="mb-4">Quà tặng</h2>
          </div>
          <div class="col-md-8 pl-md-5 heading-section ftco-animate">
            <div class="pl-md-4 border-line">
                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
            </div>
          </div>
        </div>
        <div class="row ftco-animate">
          <div class="col-md-12">
            <div class="carousel-client owl-carousel">
              <div class="item">
                <a href="#" class="client text-center p-5" style="height: 126px">
                    <img src="https://utop.vn/wp-content/uploads/2019/04/492d91d17f3459b09e1c7daa139b5738-1.jpeg" >
                </a>
              </div>
              <div class="item">
                <a href="#" class="client text-center p-5" style="height: 126px">
                    <img src="https://utop.vn/wp-content/uploads/2019/04/unnamed-1.png" >
                </a>
              </div>
              <div class="item">
                <a href="#" class="client text-center p-5">
                    CLient Logo 3
                </a>
              </div>
              <div class="item">
                <a href="#" class="client text-center p-5">
                    CLient Logo 4
                </a>
              </div>
            </div>
          </div>
        </div>
            </div>
        </section>
@stop