<?php

namespace App\Models;

use App\Models\BaseModel;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends BaseModel
{
    use SoftDeletes;

    protected $fillable = [
        'product_category_id',
        'name',
        'slug',
        'content',
        'public_at',
        'status',
        'price_regular',
        'price_sale',
        'tags',
    ];

    protected $appends = [
        'status_text',
        'price_html',
    ];

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = $value;
        $this->attributes['slug'] = str_slug($value);
    }

    public function getStatusTextAttribute()
    {
        return $this->status == 2 ? "Hide" : "Show";
    }

    public function setTagsAttribute($value)
    {
        $this->attributes['tags'] = "," . $value . ",";
    }

    public function getTagsAttribute()
    {
        return rtrim(ltrim($this->attributes['tags'], ','), ',');
    }

    public function getPriceHtmlAttribute()
    {
        $monetary_unit = config('common.monetary_unit');
        if ($this->price_regular < $this->price_sale) {
            return $this->price_sale . ' ' . $monetary_unit . ' <del class="text-muted">' . $this->price_regular . ' ' . $monetary_unit . '</del>';
        }
        return $this->price_sale . " " . $monetary_unit;
    }

    public function setPublicAtAttribute($value)
    {
        $this->attributes['public_at'] = Carbon::parse($value);
    }

    public function images()
    {
        return $this->morphMany(Images::class, 'imagetable');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\ProductCategory', 'product_category_id');
    }
}
