<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Accessory\TrashScope;
/**
 * BaseModel
 */
class BaseModel extends Model
{
    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();
        // static::addGlobalScope(new TrashScope);
    }

}
