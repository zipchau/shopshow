<?php

namespace App\Models;

use Auth;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasRoles;
    use Notifiable;
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'email',
        'phone',
        'address',
        'email_verified_at',
        'approved_at',
        'user_approved',
        'admin',
        'title',
        'password',
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = [
        'title_text', 'image',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getTitleTextAttribute()
    {
        if ($this->attributes['admin'] == 1) {
            return "Admin";
        }
        return config('form.permission.title')[$this->attributes['title']] ?? 'Error Title';
    }

    public function userApprove()
    {
        return $this->belongsTo(User::class, 'user_approved');
    }

    public function getImageAttribute()
    {
        return $this->images->src ?? 'adstyle/images/no_user.png';
    }

    public function setTitleAttribute($title)
    {
        if ($title == 'admin') {
            $this->attributes['admin'] = 1;
        }
        $this->attributes['title'] = $title;
    }

    public function setNameAttribute($name)
    {
        $this->attributes['name'] = $name;
        $this->attributes['slug'] = str_slug($name);
    }

    public function setApprovedAtAttribute($approved_at)
    {
        $this->attributes['approved_at']   = $approved_at;
        $this->attributes['user_approved'] = Auth::user()->id;
    }

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

    public function images()
    {
        return $this->morphOne(Images::class, 'imagetable');
    }

    public function approved()
    {
        if($this->approved_at){
            return true;
        }
        return false;
    }
    
    public function scopeNotMaster($query)
    {
        return $query->whereNotIn('id', ['1']);
    }
}
