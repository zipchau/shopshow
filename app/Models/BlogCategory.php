<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\BaseModel;

class BlogCategory extends BaseModel
{
    use SoftDeletes;

    protected $fillable = [
        'parent_id',
        'name',
        'slug',
        'status',
    ];

    protected $appends = [
        'status_text'
    ];
    
    public function setNameAttribute($name)
    {
        $this->attributes['name'] = $name;
        $this->attributes['slug'] = str_slug($name);
    }

    public function getStatusTextAttribute()
    {
        return $this->status == 2 ? "Hide" : "Show";
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\BlogCategory', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('App\Models\BlogCategory', 'parent_id');
    }

    public function checkDelete()
    {
        if($this->children->count() != 0  || $this->blogs->count() != 0){
            return false;
        }
        return true;
    }

    public function blogs()
    {
        return $this->hasMany('App\Models\Blog', 'blog_category_id')->orderBy('updated_at','DESC');
    }

    public function scopeNoParent($query)
    {
        return $query->whereNotIn('id',[1]);
    }

    public function scopeShow($query)
    {
        return $query->whereStatus(1);
    }
}
