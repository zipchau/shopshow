<?php

namespace App\Models;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductCategory extends BaseModel
{
    use SoftDeletes;

    protected $fillable = [
        'parent_id',
        'name',
        'slug',
        'status',
    ];

    protected $appends = [
        'status_text'
    ];
    
    public function setNameAttribute($name)
    {
        $this->attributes['name'] = $name;
        $this->attributes['slug'] = str_slug($name);
    }

    public function getStatusTextAttribute()
    {
        return $this->status == 2 ? "Hide" : "Show";
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\ProductCategory', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('App\Models\ProductCategory', 'parent_id');
    }

    public function checkDelete()
    {
        if($this->children->count() != 0  || $this->products->count() != 0){
            return false;
        }
        return true;
    }

    public function products()
    {
        return $this->hasMany('App\Models\Product', 'product_category_id')->orderBy('updated_at','DESC');
    }
}
