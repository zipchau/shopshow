<?php

namespace App\Models;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Images extends BaseModel
{
    use SoftDeletes;
    /**
     * status 2-index 1-defult use when multiple
     */
    protected $fillable = ['src', 'alt', 'description','status'];

    public function imagetable()
    {
        return $this->morphTo();
    }

}