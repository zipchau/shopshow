<?php

namespace App\Models\Accessory;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class TrashScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        $builder->withTrashed();
        $wheres = $builder->getQuery()->wheres;
        foreach($wheres as $key => $data) {
            if($data['column'] ==  $model->getTable() . '.deleted_at' && $data['type'] == "Null") {
                unset($builder->getQuery()->wheres[$key]);
            }
        }
        
        return $builder;
    }
}