<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class SliderImage extends BaseModel
{
    use SoftDeletes;
    protected $fillable = [
        'public_at',
        'prioritize',
        'title',
        'content',
        'btn_name',
        'btn_url',
        'type',
    ];

    protected $appends = ['image'];

    public function getImageAttribute()
    {
        return $this->images->src ?? 'mosaic/images/bg_2.jpg';
    }

    public function images()
    {
        return $this->morphOne(Images::class, 'imagetable');
    }

    public function scopeGetBannerBlog($query)
    {
        return $query
            ->whereType('blog')
            ->orderBy('prioritize', 'ASC')
            ->whereDate('public_at', '<=', Carbon::now());
    }
}
