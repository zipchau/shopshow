<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class BrandCategory extends BaseModel
{
    use SoftDeletes;

    protected $fillable = [
        'parent_id',
        'name',
        'slug',
        'status',
    ];

     public function setNameAttribute($name)
    {
        $this->attributes['name'] = $name;
        $this->attributes['slug'] = str_slug($name);
    }

    public function brands()
    {
        return $this->hasMany('App\Models\Brand', 'brand_category_id')->orderBy('updated_at', 'DESC');
    }
}
