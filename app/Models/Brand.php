<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Brand extends BaseModel
{
    use SoftDeletes;

    protected $fillable = [
        'brand_category_id',
        'name',
        'slug',
        'content',
        'url',
    ];

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = $value;
        $this->attributes['slug']  = str_slug($value);
    }

    public function images()
    {
        return $this->morphOne(Images::class, 'imagetable');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\BrandCategory', 'brand_category_id');
    }
}
