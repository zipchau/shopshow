<?php

namespace App\Models;

use App\Models\BaseModel;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class Blog extends BaseModel
{
    use SoftDeletes;

    protected $fillable = [
        'blog_category_id',
        'title',
        'slug',
        'content',
        'public_at',
        'status',
        'tags',
        'popular',
    ];

    protected $appends = [
        'status_text','image_frist'
    ];

    public function setTagsAttribute($value)
    {
        $this->attributes['tags'] = "," . $value . ",";
    }

    public function getTagsAttribute()
    {
        return rtrim(ltrim($this->attributes['tags'], ','), ',');
    }

    public function setTitleAttribute($value)
    {
        $this->attributes['title'] = $value;
        $this->attributes['slug']  = str_slug($value);
    }

    public function getStatusTextAttribute()
    {
        return $this->status == 2 ? "Hide" : "Show";
    }

    public function getPopularTextAttribute()
    {
        return $this->popular == 1 ? "No" : "Yes";
    }

    public function getPublicAtShowAttribute()
    {
        return date("d-m-Y", strtotime($this->public_at));
    }

    public function setPublicAtAttribute($value)
    {
        $this->attributes['public_at'] = Carbon::parse($value);
    }

    public function getImageFirstAttribute()
    {
        return $this->images->where('status',2)->first()->src;
    }

    public function images()
    {
        return $this->morphMany(Images::class, 'imagetable');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\BlogCategory', 'blog_category_id');
    }

    public function scopeSearchTags($query, $tags = [])
    {
        if (is_array($tags)) {
            foreach ($tags as $tag) {
                $query->orWhere('tags', 'like', '%,' . $tag . ".%");
            }
        }
    }

    public function scopeGetPopular($query)
    {
        return $query->wherePopular(2);
    }

    public function scopeUserGetList($query)
    {
        return $query->whereStatus(1)
        ->whereDate('public_at', '<=', Carbon::now())
        ->orderBy('public_at', 'DESC');
    }
}
