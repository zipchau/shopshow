<?php

namespace App\Jobs;

use App\Mail\SendMailContact;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Mail;

class SendEmailContact implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $details;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($details)
    {
        $details = [
            'name'    => 'alvin',
            'phone'   => '0962720738',
            'email'   => 'zipchau@gmail.com',
            'content' => 'testContent',
        ];
        $this->details = $details;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        sleep(30);
        echo 'Start send email';
        $email = new SendMailContact($this->details);
        Mail::to('zipchau@gmail.com')->send($email);
        echo 'End send email';
    }
}
