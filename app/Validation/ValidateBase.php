<?php 
namespace App\Validation;

/**
 * ValidateBase
 */
class ValidateBase
{
    function __construct()
    {
        $methods = get_class_methods($this);

        foreach ($methods as $method) {
            if ($method != '__construct') {
                $this->{$method}();
            }
        }
    }
}
?>