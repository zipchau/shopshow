<?php
namespace App\Validation;

use Hash;
use Validator;

/**
 * CommonValidate
 */
class CommonValidate extends ValidateBase
{
    /**
     * The integer check has excluded $param
     * Default remove dot character
     * validate name integer_remove
     * @return boolen
     */
    public function integerRemoved()
    {
        Validator::extend('integerRemove', function ($attribute, $value, $parameters) {
            $expectCharacter = $parameters[0] ?? '.';
            $value           = (int) str_replace($expectCharacter, '', $value);;
            return is_int($value);
        }, 'The :attribute must be an integer.');
    }

    /**
     * check tel
     * Default remove dot character
     * validate name tel
     * @return boolen
     */
    public function tel()
    {
        Validator::extend('tel', function ($attribute, $value, $parameters) {
            return preg_match("/[0-9]{10,11}/", $value);
        });
    }

    public function oldPassword()
    {
        Validator::extend('check_old_password', function ($attribute, $value, $parameters) {
            if (Hash::check($value, $parameters[0])) {
                return true;
            } else {
                return false;
            }

        });
    }
}
