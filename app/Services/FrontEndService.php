<?php
namespace App\Services;

use App\Models\Blog;
use App\Models\BlogCategory;
use App\Models\Brand;
use App\Models\SliderImage;

/**
 * FrontEndService
 */
class FrontEndService
{
    public function getBlogCategories()
    {
        return BlogCategory::noParent()->show()->select()->get();
    }

    public function getBannerBlog()
    {
        return SliderImage::with('images')->GetBannerBlog()->get();
    }

    public function getBlogs($paginate = 6)
    {
        return Blog::with('images', 'category')->userGetList()->paginate($paginate);
    }

    public function getBlogCategoryWithArticle($id,$paginate = 6)
    {
        return Blog::with('images', 'category')->whereBlogCategoryId($id)->userGetList()->paginate($paginate);
    }

    public function getBlogsPopular($limit = 3)
    {
        return Blog::with('images', 'category')->userGetList()->getPopular()->limit($limit)->get();
    }

    public function getBlogDetail($id)
    {
        return Blog::with('images', 'category')->findOrFail($id);
    }

    public function getBrands($paginate = 6)
    {
        $columns = 5;
        $lists   = Brand::with('images', 'category')->get()->toArray();
        return array_chunk($lists, $columns);
    }
}
