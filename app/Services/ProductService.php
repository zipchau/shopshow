<?php
namespace App\Services;

use App\Models\Product;
use App\Services\ImageService;

/**
 * ProductServices
 */
class ProductService
{
    protected $imageService;

    /**
     * get list
     * @return collection
     */
    public function __construct(ImageService $imageService)
    {
        $this->imageService = $imageService;
    }

    public function getList()
    {
        $paginate = config('form.common.paginate');
        $data     = Product::with('category')->select()->paginate($paginate);
        return $data;
    }

    /**
     * Find model by id
     * @param  int id
     * @return void
     */
    public function findById($id)
    {
        return Product::findOrFail($id);
    }

    /**
     * Destroy Model
     * @param  int $id
     * @return boolean
     */
    public function destroyById($id)
    {
        $model = $this->findById($id);
        $model->images()->delete();
        $model->delete();
        return true;
    }

    /**
     * Restore Model
     * @param  int $id
     * @return boolean
     */
    public function restoreById($id)
    {
        $model = $this->findById($id);
        $model->restore();
        $model->images()->restore();
        return true;
    }

    /**
     * Store or update mode;
     * @param  array $request
     * @param  int id
     * @return void
     */
    public function storeOrUpdate($request, $id = null)
    {
        if ($id) {
            $model             = $this->findById($id);
            $request['status'] = $request['status'] ?? 1;
            $model->update($request);
            if (isset($request['file_uploads'])) {
                $request['file_uploads'] = $this->setIndexImage($request['file_uploads']);
                $this->imageService->handleUpdateMultipleImage($model, $request['file_uploads']);
            }
        } else {
            $request['status'] = $request['status'] ?? 1;
            $model             = Product::create($request);
            if (isset($request['file_uploads'])) {
                $request['file_uploads'] = $this->setIndexImage($request['file_uploads']);
                foreach ($request['file_uploads'] as $key => $item) {
                    $model->images()->create(['src' => $item['src'], 'status' => $item['status'] ?? 1]);
                }
            }
        }
        return $model;
    }

    /**
     * config attribute status
     * @param array $image
     * @return array
     */
    public function setIndexImage($images)
    {
        $countIndex = 0;
        $firstIndex = 0;
        $countFor   = 1;

        foreach ($images as $key => $value) {
            if ($countFor == 1) {
                $firstIndex = $key;
            }
            if (array_key_exists('status', $value)) {
                $countIndex++;
            }
            if ($countIndex == 0 && $countFor == sizeof($images)) {
                $images[$firstIndex]['status'] = 2;
            }
            if ($countIndex > 1) {
                unset($images[$key]['status']);
            }
            $countFor++;
        }
        return $images;
    }
}
