<?php
namespace App\Services;

use App\Models\BlogCategory;

/**
 * BlogCategoryServices
 */
class BlogCategoryService
{
    /**
     * get list
     * @return collection
     */
    public function getList()
    {
        $paginate = config('form.common.paginate');
        $data     = BlogCategory::with('parent')->noParent()->select()->paginate($paginate);
        return $data;
    }

    /**
     * Find model by id
     * @param  int id
     * @return void
     */
    public function findById($id)
    {
        return BlogCategory::noParent()->findOrFail($id);
    }

    /**
     * Store or update mode;
     * @param  array $request
     * @param  int id
     * @return void
     */
    public function storeOrUpdate($request, $id = null)
    {
        if ($id) {
            $data              = $this->findById($id);
            $request['status'] = $request['status'] ?? 1;
            $data->update($request);
        } else {
            $data = BlogCategory::create($request);
        }
        return $data;
    }

    /**
     * Destroy Model
     * @param  int $id
     * @return boolean
     */
    public function destroyById($id)
    {
        $model = $this->findById($id);
        $model->delete();
        return true;
    }

    /**
     * Restore Model
     * @param  int $id
     * @return boolean
     */
    public function restoreById($id)
    {
        $model = $this->findById($id);
        $model->restore();
        return true;
    }

    /**
     * Get parent list
     * @param  int $id
     * @return array
     */
    public function getParentList($id = 0)
    {
        $data = BlogCategory::select()->whereNotIn('id', [$id])->where('parent_id', 0)->orderBy('updated_at', 'DESC')->get()->toArray();

        if ($id != 0) {
            $model = $this->findById($id);
            if ($model->children()->count() > 0) {
                $data = [];
            }
        }
        $noParentData = [
            'id'   => 0,
            'name' => 'No Parent',
        ];
        array_unshift($data, $noParentData);
        return $data;
    }

    /**
     * get list only children
     * @return collection
     */
    public function getListOnlyChildren()
    {
        $data = BlogCategory::with('children')
            ->select()
            ->whereNotIn('parent_id', [0])
            ->orderBy('created_at', 'DESC')
            ->get();
        return $data;
    }

}
