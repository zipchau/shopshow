<?php
namespace App\Services\Accessories;

use File;
use Validator;

trait ImageFileUploadAccessory
{
    protected function get_image_type($target_file)
    {
        $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
        return $imageFileType;
    }

    protected function remove_extension_from_image($image)
    {
        $extension = $this->get_image_type($image); //get extension
        $only_name = basename($image, '.' . $extension); // remove extension
        return $only_name;
    }
    public function convert_image($convert_type, $target_dir, $image_name, $image_quality = 60)
    {
        $target_dir    = getenv('DOCUMENT_ROOT') . "$target_dir/";
        $image         = $target_dir . $image_name;
        $img_name      = $this->remove_extension_from_image($image);
        $img_name_type = pathinfo($image, PATHINFO_EXTENSION);

        //to png
        if ($convert_type == 'png') {
            $binary = imagecreatefromstring(file_get_contents($image));
            //third parameter for ImagePng is limited to 0 to 9
            //0 is uncompressed, 9 is compressed
            //so convert 100 to 2 digit number by dividing it by 10 and minus with 10
            $image_quality = floor(10 - ($image_quality / 10));
            ImagePNG($binary, $target_dir . $img_name . '.' . $convert_type, $image_quality);
            return $img_name . '.' . $convert_type;
        }

        //to jpg
        if ($convert_type == 'jpg') {
            $binary = imagecreatefromstring(file_get_contents($image));
            imageJpeg($binary, $target_dir . $img_name . '.' . $convert_type, $image_quality);
            return $img_name . '.' . $convert_type;
        }
        // origin
        if ($convert_type == 'ori') {
            return $img_name . '.' . $img_name_type;
        }
        if ($convert_type == 'gif') {
            $binary = imagecreatefromstring(file_get_contents($image));
            imageGif($binary, $target_dir . $img_name . '.' . $convert_type, $image_quality);
            return $img_name . '.' . $convert_type;
        }
        return false;
    }

    public function UploadMultiple($request, $type = null)
    {
        $success = [];
        $errors  = [];
        foreach ($request->file('files') as $key => $file) {
            $handle = $this->handleUploadMultiple($request, $key, $type);
            if ($handle['status'] == false) {
                $errors[] = $handle;
            }
            if ($handle['status'] == true) {
                $success[] = $handle;
            }
        }
        return [
            'success' => $success,
            'errors'  => $errors,
        ];
    }

    public function handleUploadMultiple($request, $key, $type = null)
    {
        $validator = Validator::make($request->all(),
            [
                'files.' . $key => 'image',
            ],
            [
                'files.' . $key . '.image' => 'The file must be an image (jpeg, png, bmp, gif, or svg)',
            ]);
        if ($validator->fails()) {
            return [
                'status' => false,
                'file'   => $request->file('files')[$key]->getClientOriginalName() ?? null,
                'errors' => $validator->errors()->getMessages()['files.' . $key],
            ];
        }
        $file = $request->file('files')[$key];

        if ($type) {
            $dir = 'uploads/' . $type;
        } else {
            $dir = 'uploads';
        }
        $date       = date("dmY");
        $originName = $file->getClientOriginalName();
        $file_name  = rand() . $date . '.' . $file->getClientOriginalExtension();
        if ($file->move($dir, $file_name)) {
            $newname  = $dir;
            $imageout = $this->convert_image('jpg', '/' . $newname, $file_name);
            $newname  = $dir . '/' . $imageout;
            return ['status' => true, 'filename' => $newname, 'filenameorigin' => $originName];
        }
        return ['status' => false, 'errors' => $originName];
    }

    public function handleUpload($request, $type = null)
    {
        $validator = Validator::make($request->all(),
            [
                'file' => 'image',
            ],
            [
                'file.image' => 'The file must be an image (jpeg, png, bmp, gif, or svg)',
            ]);
        if ($validator->fails()) {
            return json_encode(array(
                'status' => false,
                'errors' => $validator->errors(),
            ));
        }
        $file = request()->file;
        if ($type) {
            $dir = 'uploads/' . $type;
        } else {
            $dir = 'uploads';
        }
        $date       = date("/Y/m/d");
        $originName = $file->getClientOriginalName();
        $file_name  = rand() . '.' . $file->getClientOriginalExtension();
        if ($file->move($dir, $file_name)) {
            $newname  = $dir;
            $imageout = $this->convert_image('jpg', '/' . $newname, $file_name);
            $newname  = $dir . '/' . $imageout;
            return json_encode(['status' => true, 'filename' => $newname, 'filenameorigin' => $originName]);
        }
        return json_encode(['status' => false]);
    }

    public function removeBySrc($request)
    {
        if (isset($request['src'])) {
            if (File::exists($request['src'])) {
                File::delete($request['src']);
                return json_encode(['status' => true, 'file' => $request['src']]);
            }
            return json_encode(['status' => false, 'message' => 'File not found!']);
        }
        return json_encode(['status' => false, 'message' => 'File not found!']);

    }
}
