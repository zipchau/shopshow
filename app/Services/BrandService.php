<?php
namespace App\Services;

use App\Models\Brand;
use File;

/**
 * BrandServices
 */
class BrandService
{
    protected $imageService;

    public function getList()
    {
        $data = Brand::select()->paginate(10);
        return $data;
    }

    /**
     * Find model by id
     * @param  int id
     * @return void
     */
    public function findById($id)
    {
        return Brand::findOrFail($id);
    }

    /**
     * Destroy Model
     * @param  int $id
     * @return boolean
     */
    public function destroyById($id)
    {
        $model = $this->findById($id);
        $model->images()->delete();
        $model->delete();
        return true;
    }

    /**
     * Restore Model
     * @param  int $id
     * @return boolean
     */
    public function restoreById($id)
    {
        $model = $this->findById($id);
        $model->restore();
        $model->images()->restore();
        return true;
    }

    /**
     * Store or update mode;
     * @param  array $request
     * @param  int id
     * @return void
     */
    public function storeOrUpdate($request, $id = null)
    {
        if ($id) {
            $model = $this->findById($id);
            $model->update($request);
            if (isset($request['images'])) {
                $image        = $request['images'];
                $oldImage     = $model->images->src ?? null;
                $actionImages = $model->images()->update(['src' => $image['src']]);
                if ($actionImages && $oldImage != $image['src']) {
                    File::delete($oldImage);
                }
            }
        } else {
            $countBrand            = Brand::all()->count();
            $request['prioritize'] = $countBrand + 1;
            $model                 = Brand::create($request);
            if (isset($request['images'])) {
                $image = $request['images'];
                $model->images()->create(['src' => $image['src']]);
            }
        }
        return $model;
    }

    public function updatePosition($request)
    {
        $arrayPosition = json_decode($request['prioritize'], true);
        foreach ($arrayPosition as $id => $value) {
            $model             = $this->findById($id);
            $model->prioritize = $value;
            $model->save();
        }
    }
}
