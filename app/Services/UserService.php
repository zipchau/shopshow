<?php
namespace App\Services;

use App\Models\User;
use File;
use HelperAdmin;

/**
 * UserServices
 */
class UserService
{
    protected $imageService;

    public function getList()
    {
        $paginate = config('form.common.paginate');
        $data     = User::with('userApprove')->notMaster()->select()->paginate($paginate);
        return $data;
    }

    /**
     * Find model by id
     * @param  int id
     * @return void
     */
    public function findById($id)
    {
        return User::findOrFail($id);
    }

    /**
     * Destroy Model
     * @param  int $id
     * @return boolean
     */
    public function destroyById($id)
    {
        $model = $this->findOrFail($id);
        $model->images()->delete();
        $model->delete();
        return true;
    }

    /**
     * Restore Model
     * @param  int $id
     * @return boolean
     */
    public function restoreById($id)
    {
        $model = $this->findOrFail($id);
        $model->restore();
        $model->images()->restore();
        return true;
    }

    /**
     * Store or update mode;
     * @param  array $request
     * @param  int id
     * @return void
     */
    public function storeOrUpdate($request, $id = null)
    {
        if ($id) {
            $model             = $this->findById($id);
            $request['status'] = $request['status'] ?? 1;
            $model->update($request);
            if (isset($request['image'])) {
                $image        = $request['image'];
                $oldImage     = $model->images->src ?? null;
                $actionImages = $model->images()->update(['src' => $image['src']]);
                if ($actionImages && $oldImage != $image['src']) {
                    File::delete($oldImage);
                }
            }
        } else {
            $model = User::create($request);
            if (isset($request['image'])) {
                $image = $request['image'];
                $model->images()->create(['src' => $image['src']]);
            }
            $model->syncPermissions(HelperAdmin::getListByRole($model->title));
        }
        return $model;
    }

    /**
     * Update permission
     * @param  array $request
     * @param  int $id
     * @return void
     */
    public function updatePermission($request, $id)
    {
        $model = $this->findById($id);
        $model->syncPermissions($request['permission']);
        return $model;
    }

    /**
     * Set Approve
     * @param  array $request
     * @param  int $id
     * @return void
     */
    public function setApprove($id)
    {
        $model = $this->findById($id);
        $model->update(['approved_at' => now()->toDateTimeString()]);
        return $model;
    }
}
