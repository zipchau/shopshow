<?php
namespace App\Services;

use App\Models\BrandCategory;

/**
 * BrandCategoryService
 */
class BrandCategoryService
{
    /**
     * get list
     * @return collection
     */
    public function getList()
    {
        $paginate = config('form.common.paginate');
        $data     = BrandCategory::select()->paginate($paginate);
        return $data;
    }

    /**
     * Find model by id
     * @param  int id
     * @return void
     */
    public function findById($id)
    {
        return BrandCategory::findOrFail($id);
    }

    /**
     * Store or update mode;
     * @param  array $request
     * @param  int id
     * @return void
     */
    public function storeOrUpdate($request, $id = null)
    {
        if ($id) {
            $data              = $this->findById($id);
            $data->update($request);
        } else {
            $data = BrandCategory::create($request);
        }
        return $data;
    }

    /**
     * Destroy Model
     * @param  int $id
     * @return boolean
     */
    public function destroyById($id)
    {
        $model = $this->findById($id);
        $model->delete();
        return true;
    }

    /**
     * Restore Model
     * @param  int $id
     * @return boolean
     */
    public function restoreById($id)
    {
        $model = $this->findById($id);
        $model->restore();
        return true;
    }

}
