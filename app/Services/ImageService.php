<?php
namespace App\Services;

use App\Models\Images;
use App\Services\Accessories\ImageFileUploadAccessory;
use File;

/**
 * ImageService
 */
class ImageService
{
    use ImageFileUploadAccessory;

    /**
     * @param  collection $model
     * @param  array $request
     * @return boolean
     */
    public function handleUpdateMultipleImage($model, $request)
    {
        $imagesModel = $model->images()->pluck('id')->toArray();
        $dataRequest = $request;
        foreach ($dataRequest as $key => $value) {
            if (isset($value['id'])) {
                $idProduct   = $value['id'];
                $imageUpdate = $model->images()->find($idProduct);
                $oldImageSrc = $imageUpdate->src;

                $pos = array_search($value['id'], $imagesModel);

                unset($value['id']);
                if ($imageUpdate->update(['src' => $value['src'], 'status' => isset($value['status']) ? 2 : 1])) {
                    if ($oldImageSrc != $value['src']) {
                        File::delete($oldImageSrc);
                    }
                }

                unset($dataRequest[$key]);
                unset($imagesModel[$pos]);
            } else {
                $model->images()->create(['src' => $value['src'], 'status' => isset($value['status']) ? 2 : 1]);
            }
        }
        if (sizeof($imagesModel) > 0) {
            foreach ($imagesModel as $itemDelete) {
                $imageDelete = $model->images()->find($itemDelete);
                if ($imageDelete->delete()) {
                    $oldImageSrc = $imageDelete->src;
                    File::delete($imageDelete);
                }
            }
        }
        return true;
    }
}
