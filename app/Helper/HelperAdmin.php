<?php
namespace App\Helper;

use Auth;
use File;
use URL;
/**
 * HelperAdmin
 */
class HelperAdmin
{
    public static function showTags($tags)
    {
        $tags          = explode(",", $tags);
        $content       = '';
        $configTagSpan = config('form.common.tag_span');
        $key           = 1;
        foreach ($tags as $tag) {
            $content = $content . '<span class="badge badge-' . $configTagSpan[$key] . '">' . $tag . '</span>';
            $key++;
            $key = $key == 6 ? 1 : $key;
        }
        return $content;
    }

    public static function showPermissionTags($permissions)
    {
        $content       = '';
        $configTagSpan = config('form.common.tag_span');
        $key           = 1;
        foreach ($permissions as $permission) {
            $content = $content . '<span class="badge badge-' . $configTagSpan[$key] . '">' . ucfirst(str_replace('-',' ',$permission->name)) . '</span>';
            $key++;
            $key = $key == 6 ? 1 : $key;
        }
        return $content;
    }

    public static function currentAdmin($attribute = 'getAll')
    {
        if (Auth::check()) {
            $data = Auth::user()->toArray();
        } else {
            $data = [];
        }
        if ($attribute == 'getAll') {
            return $data;
        }
        return $data[$attribute] ?? "error data";
    }

    public static function getListByRole($role)
    {
        $roles = config('form.permission.roles');
        if (isset($roles[$role])) {
            if ($roles[$role] == 'all-permission') {
                return config('form.permission.permissions');
            }
            return $roles[$role];
        }
        return [];
    }

    public static function showTime($value, $format = 'd/m/Y')
    {
        return date($format, strtotime($value));
    }

    public static function timeNowOrOld($value = null, $format = 'Y-m-d')
    {
        if ($value == null) {
            return date($format);
        }
        return date($format, strtotime($value));
    }

    public static function clearDotPrice($value)
    {
        return (int) str_replace('.', '', $value);
    }

    public static function showImage($image, $imageDefault = 'imagenotfound.png', $originOrDefault = false)
    {
        if ($image != false && File::exists($image)) {
            if ($originOrDefault) {
                return $image;
            }
            return URL::asset($image);
        }
        if ($originOrDefault) {
            return $imageDefault;
        }
        return URL::asset($imageDefault);
    }
}
