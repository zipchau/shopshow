<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Validation\CommonValidate;

class ValidateCustomProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        new CommonValidate();
    }
}
