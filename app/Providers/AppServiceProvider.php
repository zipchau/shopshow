<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Gate;
// use Auth;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Gate::before(function ($user, $ability) {
        //     return $user->id ==1 ? true : null;
        // });
    }
}
