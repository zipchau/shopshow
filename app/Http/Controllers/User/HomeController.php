<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Services\FrontEndService;
use Illuminate\Http\Request;
use HelperAdmin;

class HomeController extends Controller
{
    protected $frontEndService;

    public function __construct(FrontEndService $frontEndService)
    {
        $this->frontEndService = $frontEndService;
    }

    public function index()
    {
        $blogsPopular = $this->frontEndService->getBlogsPopular();
        return view('utop.pages.home',compact('blogsPopular'));
    }

    public function aboutus()
    {
        return view('utop.pages.aboutus');
    }

    public function faq()
    {
        return view('utop.pages.faq');
    }

    public function contact()
    {
        return view('utop.pages.contact');
    }

    public function business()
    {
        return view('utop.pages.business');
    }

    public function businessDetail($type)
    {
        return view('utop.pages.business_detail');
    }

    public function blog()
    {
        $categories = $this->frontEndService->getBlogCategories();
        $blogsPopular = $this->frontEndService->getBlogsPopular();
        $blogBanner = $this->frontEndService->getBannerBlog();
        $blogs = $this->frontEndService->getBlogs();
        return view('utop.pages.blog',compact('categories','blogsPopular','blogBanner','blogs'));
    }

    public function blogCategory($slug,$id)
    {
        $categories = $this->frontEndService->getBlogCategories();
        $blogBanner = $this->frontEndService->getBannerBlog();
        $blogsPopular = $this->frontEndService->getBlogsPopular();
        $blogs = $this->frontEndService->getBlogCategoryWithArticle($id);
        return view('utop.pages.blog',compact('categories','blogsPopular','blogBanner','blogs'));
    }

    public function blogDetail($slug, $id)
    {
        $categories = $this->frontEndService->getBlogCategories();
        $blogsPopular = $this->frontEndService->getBlogsPopular();
        $blog = $this->frontEndService->getBlogDetail($id);
        return view('utop.pages.blog_detail',compact('categories','blogsPopular','blog'));
    }

    public function submitContact(Request $request, $type)
    {

    }
}
