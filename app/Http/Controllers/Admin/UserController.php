<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\UpdateUserPermissionRequest;
use App\Http\Requests\Admin\UserRequest;
use App\Services\UserService;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = $this->userService->getList();

        return view('admin.user.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.user.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\Admin\UserRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $this->userService->storeOrUpdate($request->all());
        return redirect()->route('admin.user.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = $this->userService->findById($id);
        return view('admin.user.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = $this->userService->findById($id);
        return view('admin.user.form', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\Admin\UserRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        $this->userService->storeOrUpdate($request->all(), $id);
        return redirect()->route('admin.user.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->userService->destroyById($id);
        return redirect()->route('admin.user.index');
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $this->userService->restoreById($id);
        return redirect()->route('admin.user.index');
    }

    /**
     * Show the form for editing user permission the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editPermission($id)
    {
        $user           = $this->userService->findById($id);
        $userPermission = $user->getAllPermissions()->pluck('name')->toArray();
        $permissions    = config('form.permission.permissions');
        return view('admin.user.edit_permission', compact('user', 'permissions', 'userPermission'));
    }

    /**
     * Update user permission the specified resource in storage.
     *
     * @param  App\Http\Requests\Admin\UserRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updatePermission(UpdateUserPermissionRequest $request, $id)
    {
        $this->userService->updatePermission($request->all(), $id);
        return redirect()->route('admin.user.index');
    }

    /**
     * Update approve at the specified resource in storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function approve($id)
    {
        $this->userService->setApprove($id);
        return redirect()->route('admin.user.index');
    }
}
