<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\SliderImageRequest;
use App\Models\SliderImage;
use App\Services\SliderImageService;
use Illuminate\Http\Request;

class SliderController extends Controller
{
    protected $sliderImageService;

    public function __construct(SliderImageService $sliderImageService)
    {
        $this->sliderImageService = $sliderImageService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sliders = $this->sliderImageService->getList();
        return view('admin.setting_home.slider.index', compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.setting_home.slider.form');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slider = $this->sliderImageService->findById($id);
        return view('admin.setting_home.slider.form', compact('slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\Admin\SliderImageRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SliderImageRequest $request, $id)
    {
        $this->sliderImageService->storeOrUpdate($request->all(), $id);
        return redirect()->route('admin.slider.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\Admin\SliderImageRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SliderImageRequest $request)
    {
        $this->sliderImageService->storeOrUpdate($request->all());
        return redirect()->route('admin.slider.index');
    }

    public function updatePosition(Request $request)
    {
        $this->sliderImageService->updatePosition($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->sliderImageService->destroyById($id);
        return redirect()->route('admin.slider.index');
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $this->sliderImageService->restoreById($id);
        return redirect()->route('admin.slider.index');
    }
}
