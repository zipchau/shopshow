<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\BrandCategoryRequest;
use App\Services\BrandCategoryService;
use Illuminate\Http\Request;

class BrandCategoryController extends Controller
{
    protected $brandCategoryService;

    public function __construct(BrandCategoryService $brandCategoryService)
    {
        $this->brandCategoryService = $brandCategoryService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = $this->brandCategoryService->getList();

        return view('admin.brand.category.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.brand.category.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\Admin\BrandCategoryRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BrandCategoryRequest $request)
    {
        $this->brandCategoryService->storeOrUpdate($request->all());
        return redirect()->route('admin.brandcategory.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = $this->brandCategoryService->findById($id);
        return view('admin.brand.category.show', compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category   = $this->brandCategoryService->findById($id);
        return view('admin.brand.category.form', compact( 'category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\Admin\BrandCategoryRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BrandCategoryRequest $request, $id)
    {
        $this->brandCategoryService->storeOrUpdate($request->all(), $id);
        return redirect()->route('admin.brandcategory.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->brandCategoryService->destroyById($id);
        return redirect()->route('admin.brandcategory.index');
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $this->brandCategoryService->restoreById($id);
        return redirect()->route('admin.brandcategory.index');
    }
}
