<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\ImageService;
use Illuminate\Http\Request;

class ImageController extends Controller
{
    protected $imageService;

    public function __construct(ImageService $imageService)
    {
        $this->imageService = $imageService;
    }

    public function uploadImageOne(Request $request, $type = null)
    {
        return $this->imageService->handleUpload($request, $type);
    }

    public function uploadImageMultiple(Request $request, $type = null)
    {
        return $this->imageService->UploadMultiple($request, $type);
    }

    public function removeBySrc(Request $request)
    {
        return $this->imageService->removeBySrc($request->all());
    }
}
