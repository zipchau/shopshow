<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\BlogBannerImageRequest;
use App\Models\SliderImage;
use App\Services\SliderImageService;
use Illuminate\Http\Request;

class BlogBannerController extends Controller
{
    protected $sliderImageService;

    public function __construct(SliderImageService $sliderImageService)
    {
        $this->sliderImageService = $sliderImageService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sliders = $this->sliderImageService->getList('blog');
        return view('admin.blog.banner.index', compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.blog.banner.form');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $blogBanner = $this->sliderImageService->findById($id);
        return view('admin.blog.banner.form', compact('blogBanner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\Admin\BlogBannerImageRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BlogBannerImageRequest $request, $id)
    {
        $this->sliderImageService->storeOrUpdate($request->all(), $id);
        return redirect()->route('admin.blogbanner.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\Admin\BlogBannerImageRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BlogBannerImageRequest $request)
    {
        $this->sliderImageService->storeOrUpdate($request->all());
        return redirect()->route('admin.blogbanner.index');
    }

    public function updatePosition(Request $request)
    {
        $this->sliderImageService->updatePosition($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->sliderImageService->destroyById($id);
        return redirect()->route('admin.blogbanner.index');
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $this->sliderImageService->restoreById($id);
        return redirect()->route('admin.blogbanner.index');
    }
}
