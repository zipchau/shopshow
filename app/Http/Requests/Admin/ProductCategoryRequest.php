<?php

namespace App\Http\Requests\Admin;

use App\Services\ProductCategoryService;
use Auth;
use Illuminate\Foundation\Http\FormRequest;

class ProductCategoryRequest extends FormRequest
{
    protected $productCategoryService;

    public function __construct(ProductCategoryService $productCategoryService)
    {
        $this->productCategoryService = $productCategoryService;
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user()->can('product-category-control');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $parentList = $this->productCategoryService->getParentList();
        $parentList = array_column($parentList, 'id');
        return [
            'parent_id' => 'required|numeric|in:' . implode(',', $parentList),
            'name'      => 'required|max:191|unique:product_categories,name,' . $this->id,
        ];
    }
}
