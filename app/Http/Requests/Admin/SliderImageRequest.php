<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class SliderImageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [
            'images.src' => 'required|max:255',
            'title'      => 'max:255',
            'content'    => 'max:255',
            'btn_name'   => 'max:255',
            'btn_url'    => 'max:255',
        ];
        return $rules;
    }

    public function attributes()
    {
        return [
            'images.src' => 'image',
        ];
    }
}
