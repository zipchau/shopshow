<?php

namespace App\Http\Requests\Admin;

use Auth;
use Illuminate\Foundation\Http\FormRequest;

class BlogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if ($this->_method == 'PUT') {
            return Auth::user()->can('blog-edit');
        }
        return Auth::user()->can('blog-create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'blog_category_id' => 'required|exists:blog_categories,id',
            'title'            => 'required|max:255',
            'content'          => 'required',
            'file_uploads'     => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'blog_category_id' => 'Category List',
            'title'            => 'Title',
            'content'          => 'Content',
            'file_uploads'     => 'Image',
        ];
    }

}
