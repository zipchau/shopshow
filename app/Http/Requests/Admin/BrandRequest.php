<?php

namespace App\Http\Requests\Admin;

use Auth;
use Illuminate\Foundation\Http\FormRequest;

class BrandRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'brand_category_id' => 'required|exists:brand_categories,id',
            'images.src'        => 'required|max:255',
            'name'              => 'required|max:255',
        ];
    }

    public function attributes()
    {
        return [
            'brand_category_id' => 'Category List',
        ];
    }

}
