<?php

namespace App\Http\Requests\Admin;

use App\Services\BlogCategoryService;
use Illuminate\Foundation\Http\FormRequest;
use Auth;

class BlogCategoryRequest extends FormRequest
{
    protected $blogCategoryService;

    public function __construct(BlogCategoryService $blogCategoryService)
    {
        $this->blogCategoryService = $blogCategoryService;
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user()->can('blog-category-control');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->merge(['parent_id' => 1]);
        $parentList = $this->blogCategoryService->getParentList();
        $parentList = array_column($parentList, 'id');
        return [
            'parent_id' => 'numeric|in:' . implode(',', $parentList),
            'name'      => 'required|max:191|unique:blog_categories,name,' . $this->id,
        ];
    }
}
