<?php

namespace App\Http\Requests\Admin;

use Auth;
use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user()->can('user-control');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name'                  => 'required|max:255',
            'email'                 => 'required|unique:users,email,' . $this->id,
            'phone'                 => 'required|tel|unique:users,phone,' . $this->id,
            'password'              => 'required|confirmed|min:6|max:30',
            'images.src'            => 'required|max:255',
            'address'               => 'max:255',
            'title'                 => 'required|in:' . implode(',', array_keys(config('form.permission.title'))),
            'password_confirmation' => 'required|min:6',
        ];

        if ($this->_method == 'PUT') {
            if (!isset($this->password) || strlen(trim($this->password)) == 0) {
                unset($rules['password']);
                unset($rules['password_confirmation']);
            }
        }
        return $rules;
    }

    public function attributes()
    {
        return [
            'images.src' => 'image',
        ];
    }
}
