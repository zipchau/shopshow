<?php

namespace App\Http\Requests\Admin;

use Auth;
use HelperAdmin;
use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if ($this->_method == 'PUT') {
            return Auth::user()->can('product-edit');
        }
        return Auth::user()->can('product-create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->merge(['price_regular' => HelperAdmin::clearDotPrice($this->price_regular)]);
        $this->merge(['price_sale' => HelperAdmin::clearDotPrice($this->price_sale)]);

        return [
            'product_category_id' => 'required|exists:product_categories,id',
            'name'                => 'required|max:191',
            'content'             => 'required',
            'price_regular'       => 'required|integer_remove',
            'price_sale'          => 'required|integer_remove',
            'file_uploads'        => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'product_category_id' => 'Category list',
            'name'                => 'Name',
            'content'             => 'Content',
            'price_regular'       => 'Price regular',
            'price_sale'          => 'Price sale',
            'file_uploads'        => 'Image',
        ];
    }

}
